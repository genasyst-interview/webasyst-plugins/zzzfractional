<?php

return array(
    'items' => array(
        'name' => 'Целочисленные единицы',
        'units' => array(
            'item' => array(
                'name' => _w('items'),
                'fractional' => 0,
                'rate' => 1,
                'unit_multiplicity' => 1,
                'multiplier' => 1,
                'enabled' => 1
            ),
            'i_l' => array(
                'name' => _w('l'),
                'fractional' => 0,
                'rate' => 1,
                'unit_multiplicity' => 1,
                'multiplier' => 1,
                'enabled' => 1
            ),
            '100g' => array(
                'name' => '100 гр.',
                'fractional' => 0,
                'rate' => 1,
                'unit_multiplicity' => 1,
                'multiplier' => 1,
                'enabled' => 1
            ),
            '05l' => array(
                'name' => '0,5 л.',
                'fractional' => 0,
                'rate' => 1,
                'unit_multiplicity' => 1,
                'multiplier' => 1,
                'enabled' => 1
            ),
            'i_rm' => array(
                'name' => _w('м. п.'),
                'fractional' => 0,
                'rate' => 1,
                'unit_multiplicity' => 1,
                'multiplier' => 1,
                'enabled' => 1
            ),

        )
    ),
    // Длина
    'length' => array(
        'name'      => _w('Length'),
        'base_unit' => 'cm',
        'units' => array(
            'm' =>  array(
                'name' => _w('m'),
                'fractional' => 1,
                'rate' => 0.1,
                'unit_multiplicity' => 0.01,
                'multiplier' => 100,
                'enabled' => 1
            ),
            'rm' =>  array(
                'name' => _w('м. п.'),
                'fractional' => 1,
                'multiplier' => 100,
                'rate' => 0.1,
                'unit_multiplicity' => 0.01,
                'enabled' => 1
            ),
           /* 'mm' => array(
                'multiplier' => 0.1,
                'rate' => 1,
                'fractional' => 0,
            ),*/
            'cm' => array(
                'name' => _w('cm'),
                'multiplier' => 1,
                'fractional' => 0,
                'rate' => 1,
                'unit_multiplicity' => 1,
                'enabled' => 1
            ),
            'km' => array(
                'name' => _w('km'),
                'fractional' => 1,
                'multiplier' => 100000,
                'unit_multiplicity' => 0.001,
                'rate' => 0.001,
                'enabled' => 1
            ),
        ),
       
    
    ),
    // Вес
    'weight' => array(
        'name'      => _w('Weight'),
        'base_unit' => 'g',
        'units' => array(
            'kg' =>  array(
                'name' => _w('kg'),
                'multiplier' => 1000,
                'fractional' => 1,
                'rate' => 0.1,
                'unit_multiplicity' => 0.001,
                'enabled' => 1
            ),
            'g'  => array(
                'name' => _w('g'),
                'multiplier' => 1,
                'fractional' => 0,
                'unit_multiplicity' => 1,
                'rate' => 1,
                'enabled' => 1
            ),
            'lbs' => array(
                'name'       => _w('lbs'), //pound
                'multiplier' => 453.59,
                'fractional' => 1,
                'rate' => 1, 
                'unit_multiplicity' => 0.1,
                'enabled' => 1
            ),
            'oz'  => array(
                'name'       => _w('oz'), //ounce
                'multiplier' => 28.3495,
                'fractional' => 1,
                'rate' => 1,
                'unit_multiplicity' => 1,
                'enabled' => 1
            ),
        ),
    ),
    // Объем
    'volume' => array(
        'name'      => _w('Volume'),
        'base_unit' => 'ml',
        'units' => array(
            'l' =>  array(
                'name' => _w('l'),
                'multiplier' => 1000,
                'unit_multiplicity' => 0.001,
                'fractional' => 1,
                'rate' => 0.001,
                'enabled' => 1
            ),
            'ml' => array(
                'name' => _w('ml'),
                'multiplier' => 1,
                'rate' => 1,
                'fractional' => 0,
                'unit_multiplicity' => 1,
                'enabled' => 1
            ),
            'cm3' => array(
                'name' => _w('cm3'),
                'multiplier' => 1,
                'rate' => 1,
                'fractional' => 0,
                'unit_multiplicity' => 1,
                'enabled' => 1
            ),
            'm3' => array(
                'name' => _w('m3'),
                'multiplier' => 10000000,
                'unit_multiplicity' => 0.001,
                'rate' => 0.1,
                'fractional' => 1,
                'enabled' => 1
            ),
        ),

    ),
    // Площадь
    'area' => array(
        'name' => _w('Area'),
        'base_unit' => 'sqcm',
        'units' => array(
            'sqm' => array(
                'name' => _w('sq m'),
                'multiplier' => 10000,
                'unit_multiplicity' => 0.001,
                'rate' => 0.1,
                'fractional' => 1,
                'enabled' => 1
            ),
            'ha' => array(
                'name' => _w('ha'),
                'multiplier' => 100000000,
                'unit_multiplicity' => 0.001,
                'rate' => 0.1,
                'fractional' => 1,
                'enabled' => 1
            ),
           /* 'sqmm' => array(
                'name' => _w('sq mm'),
                'multiplier' => 0.01,
                'rate' => 1,
                'fractional' => 0,
                'unit_multiplicity' => 1,
                'enabled' => 1
            ),*/
            'sqcm' => array(
                'name' => _w('sq cm'),
                'multiplier' => 1,
                'rate' => 1,
                'fractional' => 0,
                'unit_multiplicity' => 1,
                'enabled' => 1
            ),
            'sqkm' => array(
                'name' => _w('sq km'),
                'multiplier' => 10000000000,
                'unit_multiplicity' => 0.001,
                'rate' => 0.1,
                'fractional' => 1,
                'enabled' => 1
            ),
        ),

    )
);