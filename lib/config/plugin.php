<?php

return array(
    'name'     => 'Дробные товары',
    'version'  => '7.4.5.243',
    'img'=>'img/zzzfractional16.png',
    'vendor'   => '990614',
    'shop_settings' => true, 
    'frontend' => true,
    'custom_settings' => true, 
    'handlers' =>  array(
        /* BACKEND */
        /* PRODUCTS */

        /* 'init' =>   'appInit', Для подключения стратегии подмены через автолоадер */
        'routing' => 'routing',
        'init' =>   'appInit', 
        'backend_product' =>      'backendProduct',
        'backend_product_edit' => 'backendProductEdit',
        'product_presave' => 'productPresave',
        'product_save' =>         'productSave',
        
        'products_collection' => 'productsCollection',
        /* 'product_mass_update' => 'productMassUpdate', */
        /* ORDERS */
        'backend_order_edit' => 'backendOrderEdit',
        'backend_order' => 'backendOrder',
        /* FRONTEND */
        'frontend_head' => 'frontendHead',
        'frontend_products' => 'frontendProducts',
        'frontend_product' => 'frontendProduct',
        'frontend_cart' => 'frontendCart',
        'cart_add' => 'cartAdd'
    ),
);
