<?php

return array(
    'php' => array(
        'strict' => true,
        'version' => '>=5.4',
    ),
    'app.shop' => array(
        'strict' => true,
        'version' => '>=7.4.3.235',
    ), 
); 