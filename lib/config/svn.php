<?php
/**
 * @see shopZzzfractionalPluginStrategyInstallerVersion::onSvnFiles()
 * TODO: В дальнейшем конфиг надо будет разбить на мажорные версии-линейки, наприсер как переход с 7.2 на 7.4
 **/
return array(
    '7.4.4.238' => array(
        /* ACTIONS */
       'shopCsvProductrunController' => array(
            'app' =>      '7.4.3.235/actions/zzzfractionalshopZzzfractionalPluginCsvProductrun.controller.php',
            'strategy' => '7.4.3.235/actions/zzzfractionalshopCsvProductrun.controller.php',
        ),
        'shopOrderEditAction' => array(
            'app' =>      '7.4.3.235/actions/zzzfractionalshopZzzfractionalPluginOrderEdit.action.php',
            'strategy' => '7.4.3.235/actions/zzzfractionalshopOrderEdit.action.php',
        ),
        'shopOrdersGetProductController' => array(
            'app' =>      '7.4.3.235/actions/zzzfractionalshopZzzfractionalPluginOrdersGetProduct.controller.php',
            'strategy' => '7.4.3.235/actions/zzzfractionalshopOrdersGetProduct.controller.php',
        ), 
        'shopProductsAction' => array(
            'app' =>      '7.4.3.235/actions/zzzfractionalshopZzzfractionalPluginProducts.action.php',
            'strategy' => '7.4.3.235/actions/zzzfractionalshopProducts.action.php',
        ),
        'shopReportsproductsActions' => array(
            'app' =>      '7.4.3.235/actions/zzzfractionalshopZzzfractionalPluginReportsproducts.actions.php',
            'strategy' => '7.4.3.235/actions/zzzfractionalshopReportsproducts.actions.php',
        ),

        /* CLASSES */
        'shopCart' => array(
            'app' =>      '7.4.3.235/classes/zzzfractionalshopZzzfractionalPluginCart.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopCart.class.php',
        ),
        'shopCheckoutShipping' => array(
            'app' =>      '7.4.3.235/classes/zzzfractionalshopZzzfractionalPluginCheckoutShipping.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopCheckoutShipping.class.php',
        ),
        'shopHelper' => array(
            'app' =>      '7.4.3.235/classes/zzzfractionalshopZzzfractionalPluginHelper.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopHelper.class.php',
        ),
        'shopOrder' => array(
            'app' =>      '7.4.3.235/classes/zzzfractionalshopZzzfractionalPluginOrder.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopOrder.class.php',
        ), 
        'shopProductMassUpdate' => array(
            'app' =>      '7.4.3.235/classes/zzzfractionalshopZzzfractionalPluginProductMassUpdate.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopProductMassUpdate.class.php',
        ),
        'shopWorkflowCreateAction' => array(
            'app' =>      '7.4.3.235/classes/zzzfractionalshopZzzfractionalPluginWorkflowCreateAction.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopWorkflowCreateAction.class.php',
        ),
        'shopWorkflowEditAction' => array(
            'app' =>      '7.4.3.235/classes/zzzfractionalshopZzzfractionalPluginWorkflowEditAction.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopWorkflowEditAction.class.php',
        ),

        /* MODELS */
        'shopCartItemsModel' => array(
            'app' =>      '7.4.3.235/models/zzzfractionalshopZzzfractionalPluginCartItemsModel.class.php',
            'strategy' => '7.4.3.235/models/zzzfractionalshopCartItemsModel.class.php',
        ),
        /**
        * Первая версия изменений описана полностью, shopOrderItemsModel файл был изменен его тоже описываем
        * Чтобы в следующих версиях можно было полноценно создавать линки на все файлы 'shopOrderItemsModel' => '7.4.4.238'
        **/
        'shopOrderItemsModel' => array(
            'app' =>      '7.4.4.238/models/zzzfractionalshopZzzfractionalPluginOrderItemsModel.class.php',
            'strategy' => '7.4.3.235/models/zzzfractionalshopOrderItemsModel.class.php',
        ),
        'shopOrderModel' => array(
            'app' =>      '7.4.3.235/models/zzzfractionalshopZzzfractionalPluginOrderModel.class.php',
            'strategy' => '7.4.3.235/models/zzzfractionalshopOrderModel.class.php',
        ),
        'shopProductStocksLogModel' => array(
            'app' =>      '7.4.3.235/models/zzzfractionalshopZzzfractionalPluginProductStocksLogModel.class.php',
            'strategy' => '7.4.3.235/models/zzzfractionalshopProductStocksLogModel.class.php',
        ),
        'shopSalesModel' => array(
            'app' =>      '7.4.3.235/models/zzzfractionalshopZzzfractionalPluginSalesModel.class.php',
            'strategy' => '7.4.3.235/models/zzzfractionalshopSalesModel.class.php',
        ),
    ),
    '7.4.5.243' => array(
            /* ACTIONS */
        'shopCsvProductrunController' => array(
            'app' =>      '7.4.5.243/actions/zzzfractionalshopZzzfractionalPluginCsvProductrun.controller.php',
            'strategy' => '7.4.3.235/actions/zzzfractionalshopCsvProductrun.controller.php',
        ),
        'shopOrderEditAction' =>  '7.4.4.238',
        'shopOrdersGetProductController' => array(
            'app' =>      '7.4.5.243/actions/zzzfractionalshopZzzfractionalPluginOrdersGetProduct.controller.php',
            'strategy' => '7.4.3.235/actions/zzzfractionalshopOrdersGetProduct.controller.php',
        ),
        'shopProductsAction' => array(
            'app' =>      '7.4.3.235/actions/zzzfractionalshopZzzfractionalPluginProducts.action.php',
            'strategy' => '7.4.5.243/actions/zzzfractionalshopProducts.action.php',
        ),
        'shopReportsproductsActions' =>  '7.4.4.238',
            /* CLASSES */
        'shopCart' =>  '7.4.4.238',
        'shopCheckoutShipping' =>  '7.4.4.238', 
        'shopHelper' =>  '7.4.4.238',
        'shopOrder' => array(
            'app' =>      '7.4.5.243/classes/zzzfractionalshopZzzfractionalPluginOrder.class.php',
            'strategy' => '7.4.3.235/classes/zzzfractionalshopOrder.class.php',
        ),
        'shopProductMassUpdate' =>  '7.4.4.238',
        'shopWorkflowCreateAction' =>  '7.4.4.238',
        'shopWorkflowEditAction' =>  '7.4.4.238',
            /* MODELS */
        'shopCartItemsModel' =>  '7.4.4.238',
        'shopOrderItemsModel' =>  '7.4.4.238',
        'shopOrderModel' =>  '7.4.4.238',
        'shopProductStocksLogModel' =>  '7.4.4.238',
        'shopSalesModel' =>  '7.4.4.238',
    ),
);