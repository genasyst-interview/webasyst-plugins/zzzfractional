<?php

class shopZzzfractionalPluginBackendSettingsController extends waViewController
{
    public function execute()
    {
        if (waRequest::method() == 'get') {
            $action = new shopZzzfractionalPluginSettingsAction();
            echo $action->display();
        } else {
            $action = new shopZzzfractionalPluginBackendSettingsSaveController();
            $action->run();
        }
    }
}
