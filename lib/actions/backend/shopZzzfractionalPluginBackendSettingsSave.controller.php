<?php

class shopZzzfractionalPluginBackendSettingsSaveController extends waJsonController
{
    public function execute()
    {
        if (waRequest::method() == 'post') {
            $plugin = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID);
            $plugin->saveSettings(waRequest::post('settings'));
        }
    }

    public function __destruct()
    {
        shopZzzfractionalPluginStrategyInstallerVersion::factory()->run();
    }
}
