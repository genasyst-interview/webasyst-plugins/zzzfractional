<?php

class shopZzzfractionalPluginSettingsAction extends waViewAction
{

    public function execute()
    {
        $plugin = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID);
        $settings = $plugin->getSettings();
        $this->view->assign('settings', $settings);

        $type_model = new shopTypeModel();
        $types = $type_model->getTypes();
        if (is_array($types) && !empty($types)) {
            $this->view->assign('product_types', $types);
        } else {
            $this->view->assign('product_types', array());
        }

        $this->view->assign('dimension', shopZzzfractionalPluginDimension::getInstance());

    }
}
