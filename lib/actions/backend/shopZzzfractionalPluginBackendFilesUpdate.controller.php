<?php

class shopZzzfractionalPluginBackendFilesUpdateController extends waController
{

    public function execute()
    {
        $version_installer = shopZzzfractionalPluginStrategyInstallerVersion::factory();
        if (waRequest::get('update')) {
            $version_installer->updatePlugin();
        } else {
            $current_install_version = $version_installer->getInstalledVersion();
            $version = $version_installer->getActualVersion();

            echo 'Текущая версия приложения: ' . $version_installer->getRealAppVersion() . '<br><br>';
            echo 'Установленные версии: приложение (' . $current_install_version['app'] . '), плагин (' . $current_install_version['strategy'] . ')!<br><br>';
            echo 'Актуальные версии: приложение (' . $version['app'] . '), плагин (' . $version['strategy'] . ')!<br><br>';

            echo 'Включенные файлы оригиналов приложения: <br>';
            $files = $version_installer->getStrategyFiles('original', 'on');
            foreach ($files as $v) {
                echo '' . $v . '<br>';
            }
            
            echo ' <br>Включенные файлы стратегии: <br>';
            $files = $version_installer->getStrategyFiles('strategy', 'on');
            foreach ($files as $v) {
                echo '' . $v . '<br>';
            }

            echo ' <br>Остальные файлы оригиналов приложения: <br>';
            $files = $version_installer->getStrategyFiles('original', 'off');
            foreach ($files as $v) {
                echo '' . $v . '<br>';
            }

            echo ' <br>Остальные файлы стратегии: <br>';
            $files = $version_installer->getStrategyFiles('strategy', 'off');
            foreach ($files as $v) {
                echo '' . $v . '<br>';
            }
        }
    }
}
