<?php

class shopZzzfractionalPluginBackendVersionInstallerController extends waController
{

    public function execute()
    {
        $version_installer = shopZzzfractionalPluginStrategyInstallerVersion::factory();
        if (waRequest::get('install')) {
            $strategy_version = waRequest::get('strategy');
            if (!version_compare($strategy_version, '1.0.0', '>')) {
                echo 'не корректная версия:' . $strategy_version;

                return;
            }
            $version_installer->install(array('strategy' => $strategy_version));
        } else {
            $current_version = $version_installer->getInstalledVersion();
            echo 'Текущая версия магазина: ' . $version_installer->getRealAppVersion() . '<br><br>
           Текущая версия подмены оригиналов: ' . $current_version['app'] . '<br>
           Текущая версия подмены стратегии: ' . $current_version['strategy'] . '<br><br>
           Версии приложения поддерживаемые:<br>';
            $versions = $version_installer->getStrategyVersions();
            foreach ($versions as $v) {
                echo '' . $v . '<br>';
            }
            echo '<br><br>Доступные версии стратегии:<br>';
            $versions = $version_installer->getStrategyVersions();
            foreach ($versions as $v) {
                echo '<a href="?plugin=zzzfractional&action=VersionInstaller&strategy=' . $v . '&install=1">Установить ' . $v . '</a><br>';
            }
        }
    }
}
