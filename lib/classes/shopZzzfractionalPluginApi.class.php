<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/helpers/shopZzzfractionalPluginHelperUnits.trait.php', 'shop'));

class shopZzzfractionalPluginApi implements shopZzzfractionalPluginHelperUnitsInterface, shopZzzfractionalPluginStrategyPublicInterface
{
    use shopZzzfractionalPluginHelperUnitsTrait;

    private static $_instance = null;

    public static function init()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    protected function __construct()
    {
    }

    /* StrategyPublicInterface */

    public function decodeCount(&$count = null, $unit_id, $cast_default = '')
    {
        return $this->getStrategy()->decodeCount($count, $unit_id, $cast_default);
    }

    public function encodeCount(&$count = null, $unit_id, $cast_default = '')
    {
        return $this->getStrategy()->encodeCount($count, $unit_id, $cast_default);
    }

    public function decodeItemsCount(&$items)
    {
        return $this->getStrategy()->decodeItemsCount($items);
    }

    public function encodeItemsCount(&$items)
    {
        return $this->getStrategy()->encodeItemsCount($items);
    }

    public function decodeItemCount(&$item, $product = null)
    {
        return $this->getStrategy()->decodeItemCount($item, $product);
    }

    public function encodeItemCount(&$item, $product = null)
    {
        return $this->getStrategy()->encodeItemCount($item, $product);
    }

    public function decodeProducts(&$products)
    {
        return $this->getStrategy()->decodeProducts($products);
    }

    public function encodeProducts(&$products)
    {
        return $this->getStrategy()->encodeProducts($products);
    }

    public function decodeProduct(&$product)
    {
        return $this->getStrategy()->decodeProduct($product);
    }

    public function encodeProduct(&$product)
    {
        return $this->getStrategy()->encodeProduct($product);
    }

    public function decodeSkus(&$skus)
    {
        return $this->getStrategy()->decodeSkus($skus);
    }

    public function encodeSkus(&$skus)
    {
        return $this->getStrategy()->encodeSkus($skus);
    }

    public function decodeSku(&$sku)
    {
        return $this->getStrategy()->decodeSku($sku);
    }

    public function encodeSku(&$sku)
    {
        return $this->getStrategy()->encodeSku($sku);
    }

    public function encodeAndCastCountByProduct(&$count, $product_data, &$error = null)
    {
        return $this->getStrategy()->encodeAndCastCountByProduct($count, $product_data, $error);
    }

    public function decodeAndCastCountByProduct(&$count, $product_data, &$error = null)
    {
        return $this->getStrategy()->decodeAndCastCountByProduct($count, $product_data, $error);
    }

    public function castCountByProduct(&$count, $product_data, &$error = null)
    {
        return $this->getStrategy()->castCountByProduct($count, $product_data, $error);
    }

    public function encodeCartAddQuantity(&$quantity, $sku_id, $product_id = null, &$error = null)
    {
        return $this->getStrategy()->encodeCartAddQuantity($quantity, $sku_id, $product_id, $error);
    }

    public function encodeCartSaveQuantity(&$quantity, $sku_id, $product_id = null, &$error = null)
    {
        return $this->getStrategy()->encodeCartSaveQuantity($quantity, $sku_id, $product_id, $error);
    }

    public function setConvert($flag = null)
    {
        $this->getStrategy()->setConvert($flag);
    }

    public function returnValueType($type = shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE_NUMERIC)
    {
        $this->getStrategy()->returnValueType($type);
    }

    public function castReturnValue(&$value)
    {
        return $this->getStrategy()->castReturnValue($value);
    }

    public function unitKey()
    {
        return $this->getStrategy()->unitKey();
    }

    public function multiplicityKey()
    {
        return $this->getStrategy()->multiplicityKey();
    }

    public function weightKey()
    {
        return $this->getStrategy()->weightKey();
    }

    public function addItemsData(&$items = array())
    {
        return $this->getStrategy()->addItemsData();
    }

    /**
     * @return shopZzzfractionalPluginStrategyEmulate
     */
    private function getStrategy()
    {
        return wao(new shopZzzfractionalPluginFactory())->getStrategy();
    }


}

