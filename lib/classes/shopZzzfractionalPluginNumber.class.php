<?php

/**
 * https://github.com/robksawyer/grabitdown/blob/078255cff1d97ae7627a452b2c0493d9b8ca7bce/app/Plugin/Tools/Lib/NumberLib.php
 */
class shopZzzfractionalPluginNumber
{

    /**
     * get the rounded average
     *
     * @param array $values : int or float values
     * @param int   $precision
     *
     * @return int $average
     * 2009-09-05 ms
     */
    public static function average($values, $precision = 0)
    {
        $average = round(array_sum($values) / count($values), $precision);

        return $average;
    }

    /**
     * @access public
     *
     * @param float $number
     * @param float $increments
     *
     * @return float $result
     * 2011-04-14 lb
     */
    public static function roundTo($number, $increments = 0.0001)
    {
        return round($number, self::getDecimalPlaces($increments));
    }

    public static function roundToMultiplicity($number, $multiplicity = 0.001)
    {
        $fmod = fmod($number, $multiplicity);
        $epsilon = $multiplicity / 2;
        if ($fmod < $epsilon) {
            return self::roundDownTo($number, $multiplicity);
        } else {
            return self::roundUpTo($number, $multiplicity);
        }
    }

    /**
     * @access public
     *
     * @param float $number
     * @param int   $increments
     *
     * @return float $result
     * 2011-04-14 lb
     */
    public static function roundUpTo($number, $increments = 1)
    {
        return (ceil($number / $increments) * $increments);
    }

    /**
     * @access public
     *
     * @param float $number
     * @param int   $increments
     *
     * @return float $result
     * 2011-04-14 lb
     */
    public static function roundDownTo($number, $increments = 1)
    {
        return (floor($number / $increments) * $increments);
    }

    public static function getEpsilon($float = 0.0)
    {
        return round(($float - floor($float)));
    }

    /**
     * @access public
     *
     * @param float $number
     *
     * @return int $decimalPlaces
     * 2011-04-15 lb
     */
    public static function getDecimalPlaces($number)
    {
        $decimalPlaces = 0;
        while ($number > 1 && $number != 0) {
            $number /= 10;
            $decimalPlaces -= 1;
        }
        while ($number < 1 && $number != 0) {
            $number *= 10;
            $decimalPlaces += 1;
        }

        return $decimalPlaces;
    }

    public static function cast($float)
    {
        $float = str_replace(',', '.', $float);
        $float = self::roundTo(floatval($float));

        return $float;
    }

    public static function toString($float)
    {
        return str_replace(',', '.', (string)$float);
    }

    public static function pad($value, $length = 2)
    {
        return str_pad(intval($value), $length, '0', STR_PAD_LEFT);
    }
}

