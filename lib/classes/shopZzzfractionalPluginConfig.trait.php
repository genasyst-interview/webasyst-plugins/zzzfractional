<?php
trait shopZzzfractionalPluginConfigTrait {
    protected static $_instance = null;
    public static function init()
    {
        if (static::$_instance == null) {
            static::$_instance = new static();
        }
        return static::$_instance;
    }
}