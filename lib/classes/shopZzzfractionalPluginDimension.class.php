<?php


class shopZzzfractionalPluginDimension extends shopDimension
{

    private static $instance;
    private $units = array();
    private $units_map = array();

    protected static $rates = array(
        'fractional' => array(
            'name'  => 'Дробные единицы',
            'units' => array(
                '0.01' => '0.01',
                '0.05' => '0.05',
                '0.1'  => '0.1',
                '0.15' => '0.15',
                '0.2'  => '0.2',
                '0.25' => '0.25',
                '0.30' => '0.30',
                '0.50' => '0.50',
                '0.75' => '0.75',
                '1.25' => '1.25',
                '1.5'  => '1.5',
                '2.5'  => '2.5',
            ),
        ),
        'item'       => array(
            'name'  => 'Целочисленные единицы',
            'units' => array(
                '1'   => '1',
                '2'   => '2',
                '3'   => '3',
                '4'   => '4',
                '5'   => '5',
                '10'  => '10',
                '20'  => '20',
                '25'  => '25',
                '50'  => '50',
                '75'  => '75',
                '100' => '100',
            ),
        ),
    );

    private function __construct()
    {
        $this->initDimensions();
    }

    private function __clone()
    {

    }

    public function getList()
    {
        $units = $this->units;
        /* Удаляем фунты, криво считает! */
        if (array_key_exists('weight', $units)
            && array_key_exists('units', $units['weight'])
            && array_key_exists('lbs', $units['weight']['units'])
        ) {
            unset($units['weight']['units']['lbs']);
        }

        return $units;
    }

    public static function getRates()
    {
        return self::$rates;
    }

    protected function initDimensions()
    {
        $available_dimensions = $this->getDimensionConfig();
        foreach ($available_dimensions as $type => $data) {
            if (is_array($data['units'])) {
                $type_data = array(
                    'name' => $data['name'],

                    'units' => array(),
                );
                if (array_key_exists('base_unit', $data)) {
                    $type_data['base_unit'] = $data['base_unit'];
                }
                foreach ($data['units'] as $unit_id => $unit_data) {
                    if (array_key_exists('enabled', $unit_data) && $unit_data['enabled'] == 1) {
                        $unit_data['type'] = $type;
                        $type_data['units'][$unit_id] = $unit_data;
                        $this->units_map[$unit_id] = $type;
                    }
                }
                $this->units[$type] = $type_data;
            }
        }
    }

    public function getUnitType($unit)
    {
        if (array_key_exists($unit, $this->units_map)) {
            return $this->units_map[$unit];
        }

        return '';
    }

    /**
     * @param $type
     * @param $unit
     *
     * @return array
     */
    public static function isUnit($unit, $type = null)
    {
        $instance = self::getInstance();
        if ($type == null) {
            $type = $instance->getUnitType($unit);
        }
        if ($type && ($d = $instance->getDimension($type))) {
            $units = self::getUnits($type);
            if (isset($units[$unit])) {
                return true;
            }
        }

        return false;
    }

    public static function getUnits($type)
    {
        $units = array();
        $instance = self::getInstance();
        if ($type && ($d = $instance->getDimension($type))) {
            if (isset($d['units'])) {
                $units = $d['units'];
                /*foreach ($d['units'] as $code => $unit) {
                    $unit['type'] = $type;
                    $units[$code] = $unit;
                }*/
            }
        }

        return $units;
    }

    /* @deprecated */
    public function getUnitRate($unit, $type = null)
    {

        $unit = self::getUnit($unit, $type);
        if (!empty($unit)) {
            if ($unit['fractional'] == 1) {
                return $unit['rate'];
            }
        }

        return 1;
    }

    public static function getUnitMultiplicity($unit_id, $type = null)
    {

        $unit = self::getUnit($unit_id, $type);
        if (!empty($unit)) {
            if ($unit['fractional'] == 1) {
                return $unit['unit_multiplicity'];
            }
        }

        return 1;
    }

    public static function getUnit($unit, $type = null)
    {
        $instance = self::getInstance();
        if ($type == null) {
            $type = $instance->getUnitType($unit);
        }
        if ($type && ($d = $instance->getDimension($type))) {
            $units = self::getUnits($type);
            if (isset($units[$unit])) {
                return $units[$unit];
            }
        }

        return array();
    }

    public static function isFractional($unit = '')
    {
        if (is_array($unit) && array_key_exists('fractional', $unit)) {
            return ($unit['fractional'] == 1);
        } else {
            $unit = self::getUnit($unit);
            if (!empty($unit) && array_key_exists('fractional', $unit)) {
                return ($unit['fractional'] == 1);
            }
        }

        return false;
    }

    public function convert($value, $type, $unit, $value_unit = null)
    {
        if (empty($type) && !empty($value_unit)) {
            $type = $this->getUnitType($value_unit);
        }

        if ($dimension = $this->getDimension($type)) {
            if ($value_unit !== null) {
                if (isset($dimension['units'][$value_unit])) {
                    $value = bcmul(str_replace(',', '.', (string)$value), str_replace(',', '.', (string)$dimension['units'][$value_unit]['multiplier']), 10);
                }
            }

            if (($unit !== null) && ($unit != $dimension['base_unit'])) {
                if (isset($dimension['units'][$unit])) {
                    $value = bcdiv(str_replace(',', '.', (string)$value), str_replace(',', '.', (string)$dimension['units'][$unit]['multiplier']), 10);
                }
                if ($dimension['units'][$unit]['unit_multiplicity'] == 1) {
                    $value = shopZzzfractionalPluginNumber::roundUpTo($value, $dimension['units'][$unit]['unit_multiplicity']);
                }
            }
        }
        $value = shopZzzfractionalPluginNumber::roundTo($value);

        return $value;
    }

    public function getDimension($type)
    {
        $type = preg_replace('/^[^\.]*\./', '', $type);
        $d = null;
        if (isset($this->units[$type])) {
            $d = $this->units[$type];
        }

        return $d;
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    private function getDimensionConfig()
    {
        $app_config = wa('shop');
        $files = array(
            $app_config->getConfigPath('shop/plugins/' . shopZzzfractionalPlugin::PLUGIN_ID) . '/dimension.php',
            $app_config->getAppPath('plugins/' . shopZzzfractionalPlugin::PLUGIN_ID, 'shop') . '/lib/config/dimension.php',
        );

        foreach ($files as $file_path) {
            if (file_exists($file_path)) {
                $data = include($file_path);
                if (is_array($data)) {
                    return $data;
                }
            }
        }

        return array();
    }

}

