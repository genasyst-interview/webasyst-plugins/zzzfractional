<?php

class shopZzzfractionalPluginView
{

    protected static $templates = null;

    /**
     * Возвращает новый объект представления
     * @return waSmarty3View
     */
    protected static function getView()
    {
        return new waSmarty3View(wa());
    }

    protected static function getApp()
    {
        return wa('shop');
    }

    /**
     * Компилирует шаблон представления и возвращает результат в виде html кода
     *
     * @param $data     -  массив
     * @param $template - относительный путь шаблона
     *
     * @return string
     */
    public static function fetch($data = array(), $template)
    {
        $view = self::getView();
        $view->assign($data);

        return $view->fetch(self::getTemplate($template));
    }

    protected static function getTemplates()
    {
        if (self::$templates == null) {
            $plugin = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID);
            self::$templates = new shopZzzfractionalPluginTemplates($plugin);
        }

        return self::$templates;
    }

    protected static function getTemplate($template)
    {
        return self::getTemplates()->getTemplate($template);
    }

    public static function getTemplateUrl($template)
    {
        return self::getTemplates()->getTemplateUrl($template);
    }

    public static function getThemeCorrection()
    {
        $theme = self::getTemplates()->getTheme();
        $plugin_path = wa('shop')->getAppPath('/plugins/' . shopZzzfractionalPlugin::PLUGIN_ID . '/');
        $theme_id = $theme['id'];
        if (!empty($theme['source_theme_id'])) {
            $theme_id = $theme['source_theme_id'];
        }

        $js_path = 'js/themes/' . $theme_id . '.js';
        $css_path = 'css/themes/' . $theme_id . '.css';
        $PluginStaticUrl = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID)->getPluginStaticUrl();

        $data = array();
        if (file_exists($plugin_path . $js_path)) {
            $data['js'] = $PluginStaticUrl . $js_path;
        }
        if (file_exists($plugin_path . $css_path)) {
            $data['css'] = $PluginStaticUrl . $css_path;
        }

        return $data;

    }
}