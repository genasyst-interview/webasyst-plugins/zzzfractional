<?php

interface shopZzzfractionalPluginHooksInterface
{
    /**
     * @param shopProduct $product
     */
    public function backendProduct(&$product);

    /**
     * Преобразование складских остатков в дробный вид
     * После сохранения продукта и его остатков в базовой единице возрращаем в конвертированном виде для показа
     *
     * @param array $data - из хука
     */
    public function productSave(&$data);

    /**
     * Конвертация складов артикулов для запись в БД
     * Преобразует дробные остатки в базовую единицу измерения
     *
     * @param array $data - данные из хука
     */
    public function productPresave(&$data);

    /**
     * @param array $data - Данные продукта из хука
     *
     * @return array - Возращает html код для показаа при редактировании продукта
     * @throws waException
     */
    public function backendProductEdit(&$data);

    /**
     * Преобразование количества позиций заказа в эмулированный дробный вид
     *
     * @param array|shopOrder $order
     */
    public function backendOrderEdit(&$order);

    /**
     * Преобразование количества позиций заказа в эмулированный дробный вид
     *
     * @param array|shopOrder $order
     */
    public function backendOrder(&$order);

    /**
     * Добавление скриптов и стилей в <head> страниц
     */
    public function frontendHead();

    /**
     * Преобразование данных количества продуктов и skus в дробный вид
     *
     * @param array $data [products =>[], skus =>[]]
     */
    public function frontendProducts(&$data);

    /**
     * Метод инициализирует кратность покупки через js для карточки продукта
     * @uses getFrontendProductData()
     *
     * @param shopProduct|shopZzzfractionalPluginProductDecorator $product
     *
     * @return array - возвращает код html для вставки в корзину продукта
     */
    public function frontendProduct(&$product);

    /**
     * Метод инициализирует кратность покупки через js для позиций корзины
     * @return string - возвращает код html для вставки на странице корзины
     */
    public function frontendCart();

    /**
     * Метод добавляет информацию в БД о единице измерения позиции заказа
     *
     * @param $item - Массив данных позиции заказа
     */
    public function cartAdd(&$item);


}