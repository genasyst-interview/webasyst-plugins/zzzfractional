<?php

class shopZzzfractionalPluginProductDecorator extends shopProduct implements ArrayAccess
{

    protected $product = null;
    protected $unit_data = null;


    public function __construct($data = array(), $is_frontend = true)
    {
        if ($data instanceof shopProduct) {
            $this->product = $data;
        } elseif (is_array($data) && !empty($data['id'])) {
            $this->product = new shopProduct($data, $is_frontend);
        }
        shopZzzfractionalPluginStrategy::castProductUnit($this->product);
    }

    /**
     * @return shopZzzfractionalPluginStrategyInterface
     **/
    private function getStrategy()
    {
        return wao(new shopZzzfractionalPluginFactory())->getStrategy();
    }

    public function getUnit()
    {
        if ($this->unit_data == null) {
            $this->unit_data = shopZzzfractionalPluginDimension::getUnit($this->getData($this->getStrategy()->unitKey()));
        }

        return $this->unit_data;
    }

    public function getUnitType()
    {
        $unit = $this->getUnit();

        return array_key_exists('type', $unit) ? $unit['type'] : '';
    }

    public function getUnitId()
    {
        shopZzzfractionalPluginStrategy::castProductUnit($this);

        return $this->getData($this->getStrategy()->unitKey());
    }

    public function getUnitName()
    {
        $unit = $this->getUnit();

        return array_key_exists('name', $unit) ? $unit['name'] : '';
    }

    /*Use in hook Frontend Product */
    public function getProduct()
    {
        return $this->product;
    }

    public function getData($name = null)
    {
        if ($name) {
            $data = $this->product[$name];
            if ($name == 'skus') {
                $this->getStrategy()->decodeSkus($data);
            }
            if ($name == 'count') {
                $this->getStrategy()->decodeCount($data, $this->getUnitId());
            }

            return $data;
        } else {
            return $this->product->getData($name);
        }
    }

    public function offsetExists($offset)
    {
        return $this->product->offsetExists($offset);
    }

    public function offsetGet($offset)
    {
        return $this->getData($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->setData($offset, $value);
    }

    public function offsetUnset($offset)
    {
        $this->getProduct()->__set($offset, null);
    }

    public function __get($name)
    {
        $value = $this->product->__get($name);
        if ($name == 'skus') {
            $this->getStrategy()->decodeSkus($value);
        }
        if ($name == 'count') {
            $this->getStrategy()->decodeCount($data, $this->getUnit());
        }

        return $value;
    }

    public function __set($name, $value)
    {
        if ($name == 'skus') {
            $this->getStrategy()->decodeSkus($value);
        }

        return $this->product->__set($name, $value);
    }

    public function __call($name, $args)
    {
        if (method_exists($this->product, $name)) {
            return call_user_func_array(array($this->product, $name), $args);
        }

        return null;

    }

}