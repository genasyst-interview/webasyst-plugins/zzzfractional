<?php


class shopZzzfractionalPluginStrategyInstallerVersionAutoload extends shopZzzfractionalPluginStrategyInstallerVersion
{
    protected static $autoload_filepath = null;
    protected $autoload_clases = array();

    protected function init()
    {
        if (!self::$autoload_filepath) {
            self::$autoload_filepath = self::$app_config->getConfigPath('plugins/'
                . shopZzzfractionalPlugin::PLUGIN_ID . '/autoload.php');
        }
        parent::init();
    }

    public function install($version = null)
    {
        $this->autoload_clases = array();
        parent::install($version);
        $this->setAutoload($this->autoload_clases);
    }

    protected function offAllFiles()
    {
        $this->setAutoload(array());
    }

    protected function offFile($path, $error_path = '')
    {
        if (empty($error_path)) {
            $error_path = $path;
        }
        if ($this->isFile($path)) {
            $filename = basename($path);
            if (!preg_match('/^' . self::FILES_PREFIX . '/', $filename)) {
                $new_filepath = dirname($path) . '/' . self::FILES_PREFIX . $filename;
                if (!waFiles::move($path, $new_filepath)) {
                    $this->log('Не удалось выключить файл подмены! Нет прав на перезапись или файл не существует: ' . $error_path . '!');
                } else {
                    $this->log('Файл выключен ' . $error_path . '!');
                }
            }
        } else {
            $this->log('Файл не существует или нет прав на перезапись: ' . $error_path . '!');
        }
    }

    protected function onFile($path, $error_path = '')
    {
        if (empty($error_path)) {
            $error_path = $path;
        }
        if ($this->isFile($path)) {
            $filename = basename($path);
            $class_filename = preg_replace('/^' . self::FILES_PREFIX . '/', '', $filename);
            $class = $this->getClassByFilename($class_filename);
            $trim_path = ltrim(
                str_replace(waConfig::get('wa_path_root'), '', $path),
                '\\/');
            $this->autoload_clases[$class] = $trim_path;
        } else {
            $this->log('Файл не найден: ' . $error_path . '!');
        }
    }

    protected function getClassByFilename($filename)
    {
        $file_parts = explode('.', $filename);
        if (count($file_parts) <= 2) {
            return false;
        }
        $class = null;
        switch ($file_parts[1]) {
            case 'class':
                return $file_parts[0];
            default:
                $result = $file_parts[0];
                for ($i = 1; $i < count($file_parts) - 1; $i++) {
                    $result .= ucfirst($file_parts[$i]);
                }

                return $result;
        }
    }

    protected function setAutoload($data = array())
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }
        waFiles::create(self::$autoload_filepath);
        $res = waUtils::varExportToFile($data, self::$autoload_filepath, true);
        clearstatcache();

        return $res;
    }

    public function updatePlugin()
    {
        $this->cleanCache();
    }

    /**
     * Возвращает классы подмен для автолоадера
     **/
    public function getClasses()
    {
        $files = array();
        if (file_exists(self::$autoload_filepath)) {
            $files = include self::$autoload_filepath;
        }
        if (!is_array($files)) {
            $files = array();
        }

        return $files;
    }

    public function autoload()
    {
        $files = $this->getClasses();

        if (!empty($files)) {
            $autoload = waAutoload::getInstance();
            foreach ($files as $class_name => $path) {
                $autoload->add($class_name, $path);
            }
        }
    }

    public function getStrategyFiles($type = 'original', $status = 'on')
    {
        $files = array();
        if ($type == 'original') {
            $path = $this->getPluginPath(self::$strategy_app_original_path);
        } else {
            $path = $this->getPluginPath(self::$strategy_app_path);
        }
        $on_files = $this->getClasses();
        foreach (waFiles::listdir($path, true) as $filepath) {
            $full_filepath = $path . '' . $filepath;
            if ($this->isFile($full_filepath)) {
                $on = false;
                foreach ($on_files as $class => $cpath) {
                    if (stripos($full_filepath, $cpath)) {
                        $on = true;
                    }
                }
                if (($status == 'on' && $on) || ($status == 'off' && !$on)) {
                    $files[] = $filepath;
                }
            }
        }

        return $files;
    }

}

