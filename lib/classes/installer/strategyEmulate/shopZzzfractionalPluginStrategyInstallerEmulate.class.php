<?php


class  shopZzzfractionalPluginStrategyInstallerEmulate extends shopZzzfractionalPluginStrategyInstallerAbstract
{

    protected $strategy_type = 'emulate';

    public function __construct()
    {

    }

    public function install($force = false)
    {
        if ($this->getStrategyType() != shopZzzfractionalPluginConfig::STRATEGY_EMULATE) {
           /* $this->changeColumns(); для стратегии реального дробного! */
        }
    }

    public function uninstall($force = false)
    {
        return true;
    }

    public function installPlugin()
    {

    }

    public function uninstallPlugin()
    {
        shopZzzfractionalPluginStrategyInstallerVersion::factory()->uninstall();
    }


    private function changeColumns()
    {
        $sql = array();
        // Общее количество остатков артикулов со для продукта
        $sql[] = "ALTER TABLE  `shop_product` CHANGE COLUMN `count`  `count` INT(11) NULL DEFAULT NULL";
        // Количество артикула для всех складов общее
        $sql[] = "ALTER TABLE  `shop_product_skus` CHANGE COLUMN `count`  `count`  INT(11) NULL DEFAULT NULL";
        // Количество артикула по складам
        $sql[] = "ALTER TABLE  `shop_product_stocks` CHANGE COLUMN `count`  `count`  INT(11) NOT NULL";
        // Лог изменения остатков
        $sql[] = "ALTER TABLE  `shop_product_stocks_log` CHANGE COLUMN `before_count`  `before_count`  INT(11)  DEFAULT NULL";
        $sql[] = "ALTER TABLE  `shop_product_stocks_log` CHANGE COLUMN `after_count`  `after_count`  INT(11)  DEFAULT NULL";
        $sql[] = "ALTER TABLE  `shop_product_stocks_log` CHANGE COLUMN `diff_count`  `diff_count`  INT(11)  DEFAULT NULL";

        // Количество артикула в заказе
        $sql[] = "ALTER TABLE  `shop_cart_items` CHANGE COLUMN `quantity`  `quantity`  INT(11) NOT NULL DEFAULT '1'";
        // Количество артикула в заказе
        $sql[] = "ALTER TABLE  `shop_order_items` CHANGE COLUMN `quantity`  `quantity`  INT(11) NOT NULL";
        // Перемещение
        $sql[] = "ALTER TABLE  `shop_transfer_products` CHANGE COLUMN `count`  `count` INT(11) NOT NULL DEFAULT '0'";

        $this->execQuery($sql);
    }
}
