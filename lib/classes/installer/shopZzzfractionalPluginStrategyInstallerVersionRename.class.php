<?php


/**
 * @deprecated
 * Class shopZzzfractionalPluginStrategyInstallerVersionRename
 */
class shopZzzfractionalPluginStrategyInstallerVersionRename extends shopZzzfractionalPluginStrategyInstallerVersion
{

    protected function offFile($path, $error_path = '')
    {
        if (empty($error_path)) {
            $error_path = $path;
        }
        if ($this->isFile($path)) {
            $filename = basename($path);
            if (!preg_match('/^' . self::FILES_PREFIX . '/', $filename)) {
                $new_filepath = dirname($path) . '/' . self::FILES_PREFIX . $filename;
                if (!waFiles::move($path, $new_filepath)) {
                    $this->log('Не удалось выключить файл подмены! Нет прав на перезапись или файл не существует: ' . $error_path . '!');
                } else {
                    $this->log('Файл выключен ' . $error_path . '!');
                }
            }
        } else {
            $this->log('Файл не существует или нет прав на перезапись: ' . $error_path . '!');
        }
    }

    protected function onFile($path, $error_path = '')
    {
        if (empty($error_path)) {
            $error_path = $path;
        }
        if ($this->isFile($path)) {
            $filename = basename($path);
            if (preg_match('/^' . self::FILES_PREFIX . '/', $filename)) {
                $new_filepath = dirname($path) . '/' . preg_replace('/^' . self::FILES_PREFIX . '/', '', $filename);
                if (!waFiles::move($path, $new_filepath)) {
                    $this->log('Не удалось включить файл подмены! Нет прав на перезапись или файл не существует: ' . $error_path . '!');
                } else {
                    $this->log('Файл включен ' . $error_path . '!');
                }
            }
        } else {
            $this->log('Файл не существует или нет прав на перезапись: ' . $error_path . '!');
        }
    }

    public function updatePlugin()
    {
        $status = $this->getPluginStatus();
        $this->log('Плагин ' . ($status ? 'ключен' : 'выключен'));

        $this->log('Запущено обновление плагина!');
        $app_path = $this->getPluginPath(self::$strategy_app_original_path);
        $strategy_path = $this->getPluginPath(self::$strategy_app_path);
        // Выключаем все версии оригиналов файлов приложения
        foreach (waFiles::listdir($app_path, true) as $filepath) {
            $full_filepath = $app_path . '' . $filepath;
            $this->updateFile($full_filepath, $filepath);
        }
        // Выключаем все версии файлов стратегии
        foreach (waFiles::listdir($strategy_path, true) as $filepath) {
            $full_filepath = $strategy_path . '' . $filepath;
            $this->updateFile($full_filepath, $filepath);
        }

        if ($status) {
            $this->install();
        }
        $this->cleanCache();
    }

    protected function updateFile($path, $error_path = '')
    {
        if (empty($error_path)) {
            $error_path = $path;
        }
        if ($this->isFile($path)) {
            $filename = basename($path);
            /* Если файл активен и без префикса */
            if (!preg_match('/^' . self::FILES_PREFIX . '/', $filename)) {
                $new_filepath = dirname($path) . '/' . self::FILES_PREFIX . $filename;
                /* Если существует файл с префиксом значит старый активный файл можно удалить */
                if ($this->isFile($new_filepath)) {
                    if (!waFiles::delete($path)) {
                        $this->log('Не удалось удалить старый файл ' . $error_path . '!');
                    } else {
                        $this->log('Старый файл удален ' . $error_path . ' !');
                    }
                } else {
                    $this->log('Не существует нового файла,  ' . basename($new_filepath) . ', старый файл не удален!');
                }
            }
        }
    }

    public function getStrategyFiles($type = 'original', $status = 'on')
    {
        $files = array();
        if ($type == 'original') {
            $path = $this->getPluginPath(self::$strategy_app_original_path);
        } else {
            $path = $this->getPluginPath(self::$strategy_app_path);
        }

        foreach (waFiles::listdir($path, true) as $filepath) {
            $full_filepath = $path . '' . $filepath;
            if ($this->isFile($full_filepath)) {
                $filename = basename($filepath);
                /* Если файл активен и без префикса */
                $on = false;
                if (!preg_match('/^' . self::FILES_PREFIX . '/', $filename)) {
                    $on = true;
                }
                if (($status == 'on' && $on) || ($status == 'off' && !$on)) {
                    $files[] = $filepath;
                }
            }
        }

        return $files;
    }
}

