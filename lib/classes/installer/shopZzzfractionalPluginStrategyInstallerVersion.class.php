<?php

/**
 * Class shopZzzfractionalPluginStrategyInstallerVersion
 */
abstract class shopZzzfractionalPluginStrategyInstallerVersion
{
    /**
     * Префикс названий файлов в директориях подмен
     */
    const FILES_PREFIX = 'zzzfractional';

    /**
     * Путь к файлам подмен оригиналов приложения
     * @var string
     */
    protected static $strategy_app_original_path = '/lib/classes/app/original/';
    /**
     * Путь к фалам подмен декораторов классов приложения
     * @var string
     */
    protected static $strategy_app_path = '/lib/classes/app/strategyEmulate/';
    /**
     * Текущая установленаа версия подмены приложения
     * @var array
     */
    protected $current_version = array();
    /**
     * Статус плагина
     * @var null
     */
    protected static $status = null;
    /**
     * Поддерживаемые версии подмен прилождения
     * @var null
     */
    protected static $supported_app_versions = null;
    /**
     * Поддерживаемые версии подмен может отличаться от приложения
     * @var null
     */
    protected static $supported_strategy_versions = null;

    /**
     * Путь к файлу конфигу версии подмены
     * @see $current_version
     * @var null
     */
    protected static $version_filepath = null;
    /**
     * Флаг была ли выполнена проверка при загрузке
     * @var null
     */
    protected static $strategy_version_check = null;
    /**
     * Флаг были ли созданы переменные
     * @var bool
     */
    protected static $is_init = false;
    /**
     * @var waAppConfig|shopConfig
     **/
    protected static $app_config = null;

    /**
     * @param waAppConfig|shopConfig|null $config
     *
     * @return shopZzzfractionalPluginStrategyInstallerVersionAutoload|shopZzzfractionalPluginStrategyInstallerVersionRename
     */
    public static function factory(waAppConfig $config = null)
    {
        if ($config instanceof waAppConfig) {
            self::$app_config = $config;
        } else {
            if (self::$app_config instanceof waAppConfig) {
                $config = self::$app_config;
            } else {
                $config = wa('shop')->getConfig();
            }
        }
        /* Если метод включен, значит подмены включаем через хук и автолоадер */
        if (method_exists('shopZzzfractionalPlugin', 'appInit')) {
            $version_installer = new shopZzzfractionalPluginStrategyInstallerVersionAutoload($config);
            /* TODO:  @deprecated  На первое время всегда выключаем старую стратегию подключения файлов */
            $current_version = $version_installer->getInstalledVersion();
            if (class_exists('shopZzzfractionalPluginStrategyInstallerVersionRename')
                && (!array_key_exists('type', $current_version) || $current_version['type'] != 'autoload')
            ) {
                $rename_installer = new shopZzzfractionalPluginStrategyInstallerVersionRename($config);
                $rename_installer->uninstall();
            }

            return $version_installer;
        } else {
            return new shopZzzfractionalPluginStrategyInstallerVersionRename($config);
        }
    }

    /**
     * shopZzzfractionalPluginStrategyInstallerVersion constructor.
     *
     * @param waAppConfig $config
     */
    public function __construct(waAppConfig $config)
    {
        if (!(self::$app_config instanceof waAppConfig)) {
            self::$app_config = $config;
        }
        $this->init();
    }

    /**
     * Действия при обновлении плагина
     * @return mixed
     */
    abstract public function updatePlugin();

    /**
     * Вкл, выкл
     * @return bool
     */
    public function getPluginStatus()
    {
        $settings_key = array('shop', shopZzzfractionalPlugin::PLUGIN_ID);
        $app_settings_model = new waAppSettingsModel();

        return (bool)$app_settings_model->get($settings_key, 'status');
    }

    /**
     *  Включение, выключение подмены файлов
     */
    public function run()
    {
        if (self::$strategy_version_check === null) {
            $status = $this->getPluginStatus();
            $this->log('Плагин  ' . ($status ? 'Включен' : 'выключен'));
            if ($status) {
                if (!$this->check()) {
                    $this->install();
                }
            } else {
                $this->uninstall();
            }
            self::$strategy_version_check = true;
        }
    }

    /**
     * Установка стратегии подмены париложения
     *
     * @param null $version
     */
    public function install($version = null)
    {
        $this->log('Запущена установка актульной версии плагина для приложения!');

        if (!is_array($version) || !array_key_exists('strategy', $version)) {
            $version = $this->getActualVersion();
        } else {
            if (!array_key_exists('app', $version)) {
                $version['app'] = $this->getActualAppVersion($version['strategy']);
            }
        }

        $current_install_version = $this->getInstalledVersion();
        $this->log('Версия приложения: ' . $this->getRealAppVersion() . '!');
        $this->log('Установленные версии: приложение (' . $current_install_version['app'] . '), плагин (' . $current_install_version['strategy'] . ')!');
        $this->log('Актуальные версии: приложение (' . $version['app'] . '), плагин (' . $version['strategy'] . ')!');
        /* if($this->getRealAppVersion() != $current_install_version['app'] || $version != $current_install_version['strategy']) { */
        $this->uninstall();
        /* Включаем версию оригиналов файлов приложения */
        $this->log('Включаем версию оригиналов файлов приложения (' . $version['app'] . ')!');
        $this->onAppFilesVersion($version['app']);
        $this->log('Включаем версию файлов стратегии (' . $version['strategy'] . ')!');
        /* Включаем версию файлов стратегии */
        $this->onStrategyFilesVersion($version['strategy']);
        /* Включаем файлы по версиям */
        $this->onSvnFiles($version['app']);

        $this->setVersion($this->getRealAppVersion(), $version['strategy']);
        /* } */
        $this->cleanCache();

    }

    /**
     *  Выключение подмены приложения
     */
    public function uninstall()
    {
        $this->log('Запущено выключение текущей версии подмены приложения в плагине...');
        $this->offAllFiles();
        $this->setVersion('0.0.1', '0.0.1');
        $this->log('Подмены приложения выключены!');
        $this->cleanCache();
    }

    /**
     * Возвращает версию приложения с билдом!!!
     * @return string
     */
    public function getRealAppVersion()
    {
        try {
            $path = self::$app_config->getAppPath('lib/config/');
            $app = include($path . 'app.php');
            $version = $app['version'];
            if (file_exists($path . 'build.php')) {
                $version .= '.' . include($path . 'build.php');
            }

            return $version;
        } catch (waException $e) {
            $this->log($e->getMessage());
        }

        return '0.0.1';
        /* тут жесткое кеширование return wa()->getVersion('shop'); */
    }

    /**
     * Возвращает версии оригиналов подмен приложения
     * @return array|null
     */
    public function getAppVersions()
    {
        if (self::$supported_app_versions == null) {
            self::$supported_app_versions = array();
            foreach (waFiles::listdir($this->getPluginPath(self::$strategy_app_original_path)) as $version) {
                self::$supported_app_versions[$version] = $version;
            }
            usort(self::$supported_app_versions, function ($a, $b) {
                return version_compare($a, $b, '>=');
            });
        }

        return self::$supported_app_versions;
    }

    /**
     * Возвращает версии декораторов оригиналов подмен приложения
     * @return array|null
     */
    public function getStrategyVersions()
    {
        if (self::$supported_strategy_versions == null) {
            self::$supported_strategy_versions = array();
            foreach (waFiles::listdir($this->getPluginPath(self::$strategy_app_path)) as $version) {
                self::$supported_strategy_versions[$version] = $version;
            }
            usort(self::$supported_strategy_versions, function ($a, $b) {
                return version_compare($a, $b, '>=');
            });
        }

        return self::$supported_strategy_versions;
    }

    /**
     * Возвращает актуальные ниболее близкие версии подмен для приложени
     * @return array
     */
    public function getActualVersion()
    {
        return array(
            'app'      => $this->getActualAppVersion(),
            'strategy' => $this->getActualStrategyVersion(),
        );
    }

    /**
     * Возвращает установленную версию подмен
     * @return array
     */
    public function getInstalledVersion()
    {
        $version = array(
            'app'      => '0.0.1',
            'strategy' => '0.0.1',
            'type'     => null,
        );
        if (is_array($this->current_version)) {
            foreach ($version as $k => $v) {
                if (array_key_exists($k, $this->current_version)) {
                    $version[$k] = $this->current_version[$k];
                }
            }
        }

        return $version;
    }

    /**
     * Создание переменных
     */
    protected function init()
    {
        if (!self::$is_init) {
            self::$version_filepath = self::$app_config->getConfigPath('plugins/' .
                shopZzzfractionalPlugin::PLUGIN_ID . '/strategy_version.php');
            self::$strategy_app_path = '/lib/classes/app/strategy' .
                ucfirst(shopZzzfractionalPluginStrategy::getStrategyType()) . '/';
            self::$is_init = true;
        }
        if (file_exists(self::$version_filepath)) {
            $this->current_version = include self::$version_filepath;
        }
    }

    /**
     * Проверка версии приложения и подмен, если приложение обновилось или плагин выключен,
     * будет переустановка версии подмен или выключение
     *
     * @return bool
     */
    protected function check()
    {
        $version = $this->getActualVersion();
        $current_install_version = $this->getInstalledVersion();
        $result = ($this->getRealAppVersion() == $current_install_version['app'] && $version['strategy'] == $current_install_version['strategy']);
        /* TODO:  @deprecated */
        if (method_exists('shopZzzfractionalPlugin', 'appInit')) {
            if (
                ($this instanceof shopZzzfractionalPluginStrategyInstallerVersionAutoload)
                && (!array_key_exists('type', $current_install_version)
                    || $current_install_version['type'] != 'autoload')
            ) {
                $result = false;
            }
        }
        $this->log('Проверка версии плагина и приложения - ' . ($result ? ' Ок' : ' Неактуальная версия!  ') . "\n" .
            'Версия приложения: ' . $this->getRealAppVersion() . "\n" .
            'Установленная версия подмены файлов приложения: ' . $current_install_version['app'] . "\n" .
            'Установленная версия стратегии: ' . $current_install_version['strategy'] . "\n"
        );

        return $result;
    }

    /**
     * Включение версии подмен оригиналов приложения
     *
     * @param string $version
     */
    protected function onAppFilesVersion($version = '')
    {
        if (!empty($version)) {
            $strategy_app_original_path =
                $this->getPluginPath(rtrim(self::$strategy_app_original_path, '\\/') . '/' . $version . '/');
            foreach (waFiles::listdir($strategy_app_original_path, true) as $filepath) {
                $filepath_full = $strategy_app_original_path . '' . ltrim($filepath, '\\/');
                $this->onFile($filepath_full, $filepath);
            }
        }
    }

    /**
     * Включение версии декораторов оригиналов приложения
     *
     * @param string $version
     */
    protected function onStrategyFilesVersion($version = '')
    {
        if (!empty($version)) {
            $strategy_version_dir = $this->getPluginPath(rtrim(self::$strategy_app_path, '\\/') . '/' . $version . '/');
            foreach (waFiles::listdir($strategy_version_dir, true) as $filepath) {
                $filepath_full = $strategy_version_dir . '' . ltrim($filepath, '\\/');
                $this->onFile($filepath_full, $filepath);
            }
        }
    }

    /**
     * Выключение подмены приложения
     */
    protected function offAllFiles()
    {
        // Выключаем все версии оригиналов файлов приложения
        $this->offAppFiles();
        // Выключаем все версии файлов стратегии
        $this->offStrategyFiles();
    }

    /**
     * Выключение подмены приложения
     */
    protected function offAppFiles()
    {
        $app_path = $this->getPluginPath(self::$strategy_app_original_path);
        if (!file_exists($app_path)) {
            $this->log('не существует: ' . $app_path . '!');
        }
        foreach (waFiles::listdir($app_path, true) as $filepath) {
            $full_filepath = $app_path . '' . $filepath;
            $this->offFile($full_filepath, $filepath);
        }
    }

    /**
     * Выключение подмены приложения
     */
    protected function offStrategyFiles()
    {
        $strategy_path = $this->getPluginPath(self::$strategy_app_path);
        if (!file_exists($strategy_path)) {
            $this->log('не существует: ' . $strategy_path . '!');
        }
        foreach (waFiles::listdir($strategy_path, true) as $filepath) {
            $full_filepath = $strategy_path . '' . $filepath;
            $this->offFile($full_filepath, $filepath);
        }
    }

    /**
     * Запись версии в конфиг
     *
     * @param string $app_version
     * @param string $version
     *
     * @return bool
     */
    protected function setVersion($app_version = '', $version = '')
    {
        $this->current_version = array(
            'app'      => $app_version,
            'strategy' => $version,
        );
        if ($this instanceof shopZzzfractionalPluginStrategyInstallerVersionRename) {
            $this->current_version['type'] = 'rename';
        } else {
            $this->current_version['type'] = 'autoload';
        }
        waFiles::create(self::$version_filepath);

        return waUtils::varExportToFile($this->current_version, self::$version_filepath, true);
    }

    /**
     * Выключает файл подмены по файловому пути
     *
     * @param string $path
     * @param string $error_path
     */
    abstract protected function offFile($path, $error_path = '');

    /**
     * Включает файл подмены по файловому пути
     *
     * @param string $path
     * @param string $error_path
     */
    abstract protected function onFile($path, $error_path = '');

    /**
     * Возвращает файлы которые надо включить для переданной версии подмены
     *
     * @param string $version
     *
     * @return array|mixed
     */
    protected function getSnvVersionFiles($version = '0.0.1')
    {
        return shopZzzfractionalPluginConfig::getSvnFiles($version);
    }

    /**
     * Подключает не измененные файлы для кекущей версии подмены из более стаых версий
     * что то вроде svn...)))
     *
     * @param string $app_version
     */
    protected function onSvnFiles($app_version = '0.0.1')
    {
        $files = $this->getSnvVersionFiles($app_version);
        if (is_array($files) && !empty($files)) {
            $app_path = $this->getPluginPath(self::$strategy_app_original_path);
            $strategy_path = $this->getPluginPath(self::$strategy_app_path);
            foreach ($files as $file => $data) {
                $data = $this->getSvnFileLinks($file, $data);
                if (is_array($data)) {
                    if (array_key_exists('app', $data) && !empty($data['app'])) {
                        $full_filepath = $app_path . ltrim($data['app'], '\\/');
                        $this->onFile($full_filepath, $data['app']);
                    }
                    if (array_key_exists('strategy', $data) && !empty($data['strategy'])) {
                        $full_filepath = $strategy_path . ltrim($data['strategy'], '\\/');
                        $this->onFile($full_filepath, $data['strategy']);
                    }
                } else {
                    $this->log('Для файла (' . $file . ') версии приложения (' . $app_version . '),
                     не найдены ранние версии описанные в конфигурации! Напишите разработчику плагина!');
                }
            }
        } else {
            $this->log('Нет конфигурации версий файлов для версии приложения ' . $app_version . '!');
        }
    }

    /**
     * Возвращает реальную директорию подключаемомго файла
     * в конфиге может быть указана ссылка на версию из которой взять данные для файла ('shopOrder' => '7.4.3.238')
     * В таком  случае будет запрошен конфиг той версии и оттуда взята информация о путях файла подмены
     *
     * @param $file_key
     * @param $data
     *
     * @return null
     */
    protected function getSvnFileLinks($file_key, $data)
    {
        if (is_array($data)) {
            return $data;
        } else {
            $file_data = $this->getSnvVersionFile($file_key, $data);
            if ($file_data) {
                return $this->getSvnFileLinks($file_key, $file_data);
            } else {
                return null;
            }
        }
    }

    /**
     * Возвращает данные файлов подмен класса по его названию и нужной версии
     *
     * @param $file_key
     * @param $version
     *
     * @return mixed|null
     */
    protected function getSnvVersionFile($file_key, $version)
    {
        $link_version_files = $this->getSnvVersionFiles($version);
        if (is_array($link_version_files) && array_key_exists($file_key, $link_version_files)) {
            return $link_version_files[$file_key];
        }

        return null;
    }

    /**
     * @param $path
     *
     * @return bool
     */
    protected function isFile($path)
    {
        return (file_exists($path) && is_file($path));
    }

    /**
     * Находит наиболее подходящую версию подмен оригиналов приложения
     *
     * @param null $max_version
     *
     * @return null
     */
    protected function getActualAppVersion($max_version = null)
    {
        if (!$max_version) {
            $max_version = $this->getRealAppVersion();
        }

        return $this->selectVersion($max_version, $this->getAppVersions());
    }

    /**
     * Находит наиболее подходящую версию подмен декораторов оригиналов приложения
     * @return null
     */
    protected function getActualStrategyVersion()
    {
        return $this->selectVersion($this->getRealAppVersion(), $this->getStrategyVersions());
    }

    /**
     * Ищет наиболее похожие версии подмен для версии приложения
     *
     * @param $version
     * @param $versions
     *
     * @return null
     */
    protected function selectVersion($version, $versions)
    {
        $last_supported_version = null;
        foreach ($versions as $v) {
            $compare = version_compare($v, $version);
            if ($compare <= 0) {
                $last_supported_version = $v;
            }
            if ($compare >= 0) {
                break;
            }
        }

        return $last_supported_version;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function getPluginPath($path = '')
    {
        return self::$app_config->getAppPath('plugins/' . shopZzzfractionalPlugin::PLUGIN_ID . '/' . ltrim($path, '/\\'));
    }

    /**
     * Очищает кеш весь
     * @throws Exception
     */
    protected function cleanCache()
    {
        try {
            $path_cache = waConfig::get('wa_path_cache');
            waFiles::protect($path_cache);

            $caches = array();
            $paths = waFiles::listdir($path_cache);
            foreach ($paths as $path) {
                #skip long action & data path
                if ($path != 'temp') {
                    $path = $path_cache . '/' . $path;
                    if (is_dir($path)) {
                        $caches[] = $path;
                    }
                }
            }

            $caches[] = $path_cache . '/temp';
            $root_path = self::$app_config->getRootPath();
            foreach ($caches as $path) {
                try {
                    waFiles::delete($path);
                } catch (Exception $ex) {
                    $this->log(str_replace($root_path . DIRECTORY_SEPARATOR, '', $ex->getMessage()));
                    waFiles::delete($path, true);
                }
            }

            if (function_exists('opcache_reset')) {
                @opcache_reset();
            }
        } catch (waException $ex) {
            $this->log($ex->getMessage());
        }
    }

    /**
     * @param string $message
     * @param bool   $force
     */
    protected function log($message = '', $force = false)
    {
        if (self::$app_config->isDebug() || $force) {
            $message = get_class($this) . ' : ' . $message;
            shopZzzfractionalPlugin::log($message);
        }
    }


    /**
     * Для использования в отладочных экшенах
     * Возвращает включенные или выключеенные файлы подмен
     *
     * @param string $type
     * @param string $status
     *
     * @return array
     */
    abstract public function getStrategyFiles($type = 'original', $status = 'on');


}

