<?php


/**
 * Class shopZzzfractionalPluginStrategyInstallerAbstract
 */
abstract class  shopZzzfractionalPluginStrategyInstallerAbstract
{


    /**
     * Метод установки стратегии дробного количества
     *
     * @param bool $force
     */
    abstract public function install($force = false);

    /**
     * Метод выключения страгегии дробного количества
     *
     * @param bool $force
     */
    abstract public function uninstall($force = false);

    /**
     * При установке плагина добавляются колонки и ставится стратеги
     * @throws Exception
     */
    abstract public function installPlugin();

    /**
     * ПРи удалении плагина, удаляются колонки из бд, а также отрабатывает отключение стратегии
     * @throws Exception
     */
    abstract public function uninstallPlugin();

    /**
     * Выполняет один запрос или несколько запросов к бд
     *
     * @param array $query
     */
    protected function execQuery($query = array())
    {
        if (!is_array($query)) {
            $query = (array)$query;
        }
        $model = new waModel();
        foreach ($query as $v) {
            try {
                $model->exec($v);
            } catch (Exception $e) {
                waLog::log($e->getMessage(), 'db.log');
            }
        }
    }

    /**
     * @param bool|string $type
     *
     * @return shopZzzfractionalPluginStrategyInstallerAbstract
     * @throws Exception
     */
    protected function getStrategyInstaller($type = false)
    {
        if (!$type) {
            $type = $this->getStrategyType();
        }
        $class = 'shopZzzfractionalPluginStrategyInstaller' . ucfirst($type);
        if (class_exists($class)) {
            return new $class();
        } else {
            waLog::log('Нет установщика стратегии ' . $class, 'zzzfractional_strategy.log');
            throw new Exception('Нет установщика стратегии ' . $class);
        }
    }

    /**
     * Возвращает тип стратегии дробного количества (реальный и эмуляция)
     * @return string
     */
    protected function getStrategyType()
    {
        return shopZzzfractionalPluginStrategy::getStrategyType();
    }

}
