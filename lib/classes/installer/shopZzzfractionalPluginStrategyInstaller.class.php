<?php


/**
 * Class shopZzzfractionalPluginStrategyInstaller
 */
class  shopZzzfractionalPluginStrategyInstaller extends shopZzzfractionalPluginStrategyInstallerAbstract
{

    /**
     * Метод установки стратегии дробного количества
     * @param bool $force
     */
    public function install($force = false)
    {
    }

    /**
     * Метод выключения страгегии дробного количества
     * @param bool $force
     */
    public function uninstall($force = false)
    {
    }

    /**
     * При установке плагина добавляются колонки и ставится стратеги
     * @throws Exception
     */
    public function installPlugin()
    {
        $model = new waModel();
        $sql = array();
        $unit_key = shopZzzfractionalPluginConfig::ENTITY_UNIT_KEY;
        $unit_multiplicity_key = shopZzzfractionalPluginConfig::ENTITY_UNIT_MULTIPLICITY_KEY;
        try {
            $model->query('SELECT ' . $unit_key . ' FROM shop_product WHERE 0');
        } catch (waDbException $e) {
            $sql[] = "ALTER TABLE shop_product ADD " . $unit_key . "  VARCHAR(32) NOT NULL DEFAULT '" . shopZzzfractionalPlugin::DEFAULT_UNIT . "'";
        }
        try {
            $model->query('SELECT ' . $unit_multiplicity_key . ' FROM shop_product WHERE 0');
        } catch (waDbException $e) {
            $sql[] = "ALTER TABLE shop_product ADD " . $unit_multiplicity_key . " VARCHAR(32) NOT NULL DEFAULT '" . shopZzzfractionalPlugin::DEFAULT_UNIT_MULTIPLICITY . "'";
        }

        try {
            $model->query('SELECT ' . $unit_key . ' FROM shop_cart_items WHERE 0');
        } catch (waDbException $e) {
            $sql[] = "ALTER TABLE shop_cart_items ADD " . $unit_key . "  VARCHAR(32) NOT NULL DEFAULT '" . shopZzzfractionalPlugin::DEFAULT_UNIT . "'";
        }
        try {
            $model->query('SELECT ' . $unit_key . ' FROM shop_order_items WHERE 0');
        } catch (waDbException $e) {
            $sql[] = "ALTER TABLE shop_order_items ADD " . $unit_key . "  VARCHAR(32) NOT NULL DEFAULT '" . shopZzzfractionalPlugin::DEFAULT_UNIT . "'";
        }
        try {
            $model->query('SELECT ' . $unit_key . ' FROM shop_product_stocks_log WHERE 0');
        } catch (waDbException $e) {
            $sql[] = "ALTER TABLE shop_product_stocks_log ADD " . $unit_key . "  VARCHAR(32) NOT NULL DEFAULT '" . shopZzzfractionalPlugin::DEFAULT_UNIT . "'";
        }

        $this->execQuery($sql);

        $strategy = $this->getStrategyInstaller();
        $strategy->installPlugin();

    }

    /**
     * ПРи удалении плагина, удаляются колонки из бд, а также отрабатывает отключение стратегии
     * @throws Exception
     */
    public function uninstallPlugin()
    {
        $model = new waModel();
        $sql = array();
        /* Удаляем стратегию */
        $strategy = $this->getStrategyInstaller();
        $strategy->uninstallPlugin();
        /* Удаляем добавленные колонки */
        $unit_key = shopZzzfractionalPluginConfig::ENTITY_UNIT_KEY;
        $unit_multiplicity_key = shopZzzfractionalPluginConfig::ENTITY_UNIT_MULTIPLICITY_KEY;
        try {
            $model->query("SELECT " . $unit_key . " FROM shop_product WHERE 0");
            $sql[] = "ALTER TABLE shop_product DROP " . $unit_key . "";
        } catch (waException $e) {
        }
        try {
            $model->query("SELECT " . $unit_multiplicity_key . " FROM shop_product WHERE 0");
            $sql[] = "ALTER TABLE shop_product DROP " . $unit_multiplicity_key . "";
        } catch (waException $e) {
        }

        try {
            $model->query('SELECT ' . $unit_key . ' FROM shop_product_stocks_log WHERE 0');
            $sql[] = "ALTER TABLE shop_product_stocks_log DROP " . $unit_key . "";
        } catch (waDbException $e) {
        }
        try {
            $model->query('SELECT ' . $unit_key . ' FROM shop_cart_items WHERE 0');
            $sql[] = "ALTER TABLE shop_cart_items DROP " . $unit_key . "";
        } catch (waDbException $e) {
        }
        try {
            $model->query('SELECT ' . $unit_key . ' FROM shop_order_items WHERE 0');
            $sql[] = "ALTER TABLE shop_order_items DROP " . $unit_key . "";
        } catch (waDbException $e) {
        }
        $this->execQuery($sql);

    }

}
