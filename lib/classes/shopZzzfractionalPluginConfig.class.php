<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/shopZzzfractionalPluginConfig.trait.php', 'shop'));

class shopZzzfractionalPluginConfig
{
    use shopZzzfractionalPluginConfigTrait;
    /**
     * Стратегия эмуляции дробного за счет конвертации
     */
    const STRATEGY_EMULATE = 'emulate';
    /**
     * Стратегия реального дробного ,в бд все храниться в decimal
     */
    const STRATEGY_REAL = 'real';

    /**
     * Параметр для управления конвертацией
     */
    const PARAM_CONVERT = 'zzzfractional_convert';
    /**
     * Параметр для управления конвертацией
     */
    const RETURN_VALUE_TYPE = 'zzzfractional_return_value_type';
    /**
     * Параметр для управления конвертацией
     */
    const RETURN_VALUE_TYPE_STRING = 'string';
    /**
     * Параметр для управления конвертацией
     */
    const RETURN_VALUE_TYPE_NUMERIC = 'numeric';

    /**
     * Ячейка данных хранения единицы измерения
     */
    const ENTITY_UNIT_KEY = 'unit';
    /**
     *  Ячейка данных хранения кратности единицы измерения
     */
    const ENTITY_UNIT_MULTIPLICITY_KEY = 'unit_multiplicity';
    /**
     *  Ячейка данных хранения веса позиции заказа
     */
    const ENTITY_WEIGHT_KEY = 'zzzfractional_weight';

    /**
     * Ячейка данных хранения типа конвертации , для предотвращения двойного срабатывания конвертации
     * [zzzfractional_emulate] = encoded || decoded
     */
    const EMULATE_ATTR_KEY = 'zzzfractional_emulate';

    /**
     * @var null
     */
    protected static $convert_config = null;

    /**
     * @var null
     */
    protected static $settings = null;
    /* protected static $_instance = null; php bug */

    /**
     * @return shopZzzfractionalPluginStrategyConfig
     */


    /**
     * @return array
     */
    protected function getConvertConfigArray()
    {
        return array(
            'frontendcartadd::post'    => array(
                'encodeandcastcountbyproduct' => array(
                    'shopcart::setquantity' => false,
                ),
            ),
            'frontendcartsave::post'   => array(
                'encodeandcastcountbyproduct' => array(
                    'shopcart::setquantity' => false,
                ),
            ),
            'frontendcartdelete::post' => array(
                'decodeitemscount' => array(
                    'shopcart::getitemtotal' => false,
                ),
            ),
            'frontendcart::post'       => array(
                'encodeandcastcountbyproduct' => array(
                    'shopcart::setquantity' => false,
                ),
            ),
            'frontendcheckout::post'   => array(
                'decodeitemscount' => array(
                    'shopworkflowcreateaction::execute' => false,
                ),
            ),
            'ordersave::post'          => array(
                'decodeitemscount' => array(
                    'shopordersavecontroller::execute' => false,
                ),
            ),
        );
    }

    /**
     * @return bool
     */
    public function getStrategyClassName()
    {
        return false;
    }

    public static function getSvnFiles($app_version = '0.0.1')
    {
        $config = self::getSvnConfig();
        if (array_key_exists($app_version, $config) && is_array($config[$app_version])) {
            return $config[$app_version];
        }

        return array();
    }

    protected static function getSvnConfig()
    {
        static $config = null;

        if ($config == null) {
            $config = array();
            $file_path =
                wa('shop')->getAppPath('plugins/' . shopZzzfractionalPlugin::PLUGIN_ID, 'shop') . '/lib/config/svn.php';

            if (file_exists($file_path)) {
                $data = include($file_path);
                if (is_array($data)) {
                    $config = $data;
                }
            }
        }

        return $config;
    }

    /**
     * @return array|null
     */
    public function getConvertConfig()
    {
        if (self::$convert_config === null) {
            self::$convert_config = $convert_config = array();
            $convert_config = $this->getConvertConfigArray();
            wa('shop')->event('zzzfractional_convert_config', $convert_config);
            if (is_array($convert_config)) {
                self::$convert_config = $convert_config;
            }
        }

        return self::$convert_config;
    }

    /**
     * @param null $name
     *
     * @return array|mixed|null|string
     * @throws waException
     */
    public function getSetting($name = null)
    {
        if (self::$settings == null) {
            self::$settings = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID)->getSettings();
        }
        if ($name == null) {
            return self::$settings;
        } elseif (!empty($name) && array_key_exists($name, self::$settings)) {
            return self::$settings[$name];
        }

        return null;
    }

    /**
     * @return bool
     */
    public function multiplicityError()
    {
        return ($this->getSetting('multiplicity_error') == 1);
    }

    /**
     * @return bool
     */
    public function multiplicityConversion()
    {
        $conversion = $this->getSetting('multiplicity_conversion');

        return ($conversion == 2 || ($conversion == 1 && wa()->getEnv() == 'frontend'));
    }

    /**
     * @return string
     */
    public function unit()
    {
        return self::ENTITY_UNIT_KEY;
    }

    /**
     * @return string
     */
    public function multiplicity()
    {
        return self::ENTITY_UNIT_MULTIPLICITY_KEY;
    }

    /**
     * @return string
     */
    public function emulateAttr()
    {
        return self::EMULATE_ATTR_KEY;
    }

    /**
     * @return string
     */
    public function weight()
    {
        return self::ENTITY_WEIGHT_KEY;
    }
}