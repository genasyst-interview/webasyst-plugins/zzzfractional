<?php


class shopZzzfractionalPluginFactory
{

    protected static $strategy_type = 'emulate';
    protected static $_instance = null;
    protected static $factories = array();
    protected static $status = false;
    protected static $init = false;

    public function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        if (!self::$init) {
            self::$strategy_type = shopZzzfractionalPluginStrategy::getStrategyType();
            self::$status = (bool)wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID)->getSettings('status');
            self::$init = true;
        }
    }

    /**
     * @return shopZzzfractionalPluginStrategyEmulate
     */
    public function getStrategy()
    {
        if (!array_key_exists('strategy', self::$factories)) {
            $class = false;
            if (class_exists('shopZzzfractionalPluginStrategyConfig')) {
                $config = shopZzzfractionalPluginStrategyConfig::init();
                $class = $config->getStrategyClassName();
            }
            if (!$class) {
                $class = 'shopZzzfractionalPluginStrategy' . ucfirst(self::$strategy_type);
            }
            if (class_exists($class)) {
                self::$factories['strategy'] = new $class();
            } else {
                throw new \Exception('Нет класса ' . $class);
            }
        }

        return self::$factories['strategy'];
    }


}

