<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopOrderEditAction extends shopZzzfractionalPluginOrderEditAction
{
    use shopZzzfractionalPluginAppTrait;

    protected function getOrderData($order)
    {
        $order_id = $order['id'];
        if (!$order_id) {
            throw new waException("Order not found", 404);
        }

        $order_data = $order->dataArray();
        $order_data['contact'] = $order->contact_essentials;
        $order_data['shipping_id'] = $order['shipping_id'];
        $order_data['subtotal'] = $order['subtotal'];
        $order_data['items'] = $order['items_extended'];

        // Gather sku_ids to fetch stocks
        $sku_ids = array();
        foreach ($order_data['items'] as $item) {
            foreach ($item['skus'] as $sku) {
                if (empty($sku['fake'])) {
                    $sku_ids[] = $sku['id'];
                }
            }
        }

        // How many of each SKU are left in stock
        $sku_stocks = array();
        if ($sku_ids) {
            $product_stocks_model = new shopProductStocksModel();
            $sku_stocks = $product_stocks_model->getBySkuId($sku_ids);
        }

        // Weight of each SKU
        $weights = array();
        $feature_model = new shopFeatureModel();
        $f = $feature_model->getByCode('weight');
        if ($f) {
            $product_ids = array_values(waUtils::getFieldValues($order_data['items'], 'id', 'id'));
            $values_model = $feature_model->getValuesModel($f['type']);
            $weights = $values_model->getProductValues($product_ids, $f['id']);
        }
        $this->getStrategy()->decodeItemsCount($order_data['items']);
        $this->getStrategy()->addItemsData($order_data['items']);
        $weight_key = $this->weightKey();
        foreach ($order_data['items'] as &$item) {
            $this->workupItems($item, $sku_stocks);
            if (isset($weights['skus'][$item['item']['sku_id']])) {
                $w = $weights['skus'][$item['item']['sku_id']];
            } else {
                $w = isset($weights[$item['id']]) ? $weights[$item['id']] : 0;
            }
            if(array_key_exists($weight_key,  $item)) {
                $w =  $item[$weight_key];
            }
            $item['weight'] = $w;
        }
        unset($item);

        return $order_data;
    }

    public function getPluginRoot() {
        return wa()->getAppPath('/');
    }
}

