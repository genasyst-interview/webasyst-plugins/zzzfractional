<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopCsvProductrunController extends shopZzzfractionalPluginCsvProductrunController
{
    use shopZzzfractionalPluginAppTrait;

    protected function findProduct(&$data)
    {
        static $currencies;
        static $model;
        static $sku_model;
        /**
         * @var shopTypeFeaturesModel $type_features_model
         */
        static $type_features_model;
        if (empty($model)) {
            $model = new shopProductModel();
        }
        if (empty($currencies)) {
            $currencies = array();
            $config = wa()->getConfig();
            /**
             * @var shopConfig $config
             */
            $c = $config->getCurrency();
            $currencies[$c] = $c;
            foreach ($config->getCurrencies() as $row) {
                $currencies[$row['code']] = $row['code'];
            }
        }

        if (!empty($data['skus'][-1]['stock'])) {
            $per_stock = false;
            $stock =& $data['skus'][-1]['stock'];
            foreach ($stock as $id => & $count) {
                if ($count === '') {
                    $count = null;
                } else {
                    $count = $count;/* Не надо приводить к целому дробные остатки! */
                    if ($id) {
                        $per_stock = true;
                    }
                }
                unset($count);
            }

            if ($per_stock) {
                if (isset($stock[0])) {
                    unset($stock[0]);
                }
            } else {
                $count = ifset($stock[0]);
                $stock = array(
                    0 => $count,
                );
                unset($count);
            }
            unset($stock);

        }

        $stack = ifset($this->data['map'][self::STAGE_CATEGORY], array());
        $category_id = end($stack);
        if (!$category_id) {
            $category_id = null;
        }
        $primary = $this->data['primary'];
        $fields = false;
        if (empty($primary)) {
            $keys = explode(':', $this->data['secondary']);
            if (empty($sku_model)) {
                $sku_model = new shopProductSkusModel();
            }
            $sku_fields = array(
                end($keys) => self::getData($data, $keys),
            );
            //hack for empty SKU code ???
            if (false && (reset($sku_fields) === '') && $this->data['extra_secondary']) {
                $extra_keys = explode(':', $this->data['extra_secondary']);
                $sku_fields[end($extra_keys)] = self::getData($data, $this->data['extra_secondary']);
            }
            if ($sku = $sku_model->getByField($sku_fields)) {
                $fields = array(
                    'category_id' => $category_id,
                    'id'          => $sku['product_id'],
                );
            }

        } elseif (!empty($primary)) {
            $fields = array(
                'category_id' => $category_id,
                $primary      => ifset($data[$primary], ''),
            );
        }

        if ($fields && $this->data['ignore_category']) {
            unset($fields['category_id']);
        }

        $key = 'p';

        // nl2br for description
        if ($this->data['nl2br_description']) {
            if (!empty($data['description'])) {
                $data['description'] = nl2br($data['description']);
            }
            if (!empty($data['summary'])) {
                $data['summary'] = nl2br($data['summary']);
            }
        }

        if ($fields && ($current_data = $model->getByField($fields))) {
            $product = new shopProduct($current_data['id']);
            $data['type_id'] = ifempty($current_data['type_id'], $this->data['type_id']);
            if (!empty($current_data['tax_id'])) {
                $data['tax_id'] = $current_data['tax_id'];
            }
            if (isset($data['currency']) && !isset($currencies[$data['currency']])) {
                $this->data['processed_count'][self::STAGE_PRODUCT]['currency']++;
                $data['currency'] = reset($currencies);
            }
            if (!empty($data['skus'])) {
                $data['sku_id'] = ifempty($current_data['sku_id'], -1);
            }
            foreach ($product->skus as $sku_id => $current_sku) {
                if (empty($data['skus'][$sku_id])) {
                    if (!count($current_sku['stock']) && ($current_sku['count'] !== null)) {
                        $current_sku['stock'][0] = $current_sku['count'];
                    }
                    $data['skus'][$sku_id] = $current_sku;
                }
            }
            if ($category_id) {
                //add extra category if category detected
                $data['categories'] = array_merge(array_keys($product->categories), array($category_id));
            }
            $key .= ':u:'.$product->getId();

        } else {
            $product = new shopProduct();
            if ($category_id) {
                $data['categories'] = array($category_id);
            }
            $data['currency'] = ifempty($data['currency'], reset($currencies));
            if (!isset($currencies[$data['currency']])) {
                $this->data['processed_count'][self::STAGE_PRODUCT]['currency']++;
                $data['currency'] = reset($currencies);
            }

            if (!empty($data['skus'])) {
                $sku = reset($data['skus']);
                $data['sku_id'] = key($data['skus']);
                if (!isset($sku['available'])) {
                    $sku['available'] = true;
                    $data['skus'][$data['sku_id']] = $sku;
                }
            }
            $key .= ':i:'.$this->getKey($fields);
        }

        //Tags workaround
        if (!empty($data['tags']) && is_string($data['tags']) && preg_match('/^\{(.+,.+)\}$/', $data['tags'], $matches)) {
            $data['tags'] = array_filter(array_map('trim', $this->parseRow($matches[1])));
        }

        //Features workaround
        if (!empty($data['features'])) {
            $virtual_sku_stock = null;
            foreach ($data['features'] as $feature => & $values) {
                if (is_array($values)) {
                } elseif (preg_match('/^\{(.+,.+)\}$/', $values, $matches)) {
                    $values = array_map('trim', $this->parseRow($matches[1]));
                } elseif (preg_match('/^<\{(.*)\}>$/', $values, $matches)) {
                    if (!isset($data['features_selectable'])) {
                        $data['features_selectable'] = array();
                    }

                    if ($values = $this->parseRow($matches[1])) {
                        foreach ($values as &$value) {
                            if (preg_match('@^(.+)=([\+\-]?(\d+|\.\d+|\d\.\d))$@', $value, $matches)) {
                                $value = array(
                                    'value' => trim($matches[1]),
                                    'price' => $matches[2],
                                );
                            } else {
                                $value = array(
                                    'value' => trim($value),
                                );
                            }
                            unset($value);
                        }

                        $data['features_selectable'][$feature] = array(
                            'values' => $values,
                        );

                        if (!empty($this->data['virtual_sku_stock'])) {
                            if (isset($data['skus'][-1]['stock'])) {
                                $virtual_sku_stock = $data['skus'][-1]['stock'];
                            }
                            if ($virtual_sku_stock !== null) {
                                $stock = &$virtual_sku_stock;

                                switch ($this->data['virtual_sku_stock']) {
                                    case 'distribute':
                                        //it's a bug!
                                        $features_count = count($values);
                                        if (is_array($stock)) {
                                            foreach ($stock as &$stock_item) {
                                                $stock_item = $stock_item / $features_count;
                                                unset($stock_item);
                                            }
                                        } else {
                                            $stock = $stock / $features_count;
                                        }
                                        $data['features_selectable'][$feature]['stock'] = &$stock;
                                        break;
                                    case 'set':
                                        $data['features_selectable'][$feature]['stock'] = $stock;
                                        break;
                                }
                                unset($stock);
                            }
                        }
                        $product->sku_type = shopProductModel::SKU_TYPE_SELECTABLE;
                        if (isset($data['skus'][-1])) {
                            if (!isset($data['base_price_selectable'])) {
                                $data['base_price_selectable'] = ifset($data['skus'][-1]['price']);
                            }

                            if (!isset($data['purchase_price_selectable'])) {
                                $data['purchase_price_selectable'] = ifset($data['skus'][-1]['purchase_price']);
                            }

                            if (!isset($data['compare_price_selectable'])) {
                                $data['compare_price_selectable'] = ifset($data['skus'][-1]['compare_price']);
                            }
                        }
                        unset($data['skus']);
                    }
                    unset($data['features'][$feature]);
                }
            }
            unset($values);
            //TODO if cleanup is disabled filter empty values for features
        }

        $this->findTax($data);

        $access = $this->findType($data);

        if ($access) {
            $access = !$product->type_id || in_array($product->type_id, $this->data['types']);
        }

        if ($access) {
            $product->__hash = $key;
            foreach ($this->data['new_features'] as $code => &$feature) {
                if (isset($data['features'][$code]) || isset($data['features_selectable'][$code])) {
                    if ($data['type_id'] && !in_array($data['type_id'], $feature['types'])) {
                        if (empty($type_features_model)) {
                            $type_features_model = new shopTypeFeaturesModel();
                        }
                        $type_features_model->updateByFeature($feature['id'], array($data['type_id']), false);

                        $feature['types'][] = $data['type_id'];
                    }
                }
                unset($feature);
            }
        }

        return $access ? $product : null;
    }


    protected function stepImportProduct($data)
    {
        $empty = $this->reader->getEmpty();
        $data += $empty;
        if ($product = $this->findProduct($data)) {
    
            $target = $product->getId() ? 'update' : 'new';
            if (!$this->emulate($product->__hash)) {
                shopProductStocksLogModel::setContext(shopProductStocksLogModel::TYPE_IMPORT);
                if (!$product->save($data)) {
                    $target = 'validate';
                }
                shopProductStocksLogModel::clearContext();

                $this->data['map'][self::STAGE_PRODUCT] = $product->getId();
                if (!empty($data['images'])) {
                    foreach ($data['images'] as & $image) {
                        if (strpos($image, ',')) {
                            $images = explode(',', $image);
                            $image = end($images);
                        }
                    }
                    unset($image);
                    $this->data['map'][self::STAGE_IMAGE] = $data['images'];
                    $this->data['count'][self::STAGE_IMAGE] += count($data['images']);
                }
            }
            $this->data['processed_count'][self::STAGE_PRODUCT][$target]++;
        }

        return true;
    }

    /**
     * @usedby stepImport
     * @param $data
     * @return bool
     */
    protected function stepImportSku($data)
    {
        static $sku_primary;
        static $sku_secondary;
        static $empty_sku;
        static $empty;
        if (!isset($sku_primary)) {
            $secondary = explode(':', $this->data['secondary']);
            $sku_primary = end($secondary);

        }
        if (!isset($sku_secondary)) {
            $extra_secondary = explode(':', $this->data['extra_secondary']);
            $sku_secondary = end($extra_secondary);
        }

        if (!isset($empty)) {
            $empty = $this->reader->getEmpty();
        }
        if (!isset($empty_sku)) {
            $empty_sku = ifset($empty['skus'][-1], array());
        }
        $data += $empty;
        if ($product = $this->findProduct($data)) {
            $item_sku_id = false;
            $current_id = ifset($this->data['map'][self::STAGE_PRODUCT]);
            $id = $product->getId();
            if ($this->emulate()) {
                $target = $id ? 'found' : 'add';
                $target_sku = 'add';
            } else {
                $target = $id ? 'update' : 'new';
                $target_sku = 'new';
            }

            $key = null;
            $sku_only = false;

            $product_exists = $this->emulate() ? ($product->__hash == $current_id) : $id;

            if ($id && isset($data['skus'][-1])) {
                if (in_array($this->data['previous_type'], array(self::STAGE_PRODUCT, self::STAGE_SKU, null), true)
                    &&
                    ($this->emulate() ? ($product->__hash == $current_id) : ($id == $current_id))
                ) {
                    $sku_only = true;
                }

                $sku = $data['skus'][-1] + $empty_sku;
                $this->castSku($sku);

                unset($data['skus'][-1]);
                $item_sku_id = -1;
                $matches = 0;
                foreach ($product->skus as $sku_id => $current_sku) {

                    if ($current_sku[$sku_primary] === ifset($sku[$sku_primary], '')) {
                        //extra workaround for empty primary attribute
                        if (false && ($current_sku[$sku_primary] === '') && $sku_secondary) {
                            if (ifset($sku[$sku_secondary], '') !== $current_sku[$sku_secondary]) {
                                continue;
                            }
                        }

                        if (++$matches == 1) {
                            $item_sku_id = $sku_id;
                            $target_sku = $this->emulate() ? 'found' : 'update';
                            $sku = array_merge($current_sku, $sku);

                        } else {
                            $target_sku = 'skip';
                            $item_sku_id = false;
                            break;
                        }
                    }
                }

                /*if(!array_key_exists($this->getStrategy()->unitKey(), $sku)) {
                    $sku[$this->getStrategy()->unitKey()] = $product[$this->getStrategy()->unitKey()];
                }
                $this->getStrategy()->addEmulateType($sku, 'decode');*/
                //$this->getStrategy()->encodeSku($sku);
              
                if ($item_sku_id !== false) {
                    if (($item_sku_id < 0) && !isset($sku['available'])) {
                        $sku['available'] = true;
                    }
                    if (!$sku_only && !$product->skus) {
                        $data['sku_id'] = $item_sku_id;
                    }
                    $data['skus'][$item_sku_id] = $sku;
                    $key = 's:';
                    if ($item_sku_id > 0) {
                        $key .= 'u:'.$item_sku_id;
                    } else {
                        $key .= 'i:';
                        $key .= $this->getKey(
                            array(
                                $sku_primary => ifset($sku[$sku_primary], ''),
                                //   $sku_secondary => $sku[$sku_secondary],
                            )
                        );

                    }
                } else {
                    unset($data['skus']);
                }
            } elseif (isset($data['skus'][-1])) {
                if (in_array($this->data['previous_type'], array(self::STAGE_PRODUCT, self::STAGE_SKU, null), true)
                    &&
                    $this->emulate()
                    && ($product->__hash == $current_id)
                ) {
                    $sku_only = true;
                    $item_sku_id = true;
                } else {

                }
                $sku = $data['skus'][-1] + $empty_sku;
                $key = 's:';
                $key .= 'i:';
                $key .= $this->getKey(
                    array(
                        $sku_primary => ifset($sku[$sku_primary], ''),
                        //$sku_secondary => $sku[$sku_secondary],
                    )
                );
            } elseif (!empty($data['features_selectable'])) {
                if ($product_exists) {
                    $target = $this->emulate() ? 'found' : 'update';
                }
                //TODO recount virtual SKUs count
                $key = 's:v:';
                $key .= $this->getKey($data['features_selectable']);
                if ($id) {
                    $target_sku = $this->emulate() ? 'found' : 'update';
                } else {
                    //add
                }
            }

            if (!in_array($item_sku_id, array(true, false, 0), true) && !empty($data['skus'][$item_sku_id]['_primary'])) {
                $product->sku_id = $item_sku_id;
                if (isset($data['sku_id'])) {
                    unset($data['sku_id']);
                }
            }

            shopProductStocksLogModel::setContext(shopProductStocksLogModel::TYPE_IMPORT);
            if ($sku_only || empty($this->data['primary'])) {
                if ($product_exists && ($item_sku_id !== false)) {
                    if (!$this->emulate($product->__hash, $key)) {
                        $truncated_data = array(
                            'skus' => $data['skus'],
                        );
                        $virtual_fields = array();
                        foreach ($virtual_fields as $field) {
                            if (isset($data[$field])) {
                                $truncated_data['skus'][$item_sku_id][$field] = $data[$field];
                            }
                        }

                        if (isset($data['features'])) {
                            $model = new shopFeatureModel();
                            $features = $model->getMultipleSelectableFeaturesByType($data['type_id'], 'code');
                            if (!$features) {
                                $features = array();
                            }
                            $features['weight'] = true;
                            foreach (array_keys($features) as $code) {
                                if (isset($data['features'][$code])) {
                                    if (!isset($truncated_data['skus'][$item_sku_id]['features'])) {
                                        $truncated_data['skus'][$item_sku_id]['features'] = array();
                                    }
                                    $truncated_data['skus'][$item_sku_id]['features'] [$code] = $data['features'][$code];
                                }
                            }
                        }

                        if (!$product->save($truncated_data)) {
                            $target_sku = 'validate';
                        };

                        $this->data['map'][self::STAGE_PRODUCT] = $product->getId();
                    } else {
                        $this->data['map'][self::STAGE_PRODUCT] = $product->__hash;
                    }
                }
            } else {
                if (!$this->emulate($product->__hash, $key)) {
                    if (!$product->save($data)) {
                        $target = 'validate';
                    };

                    $this->data['map'][self::STAGE_PRODUCT] = $product->getId();
                    if (!empty($data['images'])) {
                        $this->data['map'][self::STAGE_IMAGE] = $data['images'];
                        $this->data['count'][self::STAGE_IMAGE] += count($data['images']);
                    }
                } else {
                    $this->data['map'][self::STAGE_PRODUCT] = $product->__hash;
                }

                $this->data['processed_count'][self::STAGE_PRODUCT][$target]++;
            }

            shopProductStocksLogModel::clearContext();

            if ($product->getId() || $this->emulate()) {
                $this->data['processed_count'][self::STAGE_SKU][$target_sku]++;
            }
        } else {
            $this->data['processed_count'][self::STAGE_PRODUCT]['rights']++;
        }

        return true;
    }

    protected function stepExportProduct(&$current_stage, &$count, &$processed)
    {
        static $products;
        static $product_feature_model;
        static $feature_model;
        static $tags_model;
        static $size;

        if (!$products) {
            $offset = $current_stage[self::STAGE_PRODUCT] - ifset($this->data['map'][self::STAGE_PRODUCT], 0);
            $fields = '*';
            if (!empty($this->data['options']['images'])) {
                $fields .= ', images';
            }
            $products = $this->getCollection()->getProducts($fields, $offset, 50, false);
        }
        $chunk = 5;
        $non_sku_fields = array(
            'summary',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'description',
            'sort',
            'tags',
            'images',
            'params',
           /* $this->getStrategy()->unitKey(),
            $this->getStrategy()->multiplicityKey(),*/

        );
        while (($chunk-- > 0) && ($product = reset($products))) {
            $exported = false;

            /* check rights per product type && settlement options */
            $rights = empty($product['type_id']) || in_array($product['type_id'], $this->data['types']);

            $category_id = isset($product['category_id']) ? intval($product['category_id']) : null;
            /* check category match*/
            $category_match = !$this->data['export_category'] || ($category_id === $this->data['map'][self::STAGE_CATEGORY]);

            $full = true;

            if (!$category_match) {

                /* check subcategory match */
                if (isset($this->data['include_sub_categories'])) {
                    if (!in_array($category_id, $this->data['include_sub_categories'])) {
                        if (!isset($this->data['external_products'])) {
                            $this->data['external_products'] = array();
                        }

                        if (!isset($this->data['external_products'] [$product['id']])) {
                            $category_match = true;
                            $this->data['external_products'] [$product['id']] = $category_id;
                        }
                    }
                }

                /* check extra categories match */
                if (!$category_match && $this->data['config']['extra_categories']) {
                    $category_match = true;
                    $full = false;
                }
            }

            if ($rights && $category_match) {
                $shop_product = new shopProduct($product);
                $shop_product = new shopZzzfractionalPluginProductDecorator($shop_product);/* Объект все автоматом сконвертирует */
                if (!empty($this->data['options']['features'])) {
                    if (!isset($product['features'])) {
                        if (!$product_feature_model) {
                            $product_feature_model = new shopProductFeaturesModel();
                        }
                        $product['features'] = $product_feature_model->getValues($product['id']);
                    }
                    foreach ($product['features'] as $code => &$feature) {
                        if (!empty($this->data['composite_features'][$code])) {
                            $feature = str_replace('×', 'x', $feature);
                        }
                        unset($feature);
                    }
                }

                if (!isset($product['tags'])) {
                    if (!$tags_model) {
                        $tags_model = new shopProductTagsModel();
                    }
                    $product['tags'] = $this->writeRow($tags_model->getTags($product['id']));
                }
                if (!empty($this->data['options']['images'])) {
                    if (isset($product['images'])) {
                        if (!$size) {
                            /**
                             * @var shopConfig $config
                             */
                            $config = $this->getConfig();
                            $size = $config->getImageSize('big');
                        }
                        foreach ($product['images'] as & $image) {
                            $image = ifempty($this->data['scheme'], 'http://').ifempty($this->data['base_url'], 'localhost').shopImage::getUrl($image, $size);
                        }
                        $product['images'] = array_values($product['images']);
                    }
                }

                if (!empty($product['params']) && is_array($product['params'])) {
                    $product['params'] = $this->paramsToString($product['params']);
                }

                $product['type_name'] = $shop_product->type['name'];

                $skus = $shop_product->skus;

                if (false && $product['sku_id']) {
                    #default SKU reorder
                    if (isset($skus[$product['sku_id']])) {
                        $sku = $skus[$product['sku_id']];
                        $sku['stock'][0] = $sku['count'];
                        $product['skus'] = array(-1 => $sku);
                        unset($skus[$product['sku_id']]);
                    }
                    $this->writer->write($product);
                    if (!empty($this->data['options']['images'])) {
                        if (isset($product['images'])) {
                            $processed[self::STAGE_IMAGE] += count($product['images']);
                        }
                    }
                    $exported = true;
                    if (!empty($this->data['options']['features'])) {
                        unset($product['features']);
                    }
                }

                if (!empty($product['tax_id'])) {
                    $product['tax_name'] = ifset($this->data['taxes'][$product['tax_id']]);
                }
                if (!isset($product['features'])) {
                    $product['features'] = array();
                }

                foreach ($skus as $sku_id => $sku) {
                    if ($exported) {
                        foreach ($non_sku_fields as $field) {
                            if (isset($product[$field])) {
                                unset($product[$field]);
                            }
                        }
                    }

                    if (!empty($this->data['config']['primary_sku'])) {
                        $sku['_primary'] = ($product['sku_id'] == $sku_id) ? '1' : '';
                    }

                    $sku['stock'][0] = $sku['count'];
                    if (!empty($this->data['options']['features'])) {
                        $sku['features'] = $product_feature_model->getValues($product['id'], -$sku_id);
                        if ($product['sku_type'] == shopProductModel::SKU_TYPE_SELECTABLE) {
                            if (!$exported) {
                                $features_selectable_model = new shopProductFeaturesSelectableModel();
                                if ($selected = $features_selectable_model->getByProduct($product['id'])) {
                                    if (!$feature_model) {
                                        $feature_model = new shopFeatureModel();
                                    }
                                    $features = $feature_model->getById(array_keys($selected));
                                    foreach ($features as $feature_id => $feature) {
                                        $values = shopFeatureModel::getValuesModel($feature['type'])->getValues(
                                            array(
                                                'feature_id' => $feature_id,
                                                'id'         => $selected[$feature_id],
                                            )
                                        );
                                        if (!empty($values[$feature['id']])) {
                                            $f_values = $values[$feature['id']];
                                            if (!isset($product['features'])) {
                                                $product['features'] = array();
                                            }
                                            if (isset($sku['features'][$feature['code']])) {
                                                array_unshift($f_values, (string)$sku['features'][$feature['code']]);
                                            }

                                            $product['features'][$feature['code']] = $this->writeRow($f_values, '<{%s}>', true);
                                        }
                                    }
                                }

                                $virtual_product = $product;
                                if (isset($skus[$product['sku_id']])) {
                                    $virtual_product['skus'] = array(-1 => $skus[$product['sku_id']]);
                                } else {
                                    $virtual_product['skus'] = array(-1 => $sku);
                                }

                                $virtual_product['skus'][-1]['stock'] = array(0 => $product['count']);
                                if (!empty($virtual_product['features'])) {
                                    foreach ($virtual_product['features'] as &$feature) {
                                        if (is_array($feature)) {
                                            $feature = $this->writeRow($feature);
                                        }
                                        unset($feature);
                                    }
                                }
                                //unset name & sku for compressed virtual skus
                                $virtual_product['skus'][-1]['name'] = '';
                                $virtual_product['skus'][-1]['sku'] = '';
                                $this->writer->write($virtual_product);
                            }

                            $product['features'] = $sku['features'];
                        } else {
                            if (!$exported) {
                                foreach ($product['features'] as $code => &$values) {
                                    if (isset($sku['features'][$code])) {
                                        $values = array_unique(array_merge((array)$values, (array)$sku['features'][$code]));
                                    }
                                    unset($values);
                                }
                            } else {
                                $product['features'] = $sku['features'];
                            }
                        }
                    }

                    $product['skus'] = array(-1 => $sku);
                    if (!empty($product['features']) && $full) {
                        foreach ($product['features'] as &$feature) {
                            if (is_array($feature)) {
                                $feature = $this->writeRow($feature);
                            }
                            unset($feature);
                        }
                    }
                    $this->writer->write($product);
                    if (isset($product['images'])) {
                        $processed[self::STAGE_IMAGE] += count($product['images']);
                    }
                    $exported = true;
                    ++$current_stage[self::STAGE_SKU];
                    if ($full) {
                        ++$processed[self::STAGE_SKU];
                    }
                }
            } elseif (count($products) > 1) {
                ++$chunk;
            }

            array_shift($products);
            ++$current_stage[self::STAGE_PRODUCT];
            if ($exported && $full) {
                ++$processed[self::STAGE_PRODUCT];
            }
        }

        return ($current_stage[self::STAGE_PRODUCT] < $count[self::STAGE_PRODUCT]);
    }
}
