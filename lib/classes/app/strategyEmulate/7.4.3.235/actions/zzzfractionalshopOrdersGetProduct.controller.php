<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopOrdersGetProductController extends shopZzzfractionalPluginOrdersGetProductController
{
    use shopZzzfractionalPluginAppTrait;
    
    public function workup(&$product, $sku_stocks)
    {
        foreach (array('price', 'min_price', 'max_price') as $key) {
            $product[$key] = round($product[$key], 2);
        }
        if (!empty($product['services']) && is_array($product['services'])) {
            $this->workupServices($product['services']);
        }

        if (!$product['image_id']) {
            $product['url_crop_small'] = null;
        } else {
            /** @var shopConfig $config */
            $config = $this->getConfig();

            $product['url_crop_small'] = shopImage::getUrl(
                array(
                    'id'         => $product['image_id'],
                    'filename'   => $product['image_filename'],
                    'product_id' => $product['id'],
                    'ext'        => $product['ext'],
                ),
                $config->getImageSize('crop_small')
            );
        }
        $this->getStrategy()->decodeProduct($product);/* Приводим остатки к пользовательскому виду */
        // aggregated stocks count icon for product
        $product['icon'] = shopHelper::getStockCountIcon($product['count'], null, true);
        foreach ($product['skus'] as &$sku) {
            $this->workupSku($sku, $sku_stocks);
        }
        unset($sku);
    }

    protected function workupSku(&$sku, $sku_stocks)
    {
        $product = $this->getAppHelper()->getProductBySku($sku);
        $unit_key = $this->unitKey();
        $sku['price'] = round($sku['price'], 2);
        if (!empty($sku['services']) && is_array($sku['services'])) {
            $this->workupServices($sku['services']);
        }
        // detailed stocks count icon for sku
        if (empty($sku_stocks[$sku['id']])) {
            $sku['icon'] = shopHelper::getStockCountIcon($sku['count'], null, true);
        } else {
            $icons = array();
            $counts_htmls = array();
            foreach ($sku_stocks[$sku['id']] as $stock_id => $stock) {
                /* Приводим склады к пользовательскому виду */
                if(array_key_exists($unit_key, $product)) { 
                    $stock['count'] = $this->getStrategy()->decodeCount($stock['count'], $product[$unit_key],null);
                }
                $icon = &$icons[$stock_id];
                $icon = shopHelper::getStockCountIcon($stock['count'], $stock_id)." ";
                $count_html = &$counts_htmls[$stock_id];
                if ($stock['count'] === null) {
                    $count_html = sprintf(str_replace('%d', '%s', _w('%d left')), '∞');
                } else {
                    /* Подменяем надпись на дробный остаток */
                    $mess = str_replace('%d', '%.2f', _w('%d left'));
                    $count_html = _w($mess, $mess, $stock['count']);
                }
                unset($icon, $count_html);
            }
            $sku['icon'] = shopHelper::getStockCountIcon($sku['count'], null, true);
            $sku['icons'] = $icons;
            $sku['count_htmls'] = $counts_htmls;
        }
    }
}
