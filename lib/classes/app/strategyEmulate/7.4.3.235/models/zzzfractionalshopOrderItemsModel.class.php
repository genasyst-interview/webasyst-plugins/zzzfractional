<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php','shop'));

class shopOrderItemsModel extends shopZzzfractionalPluginOrderItemsModel
{
    use shopZzzfractionalPluginAppTrait;
    protected function getRawItems($order_id)
    {
        $items = parent::getRawItems($order_id);
        $items = $this->getStrategy()->decodeItemsCount($items);
        return $items;
    }
}