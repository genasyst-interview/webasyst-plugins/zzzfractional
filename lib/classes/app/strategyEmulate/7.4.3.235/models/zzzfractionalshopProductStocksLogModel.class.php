<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php','shop'));

class shopProductStocksLogModel extends shopZzzfractionalPluginProductStocksLogModel
{
    use shopZzzfractionalPluginAppTrait;

    public function getList($fields = '*,stock_name,sku_name,product_name', $options = array())
    {
        $options += $this->getDefaultOptions();

        $main_fields = array();
        $post_fields = array();
        foreach (explode(',', $fields) as $name) {
            if ($this->fieldExists($name) || $name == '*') {
                $main_fields[]= $name;
            } else {
                $post_fields[]= $name;
            }
        }

        $where = $this->getWhereByField($options['where']);

        $limit_str = '';
        if ($options['limit'] !== false) {
            $limit_str = " LIMIT ".($options['offset'] ? $options['offset'].',' : '').(int)$options['limit'];
        }

        $sql = "SELECT ".implode(',', $main_fields)." FROM `{$this->table}`".
                ($where ? " WHERE $where" : "").
                " ORDER BY ".$options['order'].
                $limit_str;

        $data = $this->query($sql)->fetchAll('id');
        if (!$data) {
            return $data;
        }
        if(is_array($data) && !empty($data)) {
            $this->getStrategy()->productStocksLogModelGetList($data);
        }
        $this->workupList($data, $post_fields);
        /* Подменяем надпись */
        $unit_key = $this->unitKey();
        foreach ($data as &$v) {
            if(array_key_exists($unit_key, $v) && shopZzzfractionalPluginDimension::isFractional($v[$unit_key])) {
                $description = sprintf(_w('In stock value updated to %d'), $v['after_count']);
                if(preg_match('/'.$description.'/', $v['description'])) {
                    $v['description'] = preg_replace('/'.intval($v['after_count']).'$/',
                        round($v['after_count'],2),
                        $v['description']
                    );
                }
            }
        }
        return $data;

    }


    public function multipleInsert($data)  {
        $unit_key = $this->unitKey();
        if($this->fieldExists($unit_key)) {
            $this->getStrategy()->productStocksLogModelPrepareMultipleInsert($data);
        }
        return parent::multipleInsert($data);
    }

    public function insert($data, $type = 0)
    {
        $unit_key = $this->unitKey();
        if($this->fieldExists($unit_key)) {
            $this->getStrategy()->productStocksLogModelPrepareInsert($data);
        }
        return parent::insert($data, $type);
    }
}
