<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php','shop'));

class shopOrderModel extends shopZzzfractionalPluginOrderModel
{
    use shopZzzfractionalPluginAppTrait;
    /**
     * @param int|array|null $order_id null - all orders
     */
    public function recalculateProductsTotalSales($order_id = null)
    {
        $this->removeProductsTotalSales($order_id);
        $units = $this->getStrategy()->getDimension()->getList();
        $unit_key =  $this->unitKey();

        $product_ids = array();
        if ($order_id !== null) {
            $order_id = (array)$order_id;
            $sql = "SELECT DISTINCT product_id FROM shop_order_items
                WHERE type = 'product' AND order_id IN (i:order_id)";
            $product_ids = $this->query($sql, array('order_id' => $order_id))->fetchAll(null, true);
        }
        foreach ($units as $type => $data)  {
            foreach ($data['units'] as $uk => $uv)  {
                $sql = "SELECT oi.product_id AS id,   
                (SUM(oi.price * o.rate * oi.quantity)".
                    $this->getMultiplierString($uv['multiplier'])
                    .")
                 total_sales
                FROM ".$this->table." o JOIN shop_order_items oi
                ON o.id = oi.order_id 
                AND oi.type = 'product'
                 AND oi.".$unit_key." = '".$uk."'
                WHERE paid_date IS NOT NULL
                GROUP BY oi.product_id
                ORDER BY oi.product_id";
                if ($order_id === null) {
                    $sql = "UPDATE `shop_product` p JOIN ($sql) r ON p.id = r.id SET p.total_sales = p.total_sales+r.total_sales";
                } elseif ($product_ids) {
                    $sql = "UPDATE `shop_product` p JOIN ($sql) r ON p.id = r.id
                SET p.total_sales = p.total_sales+r.total_sales
                WHERE p.id IN(".implode(',', $product_ids).")";
                }
                $this->query($sql);
            }
        }
    }

    protected function removeProductsTotalSales($order_id = null) {
        $product_ids = array();
        if ($order_id !== null) {
            $order_id = (array)$order_id;
            $sql = "SELECT DISTINCT product_id FROM shop_order_items
                WHERE type = 'product' AND order_id IN (i:order_id)";
            $product_ids = $this->query($sql, array('order_id' => $order_id))->fetchAll(null, true);
        }
        $sql = "SELECT oi.product_id AS id FROM ".$this->table." o JOIN shop_order_items oi
                ON o.id = oi.order_id AND oi.type = 'product'
                WHERE paid_date IS NOT NULL
                GROUP BY oi.product_id
                ORDER BY oi.product_id";
        if ($order_id === null) {
            $sql = "UPDATE `shop_product` p JOIN ($sql) r ON p.id = r.id SET p.total_sales = 0.0";
        } elseif ($product_ids) {
            $sql = "UPDATE `shop_product` p JOIN ($sql) r ON p.id = r.id
                SET p.total_sales = 0.0
                WHERE p.id IN(".implode(',', $product_ids).")";
        }
        $this->query($sql);
    }

    public function getTotalSalesByProduct($product_id, $product_currency = null)
    {
        $units = $this->getStrategy()->getDimension()->getList();
        $unit_key =  $this->unitKey();

        if ($product_currency) {
            $currency_model = new shopCurrencyModel();
            $rate = $currency_model->getRate($product_currency);
            if (!$rate) {
                $rate = 1;
            }
        } else {
            $rate = 1;
        }
        $order_subtotal = '(o.total+o.discount-o.tax-o.shipping)';
        /*Original  $sql = "SELECT
                    SUM(oi.price * o.rate * oi.quantity) subtotal,
                    SUM(oi.quantity) quantity,
                    SUM(IF({$order_subtotal} <= 0, 0, oi.price*o.rate*oi.quantity*o.discount / {$order_subtotal})) AS discount,
                    SUM(IF(oi.purchase_price > 0, oi.purchase_price*o.rate, ps.purchase_price*".$this->escape($rate).")*oi.quantity) purchase
                FROM ".$this->table." o
                    JOIN shop_order_items oi
                        ON o.id = oi.order_id
                            AND oi.product_id = i:product_id
                            AND oi.type = 'product'
                    JOIN shop_product_skus ps
                        ON oi.sku_id = ps.id
                WHERE paid_date >= DATE_SUB(DATE('".date('Y-m-d')."'), INTERVAL 30 DAY)";
        */
        $data = array();
        foreach ($units as $type => $units_data)  {
            foreach ($units_data['units'] as $uk  =>  $uv)  {
                $sql = "SELECT 
                   (SUM(oi.price * o.rate * oi.quantity)".
                    $this->getMultiplierString($uv['multiplier'])
                    .") subtotal,
                    (SUM(oi.quantity)".
                    $this->getMultiplierString($uv['multiplier'])
                    .") quantity,
                    (SUM(IF({$order_subtotal} <= 0, 0, (oi.price*oi.quantity".
                    $this->getMultiplierString($uv['multiplier'])
                    .")*o.rate*o.discount / {$order_subtotal}))) AS discount,
(SUM(IF(oi.purchase_price > 0, oi.purchase_price*o.rate, ps.purchase_price*".$this->escape($rate).")*(oi.quantity".
                    $this->getMultiplierString($uv['multiplier'])
                    ."))) purchase
                FROM ".$this->table." o
                    JOIN shop_order_items oi
                        ON o.id = oi.order_id 
                            AND oi.".$unit_key." = '".$uk."'
                            AND oi.product_id = i:product_id 
                            AND oi.type = 'product'
                    JOIN shop_product_skus ps
                        ON oi.sku_id = ps.id
                WHERE paid_date >= DATE_SUB(DATE('".date('Y-m-d')."'), INTERVAL 30 DAY)";
                foreach ($this->query($sql, array('product_id' => $product_id))->fetch() as $key => $value) {
                    if (!is_numeric($key)) {
                        if(!array_key_exists($key, $data)) {
                            $data[$key] = 0.0;
                        }
                        $data[$key] = $data[$key]+$value;
                    }

                }
            }
        }
        $data['total'] = $data['subtotal'] - $data['discount'];
        return $data;
    }


    public function getSalesByProduct($product_id, $start_date)
    {
        $units = $this->getStrategy()->getDimension()->getList();
        $unit_key =  $this->unitKey();

        $product_id = (int)$product_id;

        $order_subtotal = '(o.total+o.discount-o.tax-o.shipping)';
        /* ORIGINAL $sql = "SELECT
            o.paid_date AS date,
            SUM(oi.quantity) AS quantity,
            SUM(oi.price*o.rate*oi.quantity) AS subtotal_sales,
            SUM(IF({$order_subtotal} <= 0, 0, oi.price*o.rate*oi.quantity*o.discount / {$order_subtotal})) AS discount,
            SUM(IF(oi.purchase_price > 0, oi.purchase_price*o.rate, IFNULL(ps.purchase_price*pcur.rate, 0))*oi.quantity) AS purchase
        FROM {$this->table} AS o
            JOIN shop_order_items AS oi
                ON oi.order_id=o.id
            LEFT JOIN shop_product AS p
                ON oi.product_id=p.id
            LEFT JOIN shop_product_skus AS ps
                ON oi.sku_id=ps.id
            LEFT JOIN shop_currency AS pcur
                ON pcur.code=p.currency
            WHERE p.id = i:product_id AND oi.type = 'product' AND o.paid_date IS NOT NULL AND o.paid_date >= :start_date
            GROUP BY o.paid_date";
        */
        $result = array();
        foreach ($units as $type => $units_data)  {
            foreach ($units_data['units'] as $uk => $uv)  {
                $sql = "SELECT
            o.paid_date AS date,
            (SUM(oi.quantity".
                    $this->getMultiplierString($uv['multiplier'])
                    .")) AS quantity,
            (SUM(oi.price*o.rate*oi.quantity)".
                    $this->getMultiplierString($uv['multiplier'])
                    .") AS subtotal_sales,
            (SUM(IF({$order_subtotal} <= 0, 0, ((oi.price*o.rate*oi.quantity*o.discount)".
                    $this->getMultiplierString($uv['multiplier'])
                    .") / {$order_subtotal}))) AS discount,
            (SUM(IF(oi.purchase_price > 0, oi.purchase_price*o.rate, IFNULL(ps.purchase_price*pcur.rate, 0))*oi.quantity".
                    $this->getMultiplierString($uv['multiplier'])
                    .")) AS purchase
        FROM {$this->table} AS o
            JOIN shop_order_items AS oi
                ON oi.order_id=o.id
            LEFT JOIN shop_product AS p
                ON oi.product_id=p.id
            LEFT JOIN shop_product_skus AS ps
                ON oi.sku_id=ps.id
            LEFT JOIN shop_currency AS pcur
                ON pcur.code=p.currency
            WHERE p.id = i:product_id AND oi.type = 'product'   AND oi.".$unit_key." = '".$uk."' AND o.paid_date IS NOT NULL AND o.paid_date >= :start_date
            GROUP BY o.paid_date";
                
                foreach ($this->query($sql, array(
                    'product_id' => $product_id,
                    'start_date' => $start_date
                ))->fetchAll() as $v) {
                    $result[] = $v;
                }
            }
        }
        $date_sales = array();
        foreach ($result as $row) {
            if(array_key_exists($row['date'], $date_sales)) {
                foreach (array('quantity','subtotal_sales','discount','purchase')as  $key) {
                    if(!array_key_exists($key, $date_sales[$row['date']])) {
                        $date_sales[$row['date']][$key] = 0.0;
                    }
                    $date_sales[$row['date']][$key] =  $date_sales[$row['date']][$key] + $row[$key];
                }
            } else {
                $date_sales[$row['date']] = $row;
            }
        }
        foreach ($date_sales as &$row) {
            $row['sales'] = $row['subtotal_sales'] - $row['discount'];
        }
        
        return $date_sales;
    }
   
    protected function getMultiplierString($multiplier) {
        return ($multiplier> 0.0 && $multiplier != 1.0)?
            '/'.shopZzzfractionalPluginNumber::toString($multiplier):'';
    }
} 
