<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php','shop'));

class shopCartItemsModel extends shopZzzfractionalPluginCartItemsModel
{
    use shopZzzfractionalPluginAppTrait;
    public function total($code)
    {
        if (!$code) {
            return 0;
        }
        $sql = "SELECT c.id as item_id, c.quantity, s.*
                FROM ".$this->table." c
                    JOIN shop_product_skus s ON c.sku_id = s.id
                WHERE c.code = s:code
                    AND type = 'product'";

        $skus = $this->query($sql, array('code' => $code))->fetchAll('item_id');
        if (!$skus) {
            return 0.0;
        }
        $product_ids = array();
        foreach ($skus as $k => $sku) {
            $product_ids[] = $sku['product_id'];
            $skus[$k]['original_price'] = $sku['price'];
            $skus[$k]['original_compare_price'] = $sku['compare_price'];
        }
        $product_ids = array_unique($product_ids);
        $product_model = new shopProductModel();
        $products = $product_model->getById($product_ids);

        foreach ($products as $p_id => $p) {
            $products[$p_id]['original_price'] = $p['price'];
            $products[$p_id]['original_compare_price'] = $p['compare_price'];
        }
        $event_params = array(
            'products' => &$products,
            'skus' => &$skus
        );
        wa('shop')->event('frontend_products', $event_params);
        shopRounding::roundSkus($skus);
        $products_total = 0.0;
        foreach ($skus as $s) {
            $products_total += $s['frontend_price'] * $s['quantity'];
        }
        // services
        $services_total = $this->getServicesTotal($code, $event_params);
        return (float) ($products_total + $services_total);
    }

    // Helper for total()
    // Services total in frontend currency
    protected function getServicesTotal($code, $products_skus)
    {
        $sql = "SELECT c.*, s.currency
                FROM ".$this->table." c
                    JOIN shop_service s
                        ON c.service_id = s.id
                WHERE c.code = s:code
                    AND type = 'service'";
        $services = $this->query($sql, array('code' => $code))->fetchAll();
        if (!$services) {
            return 0.0;
        }

        $variant_ids = array();
        $product_ids = array();
        $service_stubs = array();
        foreach ($services as $s) {
            if ($s['service_variant_id']) {
                $variant_ids[] = $s['service_variant_id'];
            }
            $product_ids[] = $s['product_id'];

            $service_stubs[$s['service_id']] = array(
                'id' => $s['service_id'],
                'currency' => $s['currency'],
            );
        }
        $variant_ids = array_unique($variant_ids);
        $product_ids = array_unique($product_ids);

        $config = wa('shop')->getConfig();
        /**
         * @var shopConfig $config
         */

        // get variant settings
        $rounding_enabled = shopRounding::isEnabled();
        $variants_model = new shopServiceVariantsModel();
        $variants = $variants_model->getWithPrice($variant_ids);
        if ($rounding_enabled) {
            shopRounding::roundServiceVariants($variants, $service_stubs);
        }
        $round_services = wa()->getSetting('round_services');

        // get products/skus settings
        $product_services_model = new shopProductServicesModel();
        $products_services = $product_services_model->getByProducts($product_ids, true);



        $primary = $config->getCurrency();
        $frontend_currency = $config->getCurrency(false);

        // Calculate total amount for all services
        $services_total = 0;
        $this->getStrategy()->decodeItemsCount($services);
        foreach ($services as $s) {
            $p_id = $s['product_id'];
            $sku_id = $s['sku_id'];
            $s_id = $s['service_id'];
            $v_id = $s['service_variant_id'];
            $p_services = isset($products_services[$p_id]) ? $products_services[$p_id] : array();

            $s['price'] = $variants[$v_id]['price'];

            // price variant for sku
            if (!empty($p_services['skus'][$sku_id][$s_id]['variants'][$v_id]['price'])) {
                shopRounding::roundServiceVariants(
                    $p_services['skus'][$sku_id][$s_id]['variants'],
                    array(
                        array(
                            'id'       => $s['service_id'],
                            'currency' => $s['currency'],
                        ),
                    )
                );
                $s['price'] = $p_services['skus'][$sku_id][$s_id]['variants'][$v_id]['price'];
            }

            if ($s['currency'] == '%') {
                if (isset($products_skus['skus'][$s['parent_id']])) {
                    $sku_price = $products_skus['skus'][$s['parent_id']]['frontend_price'];
                } else {
                    // most likely never happen case, but just in case
                    $product = $products_skus['products'][$s['product_id']];
                    $product_price = $product['price'];
                    $product_currency = $product['currency'] !== null ? $product['currency'] : $primary;
                    $sku_price = shop_currency($product_price, $product_currency, $frontend_currency, false);
                }
                $s['price'] = shop_currency($s['price'] * $sku_price / 100, $frontend_currency, $frontend_currency, false);
            } else {
                $s['price'] = shop_currency($s['price'], $variants[$v_id]['currency'], $frontend_currency, false);
            }

            if (!empty($round_services)) {
                $s['price'] = shopRounding::roundCurrency($s['price'], $frontend_currency);
            }

            $services_total += $s['price'] * $s['quantity'];
        }
        return $services_total;
    }

    public function count($code, $type = null)
    {
        if (!$code) {
            return 0;
        }
        $sql = "SELECT id, product_id, quantity FROM ".$this->table." WHERE code = s:code";
        if ($type) {
            $sql .= ' AND type = s:type';
        }
        $data = $this->query($sql, array(
            'code' => $code,
            'type' => $type
        ))->fetchAll();
        $this->getStrategy()->decodeItemsCount($data);
        $quantity = 0.0;
        foreach ($data as $v) {
            $quantity += $v['quantity'];
        }
        return $quantity;
    }
    
    public function getItemByProductAndServices($code, $product_id, $sku_id, $services)
    {
        if (!$services) {
            return $this->getSingleItem($code, $product_id, $sku_id);
        }
        $items = array();
        $rows = $this->getByField(array('code' => $code, 'product_id' => $product_id, 'sku_id' => $sku_id), true);
        foreach ($rows as $row) {
            if ($row['type'] == 'product') {
                if (isset($items[$row['id']])) {
                    $row['services'] = $items[$row['id']]['services'];
                }
                $items[$row['id']] = $row;
            } else {
                $items[$row['parent_id']]['services'][$row['service_id']] = $row['service_variant_id'];
            }
        }
        foreach ($items as $item) {
            if (!isset($item['services']) || count($item['services']) != count($services)) {
                continue;
            }
            $flag = true;
            foreach ($item['services'] as $s_id => $v_id) {
                if (!isset($services[$s_id]) || $services[$s_id] != $v_id) {
                    $flag = false;
                    break;
                }
            }
            if ($flag) {
                return $this->getStrategy()->decodeItemCount($item);
            }
        }
        return null;
    }

    public function getSingleItem($code, $product_id, $sku_id)
    {
        $item = parent::getSingleItem($code, $product_id, $sku_id);
        if(!empty($item)) {
            $this->getStrategy()->decodeItemCount($item);
        }
        return $item;
    }
}
