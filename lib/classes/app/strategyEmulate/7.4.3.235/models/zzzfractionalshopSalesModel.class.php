<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php','shop'));

class shopSalesModel extends shopZzzfractionalPluginSalesModel
{
    use shopZzzfractionalPluginAppTrait;

    public function getCohorts($cohorts_type, $date_start, $date_end, $options)
    {
        empty($date_end) && ($date_end = date('Y-m-d 23:59:59'));
        empty($date_start) && ($date_start = $this->getMinDate());

        if ($cohorts_type == 'profit' || $cohorts_type == 'clv' || $cohorts_type == 'roi') {
            // Calculate profit
            $cohorts = $this->getCohorts('subtotal', $date_start, $date_end, $options);
            $cohorts_purchase = $this->getCohorts('purchase', $date_start, $date_end, $options);
            foreach($cohorts_purchase as $reg_date => $series) {
                foreach($series as $order_date => $stats) {
                    if (empty($cohorts[$reg_date][$order_date])) {
                        $cohorts[$reg_date][$order_date] = $stats;
                        $cohorts[$reg_date][$order_date]['metric'] = 0;
                    }
                    $cohorts[$reg_date][$order_date]['metric'] -= $stats['metric'];
                }
            }
            unset($cohorts_purchase);
            if ($cohorts_type == 'profit') {
                return $cohorts;
            }

            if ($cohorts_type == 'clv') {
                // Divide profit by number of customers to calculate CLV
                $cohorts_customers = $this->getCohortCustomersCount($date_start, $date_end, $options);
                foreach($cohorts as $reg_date => $series) {
                    $total_profit_so_far = 0;
                    $customers_count = $cohorts_customers[$reg_date];
                    foreach($series as $order_date => $stats) {
                        $total_profit_so_far += $cohorts[$reg_date][$order_date]['metric'];
                        if ($customers_count) {
                            $cohorts[$reg_date][$order_date]['metric'] = $total_profit_so_far / $customers_count;
                        } else {
                            $cohorts[$reg_date][$order_date]['metric'] = 0;
                        }
                    }
                }
            } else {
                // Calculate ROI based on profit and expenses
                $cohort_cost = $this->getCohortCost($date_start, $date_end, $options);
                foreach($cohorts as $reg_date => $series) {
                    $total_profit_so_far = 0;
                    $cost = $cohort_cost[$reg_date];
                    foreach($series as $order_date => $stats) {
                        $total_profit_so_far += $cohorts[$reg_date][$order_date]['metric'];
                        if ($cost) {
                            $cohorts[$reg_date][$order_date]['metric'] = $total_profit_so_far*100/$cost;
                        } else {
                            $cohorts[$reg_date][$order_date]['metric'] = 0;
                        }
                    }
                }
            }

            return $cohorts;
        }

        list($storefront_join, $storefront_where) = $this->getStorefrontSql($options);
        $customer_date_sql = self::getDateSql('cn.create_datetime', $date_start, $date_end);
        $order_date_col = 'DATE(o.create_datetime)';
        $order_date_sql = self::getDateSql('o.create_datetime', $date_start, $date_end).' AND o.paid_date IS NOT NULL';

        $customer_source_join = '';
        $customer_source_where = '';
        if (!empty($options['customer_source'])) {
            $customer_source_join = "JOIN shop_customer AS c ON c.contact_id=o.contact_id";
            $customer_source_where = "AND c.source='".$this->escape($options['customer_source'])."'";
        }

        $group_by = ifset($options['group'], 'months');

        $metric_join = '';
        switch($cohorts_type) {
            case 'sales':
                $metric_sql = "SUM(o.total*o.rate)";
                break;
            case 'subtotal':
                $metric_sql = "SUM((o.total - o.tax - o.shipping - o.discount)*o.rate)";
                break;
            case 'purchase':
                $metric_join = "JOIN shop_order_items AS oi
                                    ON oi.order_id=o.id
                                        AND oi.type='product'
                                LEFT JOIN shop_product AS p
                                    ON oi.product_id=p.id
                                LEFT JOIN shop_product_skus AS ps
                                    ON oi.sku_id=ps.id
                                LEFT JOIN shop_currency AS pcur
                                    ON pcur.code=p.currency";
                $metric_sql = "SUM(IF(oi.purchase_price > 0, oi.purchase_price*o.rate, IFNULL(ps.purchase_price*pcur.rate, 0))*oi.quantity)";
                break;
            case 'order_count':
                $metric_sql = "COUNT(DISTINCT o.id)";
                break;
            case 'customer_count':
                $metric_sql = "COUNT(DISTINCT o.contact_id)";
                break;
            default:
                throw new waException('Unknown type: '.htmlspecialchars($cohorts_type));
        }

        if($cohorts_type == 'purchase') {
            $units = $this->getStrategy()->getDimension()->getList();
            $unit_key =  $this->unitKey();
            $tmp_result = array();
            foreach ($units as $type => $units_data)  {
                foreach ($units_data['units'] as $uk => $uv)  {
                    $metric_join = "JOIN shop_order_items AS oi
                                    ON oi.order_id=o.id
                                        AND oi.type='product'
                                         AND oi.".$unit_key." = '".$uk."'
                                LEFT JOIN shop_product AS p
                                    ON oi.product_id=p.id
                                LEFT JOIN shop_product_skus AS ps
                                    ON oi.sku_id=ps.id
                                LEFT JOIN shop_currency AS pcur
                                    ON pcur.code=p.currency";
                    $metric_sql = "(SUM(IF(oi.purchase_price > 0, oi.purchase_price*o.rate, IFNULL(ps.purchase_price*pcur.rate, 0))*oi.quantity)".
                        $this->getMultiplierString($uv['multiplier'])
                        .")";

                    $sql = "SELECT DATE(cn.create_datetime) AS registration_date, {$order_date_col} AS order_date, {$metric_sql} AS metric
                FROM shop_order AS o
                    JOIN wa_contact AS cn
                        ON cn.id=o.contact_id
                    {$customer_source_join}
                    {$storefront_join}
                    {$metric_join}
                WHERE {$order_date_sql}
                    AND {$customer_date_sql}
                    {$storefront_where}
                    {$customer_source_where}
                GROUP BY DATE(cn.create_datetime), {$order_date_col}";

                    $tmp_result += $this->query($sql)->fetchAll();
                }
            }

            $result = array();
            foreach($tmp_result as $row) {
                $period_start_reg = self::getPeriodStart($row['registration_date'], $group_by);
                $period_start_order = self::getPeriodStart($row['order_date'], $group_by);
                if (empty($result[$period_start_reg][$period_start_order])) {
                    $result[$period_start_reg][$period_start_order] = array(
                        'date' => $period_start_order,
                        'metric' => 0,
                    );
                }
                $result[$period_start_reg][$period_start_order]['metric'] += $row['metric'];
            }
        } else {
            $sql = "SELECT DATE(cn.create_datetime) AS registration_date, {$order_date_col} AS order_date, {$metric_sql} AS metric
                FROM shop_order AS o
                    JOIN wa_contact AS cn
                        ON cn.id=o.contact_id
                    {$customer_source_join}
                    {$storefront_join}
                    {$metric_join}
                WHERE {$order_date_sql}
                    AND {$customer_date_sql}
                    {$storefront_where}
                    {$customer_source_where}
                GROUP BY DATE(cn.create_datetime), {$order_date_col}";
            $result = array();
            foreach($this->query($sql) as $row) {
                $period_start_reg = self::getPeriodStart($row['registration_date'], $group_by);
                $period_start_order = self::getPeriodStart($row['order_date'], $group_by);
                if (empty($result[$period_start_reg][$period_start_order])) {
                    $result[$period_start_reg][$period_start_order] = array(
                        'date' => $period_start_order,
                        'metric' => 0,
                    );
                }
                $result[$period_start_reg][$period_start_order]['metric'] += $row['metric'];
            }
        }

        // Loop over all days of a period and add empty dates into $result
        $start_ts = strtotime($date_start);
        $end_ts = $date_end ? strtotime($date_end) : time();
        for ($t = $start_ts; $t <= $end_ts; $t = strtotime(date('Y-m-d', $t) . ' +1 day')) {
            $date = self::getPeriodStart($t, $group_by);
            if (empty($result[$date])) {
                $result[$date] = array();
            }
        }

        // Add empty dates to each cohort in $result,
        foreach($result as $reg_date => $periods) {
            foreach(array_keys($result) as $date) {
                if (empty($periods[$date])) {
                    $result[$reg_date][$date] = array(
                        'date' => $date,
                        'metric' => 0,
                    );
                }
            }
            ksort($result[$reg_date]);
        }
        ksort($result);

        return $result;
    }

    protected function rebuildFromTmp($type, $date_start, $date_end, $options)
    {
        // SQL part to filter orders by date
        if (wa()->getSetting('reports_date_type', 'paid', 'shop') == 'create') {
            $date_col = 'DATE(o.create_datetime)';
        } else {
            $date_col = 'o.paid_date';
        }
        $units = $this->getStrategy()->getDimension()->getList();
        $unit_key = $this->unitKey();
        // Obtain lock
        try {
            $this->exec(
                "LOCK TABLES
                    {$this->table} WRITE,
                    shop_expense READ,
                    shop_sales_tmp AS st READ,
                    shop_order AS o READ,
                    shop_product AS p READ,
                    shop_product_skus AS ps READ,
                    shop_currency AS pcur READ,
                    shop_order_items AS oi READ"
            );
        } catch (waDbException $e) {
            // Oh, well... nvm then.
            $no_lock = true;
        }

        // Delete old cached data we're about to rebuild
        $hash = self::getHash($type, $options);
        $date_sql = self::getDateSql('`date`', $date_start, $date_end);
        $sql = "DELETE FROM {$this->table} WHERE $date_sql AND hash=?";
        $this->exec($sql, $hash);

        // Fill in data into shop_sales using [order=>name] bound in temporary table
        // `order_count`, `sales`, `shipping`, `tax`, `new_customer_count`:
        $escaped_hash = $this->escape($hash);
        $sql = "INSERT IGNORE INTO {$this->table} (hash, `date`, name, order_count, sales, shipping, tax, new_customer_count)
                SELECT
                    '{$escaped_hash}' AS `hash`,
                    {$date_col} as `date`,
                    st.name AS `name`,
                    COUNT(*) AS `order_count`,
                    SUM(o.total*o.rate) AS `sales`,
                    SUM(o.shipping*o.rate) AS `shipping`,
                    SUM(o.tax*o.rate) AS `tax`,
                    SUM(o.is_first) AS `new_customer_count`
                FROM shop_sales_tmp AS st
                    JOIN shop_order AS o
                        ON o.id=st.order_id
                GROUP BY st.name, {$date_col}";
        $this->exec($sql);

        // Purchase costs: `purchase`
        foreach ($units as $type => $units_data)  {
            foreach ($units_data['units'] as $uk => $uv)  {
                $sql = "INSERT INTO {$this->table} (hash, `date`, name, purchase)
                    SELECT
                        '{$escaped_hash}',
                        {$date_col},
                        st.name,
                        (SUM(IF(oi.purchase_price > 0, oi.purchase_price*o.rate, IFNULL(ps.purchase_price*pcur.rate, 0))*oi.quantity)".
                    $this->getMultiplierString($uv['multiplier'])
                    .")
                    FROM shop_sales_tmp AS st
                        JOIN shop_order AS o
                            ON o.id=st.order_id
                        JOIN shop_order_items AS oi
                            ON oi.order_id=o.id
                        LEFT JOIN shop_product AS p
                            ON oi.product_id=p.id
                        LEFT JOIN shop_product_skus AS ps
                            ON oi.sku_id=ps.id
                        LEFT JOIN shop_currency AS pcur
                            ON pcur.code=p.currency
                    WHERE oi.type='product' 
                     AND oi.".$unit_key." = '".$uk."'
                    GROUP BY st.name, {$date_col}
                ON DUPLICATE KEY UPDATE purchase=VALUES(purchase)";
                $this->exec($sql);

            }
        }

        // Marketing costs: `cost`. Used in the loop below.
        $expenses = array();
        if (empty($options['abtest_id']) && in_array($type, array('sources', 'campaigns', 'social', 'customer_sources'))) {

            $name_sql = '';
            if ($type == 'social') {
                $social_domains = wa('shop')->getConfig()->getOption('social_domains');
                if (!$social_domains || !is_array($social_domains)) {
                    $name_sql = 'AND 1=0';
                } else {
                    $domains = array();
                    foreach(array_keys($social_domains) as $d) {
                        $domains[] = "'".$this->escape($d)."'";
                    }
                    $name_sql = 'AND name IN ('.join(',', $domains).')';
                }
            }

            if ($type == 'customer_sources') {
                $type_sql = '';
            } else if ($type == 'campaigns') {
                $type_sql = "AND type='campaign'";
            } else {
                $type_sql = "AND type='source'";
            }

            $storefront_sql = '';
            if (!empty($options['storefront'])) {
                $storefront_sql = "AND storefront='".$this->escape($options['storefront'])."'";
            }
            $sql = "SELECT name, start, end, amount, DATEDIFF(end, start) + 1 AS days_count
                    FROM shop_expense
                    WHERE start <= DATE(?)
                        AND end >= DATE(?)
                        {$type_sql}
                        {$name_sql}
                        {$storefront_sql}
                    ORDER BY start";
            $expenses = $this->query($sql, array(
                $date_end,
                $date_start,
            ))->fetchAll();
        }

        // List of dates we have an empty record for. Used in the following loop.
        $sql = "SELECT DISTINCT `date`
                FROM {$this->table}
                WHERE hash=?
                    AND name=''
                    AND {$date_sql}";
        $dates = $this->query($sql, $hash)->fetchAll('date');

        // Loop over all days of a period, bulding expenses for each date
        // and making sure a special empty name record exists for each date.
        $empty_values = array();
        $marketing_costs = array();
        $start_ts = strtotime($date_start);
        $end_ts = strtotime($date_end);
        for ($t = $start_ts; $t <= $end_ts; $t = strtotime(date('Y-m-d', $t) . ' +1 day')) {
            $date = date('Y-m-d', $t);
            if (empty($dates[$date])) {
                $empty_values[] = "('$escaped_hash','$date','')";
            }

            // Prepare data for marketing costs for this day
            foreach($expenses as $i => $e) {
                if (strtotime($e['end']) < $t) {
                    unset($expenses[$i]);
                    continue;
                }
                if (strtotime($e['start']) > $t) {
                    break;
                }

                if ($e['days_count'] > 0) {
                    if (empty($marketing_costs[$date][$e['name']])) {
                        $marketing_costs[$date][$e['name']] = 0;
                    }
                    $marketing_costs[$date][$e['name']] += $e['amount'] / $e['days_count'];
                }
            }
        }
        unset($dates);

        // Insert/update marketing costs in shop_sales
        $cost_values = array();
        foreach($marketing_costs as $date => $exps) {
            foreach($exps as $name => $amount) {
                $amount = $this->castValue('float', $amount);
                $cost_values[] = "('{$escaped_hash}', '".$this->escape($name)."', '{$date}', '{$amount}')";
            }
        }
        while ($cost_values) {
            $part = array_splice($cost_values, 0, min(100, count($cost_values)));
            $sql = "INSERT INTO {$this->table} (hash, name, `date`, cost) VALUES ".join(', ', $part)
                    ." ON DUPLICATE KEY UPDATE cost=VALUES(cost)";
            $this->exec($sql);
        }

        // Insert empty rows so that there are no gaps in the period
        while ($empty_values) {
            $part = array_splice($empty_values, 0, min(50, count($empty_values)));
            $sql = "INSERT IGNORE INTO {$this->table} (hash, `date`, name) VALUES ".join(', ', $part);
            $this->exec($sql);
        }

        // Release lock
        empty($no_lock) && $this->exec("UNLOCK TABLES");
    }



    /*  Original shopServicesModel::getTop Перенесенный метод для экшена shopReportsproductsActions */
    public function getProductsTop($limit, $order = 'sales', $start_date = null, $end_date = null, $options = array())
    {
        $paid_date_sql = self::getDateSql('o.paid_date', $start_date, $end_date);

        if ($order !== 'sales') {
            $order = 'profit';
        }
        $limit = (int)$limit;
        $limit = ifempty($limit, 10);

        $storefront_join = '';
        $storefront_where = '';
        if (!empty($options['storefront'])) {
            $storefront_join = "JOIN shop_order_params AS op2
                                    ON op2.order_id=o.id
                                        AND op2.name='storefront'";
            $storefront_where = "AND op2.value='".$this->escape($options['storefront'])."'";
        }
        if (!empty($options['sales_channel'])) {
            $storefront_join .= " JOIN shop_order_params AS opst2
                                    ON opst2.order_id=o.id
                                        AND opst2.name='sales_channel' ";
            $storefront_where .= " AND opst2.value='".$this->escape($options['sales_channel'])."' ";
        }

        // !!! With 15k orders this query takes ~3 seconds todo: Надо бы ускорить)))
        $units = $this->getStrategy()->getDimension()->getList();
        $unit_key = $this->unitKey();
        $sum_fields = array('sales','profit','sales_subtotal','discount','purchase');
        $result = array();
        foreach ($units as $type => $units_data)  {
            foreach ($units_data['units'] as $uk => $uv)  {
                $sales_subtotal = '((oi.price*o.rate*oi.quantity)'.
                    $this->getMultiplierString($uv['multiplier'])
                    .')';
                $order_subtotal = '(o.total+o.discount-o.tax-o.shipping)';
                $discount = "IF({$order_subtotal} <= 0, 0, ".$sales_subtotal."*o.discount / {$order_subtotal})";
                $purchase = '((IF(oi.purchase_price > 0, oi.purchase_price*o.rate, ps.purchase_price*pcur.rate)*oi.quantity)'.
                    $this->getMultiplierString($uv['multiplier'])
                    .')';

                $sql = "SELECT
                    p.*,
                    SUM({$sales_subtotal} - {$discount}) AS sales,
                    SUM({$sales_subtotal} - {$discount} - {$purchase}) AS profit,
                    SUM({$sales_subtotal}) AS sales_subtotal,
                    SUM({$discount}) AS discount,
                    SUM({$purchase}) AS purchase
                FROM shop_order AS o
                    JOIN shop_order_items AS oi
                        ON oi.order_id=o.id
                    JOIN shop_product AS p
                        ON oi.product_id=p.id
                    JOIN shop_product_skus AS ps
                        ON oi.sku_id=ps.id
                    JOIN shop_currency AS pcur
                        ON pcur.code=p.currency
                    {$storefront_join}
                WHERE $paid_date_sql
                    AND oi.type = 'product'  AND oi.".$unit_key." = '".$uk."'
                    {$storefront_where}
                GROUP BY p.id
                ORDER BY $order DESC
                LIMIT $limit";

                foreach ($this->query($sql)->fetchAll() as $row) {
                    if(array_key_exists($row['id'], $result)) {
                        foreach ($sum_fields as $key) {
                            $result[$row['id']][$key] += (float)$row[$key];
                        }
                    } else {
                        $result[$row['id']] = $row;
                    }
                }
            }
        }

        return $result;
    }
    /* Original shopServicesModel::getTop Перенесенный метод для экшена shopReportsproductsActions */
    public function getServicesTop($limit, $start_date = null, $end_date = null, $options=array())
    {
        $paid_date_sql = array();
        if ($start_date) {
            $paid_date_sql[] = "o.paid_date >= DATE('".$this->escape($start_date)."')";
        }
        if ($end_date) {
            $paid_date_sql[] = "o.paid_date <= DATE('".$this->escape($end_date)."')";
        }
        if ($paid_date_sql) {
            $paid_date_sql = implode(' AND ', $paid_date_sql);
        } else {
            $paid_date_sql = "o.paid_date IS NOT NULL";
        }

        $limit = (int) $limit;
        $limit = ifempty($limit, 10);

        $storefront_join = '';
        $storefront_where = '';
        if (!empty($options['storefront'])) {
            $storefront_join = "JOIN shop_order_params AS op2
                                    ON op2.order_id=o.id
                                        AND op2.name='storefront'";
            $storefront_where = "AND op2.value='".$this->escape($options['storefront'])."'";
        }
        if (!empty($options['sales_channel'])) {
            $storefront_join .= " JOIN shop_order_params AS opst2
                                    ON opst2.order_id=o.id
                                        AND opst2.name='sales_channel' ";
            $storefront_where .= " AND opst2.value='".$this->escape($options['sales_channel'])."' ";
        }


        $units = $this->getStrategy()->getDimension()->getList();
        $unit_key =  $this->unitKey();
        $sum_fields = array('total');
        $result = array();
        foreach ($units as $type => $units_data)  {
            foreach ($units_data['units'] as $uk => $uv)  {
                $sql = "SELECT
                    s.*,
                    (SUM(oi.price*o.rate*oi.quantity)".
                    $this->getMultiplierString($uv['multiplier'])
                    .") AS total
                FROM shop_order AS o
                    JOIN shop_order_items AS oi
                        ON oi.order_id=o.id
                    JOIN shop_service AS s
                        ON oi.service_id=s.id
                    {$storefront_join}
                WHERE $paid_date_sql
                    AND oi.type = 'service' AND oi.".$unit_key." = '".$uk."'
                    {$storefront_where}
                GROUP BY s.id
                ORDER BY total DESC
                LIMIT $limit";

                foreach ($this->query($sql)->fetchAll() as $row) {
                    if(array_key_exists($row['id'], $result)) {
                        foreach ($sum_fields as $key) {
                            $result[$row['id']][$key] += (float)$row[$key];
                        }
                    } else {
                        $result[$row['id']] = $row;
                    }
                }
            }
        }

        return  $result;
    }
    /* Original shopTypeModel::getSales Перенесенный метод для экшена shopReportsproductsActions */
    public function getTypeSales($start_date = null, $end_date = null, $options = array())
    {
        $storefront_join = '';
        $storefront_where = '';
        if (!empty($options['storefront'])) {
            $storefront_join = "JOIN shop_order_params AS op2
                                    ON op2.order_id=o.id
                                        AND op2.name='storefront'";
            $storefront_where = "AND op2.value='".$this->escape($options['storefront'])."'";
        }

        // !!! With 15k orders this query takes ~2 seconds
        $paid_date_sql = shopOrderModel::getDateSql('o.paid_date', $start_date, $end_date);

        $units = $this->getStrategy()->getDimension()->getList();
        $unit_key =  $this->unitKey();
        $sum_fields = array('sales');
        $result = array();
        foreach ($units as $type => $units_data)  {
            foreach ($units_data['units'] as $uk => $uv)  {
                $sql = "SELECT
                    t.*, 
                    (SUM(ps.price*pcur.rate*oi.quantity)".
                    $this->getMultiplierString($uv['multiplier'])
                    .") AS sales
                FROM shop_order AS o
                    JOIN shop_order_items AS oi 
                        ON oi.order_id=o.id
                    JOIN shop_product AS p
                        ON oi.product_id=p.id
                    JOIN shop_product_skus AS ps
                        ON oi.sku_id=ps.id
                    JOIN shop_currency AS pcur 
                        ON pcur.code=p.currency
                    JOIN shop_type AS t
                        ON t.id=p.type_id 
                    {$storefront_join} 
                WHERE $paid_date_sql 
                    AND oi.type = 'product'  AND oi.".$unit_key." = '".$uk."'
                    {$storefront_where}
                GROUP BY t.id";

                foreach ($this->query($sql)->fetchAll() as $row) {
                    if(array_key_exists($row['id'], $result)) {
                        foreach ($sum_fields as $key) {
                            $result[$row['id']][$key] += (float)$row[$key];
                        }
                    } else {
                        $result[$row['id']] = $row;
                    }
                }
            }
        }
        return  $result;
    }

    protected function getMultiplierString($multiplier) {
        return ($multiplier> 0.0 && $multiplier != 1.0)?
            '/'.shopZzzfractionalPluginNumber::toString($multiplier):'';
    }
} 
