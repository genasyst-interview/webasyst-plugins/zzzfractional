<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopWorkflowEditAction extends shopZzzfractionalPluginWorkflowEditAction
{
    use shopZzzfractionalPluginAppTrait;

    public function execute($data = null)
    {
        $order = $this->order_model->getById($data['id']);

        # after editing every unsettled order became simple order
        if ($order['unsettled']) {
            $data['unsettled'] = 0;
        }

        $subtotal = 0;
        $services = $products = array();
        foreach ($data['items'] as $item) {
            if ($item['service_id']) {
                $services[] = $item['service_id'];
            } else {
                $products[] = $item['product_id'];
            }
        }
        $service_model = new shopServiceModel();
        $product_model = new shopProductModel();
        $services = $service_model->getById($services);
        $products = $product_model->getById($products);

        foreach ($data['items'] as &$item) {
            $item['currency'] = $order['currency'];
            $item['price'] = $this->price($item['price'], $order['currency']);

            if ($item['service_id']) {
                if (isset($services[$item['service_id']])) {
                    $item['service'] = $services[$item['service_id']];
                } else {
                    $item['service'] = array(
                        'name' => $item['name'],
                        'id'   => $item['service_id'],
                    );
                }
            } else {
                if (isset($products[$item['product_id']])) {
                    $item['product'] = $products[$item['product_id']];
                } else {
                    $item['product'] = array(
                        'name'   => $item['name'],
                        'id'     => $item['product_id'],
                        'sku_id' => $item['sku_id'],
                    );
                }
            }
            $subtotal += $item['price'] * $this->price($item['quantity']);
        }
        $data['items'] = $this->getStrategy()->encodeItemsCount($data['items']);
        unset($item);

        foreach (array('shipping', 'discount') as $k) {
            if (!isset($data[$k])) {
                $data[$k] = 0;
            } else {
                $data[$k] = $this->price($data[$k], $order['currency']);
            }
        }
        $contact = new waContact($order['contact_id']);
        $shipping_address = $contact->getFirst('address.shipping');
        if (!$shipping_address) {
            $shipping_address = $contact->getFirst('address');
        }
        $shipping_address = $shipping_address ? $shipping_address['data'] : array();
        $billing_address = $contact->getFirst('address.billing');
        if (!$billing_address) {
            $billing_address = $contact->getFirst('address');
        }
        $billing_address = $billing_address ? $billing_address['data'] : array();

        $discount_rate = $subtotal ? ($data['discount'] / $subtotal) : 0;

        $taxes_params = array(
            'shipping'      => $shipping_address,
            'billing'       => $billing_address,
            'discount_rate' => $discount_rate,
        );

        if (!empty($data['params']['shipping_tax_id'])) {
            $data['items']['%shipping%'] = array(
                'type'     => 'shipping',
                'tax_id'   => $data['params']['shipping_tax_id'],
                'quantity' => 1,
                'price'    => $data['shipping'],
                'currency' => $order['currency'],
            );
        }

        $taxes = shopTaxes::apply($data['items'], $taxes_params, $order['currency']);

        if (isset($data['items']['%shipping%']['tax'])) {
            $data['params']['shipping_tax'] = $data['items']['%shipping%']['tax'];
            $data['params']['shipping_tax_percent'] = $data['items']['%shipping%']['tax_percent'];
            $data['params']['shipping_tax_included'] = $data['items']['%shipping%']['tax_included'];
        } else {
            $data['params']['shipping_tax'] = null;
            $data['params']['shipping_tax_percent'] = null;
            $data['params']['shipping_tax_included'] = null;
        }
        unset($data['items']['%shipping%']);
        $tax = $tax_included = 0;
        foreach ($taxes as $t) {
            if (isset($t['sum'])) {
                $tax += $t['sum'];
            }
            if (isset($t['sum_included'])) {
                $tax_included += $t['sum_included'];
            }
        }

        $data['tax'] = $tax_included + $tax;
        $data['total'] = $subtotal + $tax + $data['shipping'] - $data['discount'];

        // for logging changes in stocks
        shopProductStocksLogModel::setContext(
            shopProductStocksLogModel::TYPE_ORDER,
            /*_w*/('Order %s was edited'),
            array(
                'order_id' => $data['id']
            )
        );

        if (!empty($data['params']['shipping_id'])) {
            try {
                if ($shipping_plugin = shopShipping::getPlugin(null, $data['params']['shipping_id'])) {
                    $shipping_currency = $shipping_plugin->allowedCurrency();
                    $data['params']['shipping_currency'] = $shipping_currency;
                    $rate_model = new shopCurrencyModel();
                    if ($row = $rate_model->getById($shipping_currency)) {
                        $data['params']['shipping_currency_rate'] = $row['rate'];
                    }
                }

            } catch (waException $ex) {

            }
        }

        // SAVE ORDER ITEMS
        $this->order_model->update($data, $data['id']);

        $this->waLog('order_edit', $data['id']);

        shopProductStocksLogModel::clearContext();

        if (!empty($data['params'])) {
            $this->order_params_model->set($data['id'], $data['params'], false);

            // Update shop customer source if this is the first order and source changed
            if (isset($data['params']['referer_host'])) {
                $scm = new shopCustomerModel();
                $shop_customer = $scm->getById($order['contact_id']);
                if (ifset($shop_customer, 'number_of_orders', null) == 1 && $shop_customer['source'] != $data['params']['referer_host']) {
                    $scm->updateById($order['contact_id'], array(
                        'source' => $data['params']['referer_host'],
                    ));
                }
            }
        }
        return true;
    }
}
