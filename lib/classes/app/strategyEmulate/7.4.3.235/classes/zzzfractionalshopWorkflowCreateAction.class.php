<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopWorkflowCreateAction extends shopZzzfractionalPluginWorkflowCreateAction
{
    use shopZzzfractionalPluginAppTrait;

    public function execute($data = null)
    {
        $data['items'] = $this->getStrategy()->decodeItemsCount($data['items']);/* Для подсчета итогов приводим в польз. вид */
        if (wa()->getEnv() == 'frontend') {
            // Save final stock and virtual stock in order params
            list($virtualstock, $stock) = self::determineOrderStocks($data);
            $data['params']['virtualstock_id'] = $virtualstock ? $virtualstock['id'] : null;
            $data['params']['stock_id'] = $stock ? $stock['id'] : null;
            self::fillItemsStockIds($data['items'], $virtualstock, $stock);
        }
        if (!empty($data['currency'])) {
            $currency = $data['currency'];
        } else {
            $currency = $this->getConfig()->getCurrency(false);
            $data['currency'] = $currency;
        }
        $rate_model = new shopCurrencyModel();
        $row = $rate_model->getById($currency);
        $rate = $row['rate'];

        // Save contact
        $contact = $this->getContact($data);

        // Calculate subtotal, taking currency conversion into account
        $subtotal = 0;
        foreach ($data['items'] as &$item) {
            if ($currency != $item['currency']) {
                $item['price'] = shop_currency($item['price'], $item['currency'], null, false);
                if (!empty($item['purchase_price'])) {
                    $item['purchase_price'] = shop_currency($item['purchase_price'], $item['currency'], null, false);
                }
                $item['currency'] = $currency;
            } else {
                $item['price'] = shop_currency($item['price'], $item['currency'], $currency, false);
            }
            $subtotal += $item['price'] * $item['quantity'];
        }
        unset($item);

        $data['shipping'] = shop_currency($data['shipping'], $currency, $currency, false);

        // Calculate discount, unless already set
        if ($data['discount'] === '') {
            $data['total'] = $subtotal;
            $data['discount_description'] = null;
            $data['discount'] = shopDiscounts::apply($data, $data['discount_description']);
        } else {
            if (empty($data['discount_description']) && !empty($data['discount'])) {
                $data['discount_description'] = sprintf_wp('Discount specified manually during order creation: %s', shop_currency($data['discount'], $currency, $currency));
            }
        }

        // Calculate taxes
        $shipping_address = $contact->getFirst('address.shipping');
        if (!$shipping_address) {
            $shipping_address = $contact->getFirst('address');
        }
        $billing_address = $contact->getFirst('address.billing');
        if (!$billing_address) {
            $billing_address = $contact->getFirst('address');
        }

        $discount_rate = $subtotal ? ($data['discount'] / $subtotal) : 0;
        $taxes_params = array(
            'shipping'      => isset($shipping_address['data']) ? $shipping_address['data'] : array(),
            'billing'       => isset($billing_address['data']) ? $billing_address['data'] : array(),
            'discount_rate' => $discount_rate,
        );
        if (!empty($data['params']['shipping_tax_id'])) {
            $data['items']['%shipping%'] = array(
                'type'     => 'shipping',
                'tax_id'   => $data['params']['shipping_tax_id'],
                'quantity' => 1,
                'price'    => $data['shipping'],
                'currency' => $currency,
            );
        }
        $taxes = shopTaxes::apply($data['items'], $taxes_params, $currency);
        if (isset($data['items']['%shipping%']['tax'])) {
            $data['params']['shipping_tax'] = $data['items']['%shipping%']['tax'];
            $data['params']['shipping_tax_percent'] = $data['items']['%shipping%']['tax_percent'];
            $data['params']['shipping_tax_included'] = $data['items']['%shipping%']['tax_included'];
        }
        unset($data['items']['%shipping%']);

        $tax = $tax_included = 0;
        foreach ($taxes as $t) {
            if (isset($t['sum'])) {
                $tax += $t['sum'];
            }
            if (isset($t['sum_included'])) {
                $tax_included += $t['sum_included'];
            }
        }

        $order = array(
            'state_id'  => 'new',
            'total'     => $subtotal - $data['discount'] + $data['shipping'] + $tax,
            'currency'  => $currency,
            'rate'      => $rate,
            'tax'       => $tax_included + $tax,
            'discount'  => $data['discount'],
            'shipping'  => $data['shipping'],
            'comment'   => isset($data['comment']) ? $data['comment'] : '',
            'unsettled' => !empty($data['unsettled']) ? 1 : 0,

        );
        $order['contact_id'] = $contact->getId();

        // Add contact to 'shop' category
        $contact->addToCategory('shop');

        // Save order
        $order_id = $this->order_model->insert($order);

        // Create record in shop_customer, or update existing record
        $scm = new shopCustomerModel();
        $scm->updateFromNewOrder($order['contact_id'], $order_id, ifset($data['params']['referer_host']));

        // save items
        $parent_id = null;
        $data['items'] = $this->getStrategy()->encodeItemsCount($data['items']);/* Для записи в бд приводим к базовому */
        foreach ($data['items'] as $item) {
            $item['order_id'] = $order_id;
            if ($item['type'] == 'product') {
                $parent_id = $this->order_items_model->insert($item);
            } elseif ($item['type'] == 'service') {
                $item['parent_id'] = $parent_id;
                $this->order_items_model->insert($item);
            }
        }

        // Order params
        if (empty($data['params'])) {
            $data['params'] = array();
        }

        if (!empty($data['params']['shipping_id'])) {
            try {
                if ($shipping_plugin = shopShipping::getPlugin(null, $data['params']['shipping_id'])) {
                    $shipping_currency = $shipping_plugin->allowedCurrency();
                    $data['params']['shipping_currency'] = $shipping_currency;
                    if ($row = $rate_model->getById($shipping_currency)) {
                        $data['params']['shipping_currency_rate'] = $row['rate'];
                    }
                }

            } catch (waException $ex) {

            }
        }

        $data['params']['auth_code'] = self::generateAuthCode($order_id);
        $data['params']['auth_pin'] = self::generateAuthPin();
        if (empty($data['params']['sales_channel'])) {
            if (empty($data['params']['storefront'])) {
                $data['params']['sales_channel'] = 'other:';
            } else {
                $data['params']['sales_channel'] = 'storefront:'.$data['params']['storefront'];
            }
        }

        if (!empty($data['params']['storefront'])) {
            $data['params']['signup_url'] = shopHelper::generateSignupUrl($contact, $data['params']['storefront']);
        }

        // Save params
        $this->order_params_model->set($order_id, $data['params']);

        // Write discounts description to order log
        if (!empty($data['discount_description']) && !empty($data['discount']) && empty($data['skip_description'])) {
            $this->order_log_model->add(array(
                'order_id'        => $order_id,
                'contact_id'      => $order['contact_id'],
                'before_state_id' => $order['state_id'],
                'after_state_id'  => $order['state_id'],
                'text'            => $data['discount_description'],
                'action_id'       => '',
            ));
        }

        $this->waLog('order_create', $order_id, null, $order['contact_id']);

        return array(
            'order_id'   => $order_id,
            'contact_id' => wa()->getEnv() == 'frontend' ? $contact->getId() : wa()->getUser()->getId()
        );
    }

}
