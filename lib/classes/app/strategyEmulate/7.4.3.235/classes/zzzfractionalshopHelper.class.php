<?php

class shopHelper extends shopZzzfractionalPluginHelper {
    public static function workupOrderItems($order_items, $options)
    {
        $options += array(
            'weight'   => null,
            'tax'      => null,
            'currency' => ifset($options['order_currency']),
        );
        $items = array();

        $values = array();

        if ($options['weight']) {
            $product_ids = array();
            foreach ($order_items as $i) {
                if (!empty($i['product_id'])) {
                    $product_ids[] = $i['product_id'];
                }
            }
            $product_ids = array_unique($product_ids);
            if ($product_ids) {
                $feature_model = new shopFeatureModel();
                $feature = $feature_model->getByCode('weight');
                if ($feature) {
                    $values_model = $feature_model->getValuesModel($feature['type']);
                    $values = $values_model->getProductValues($product_ids, $feature['id']);
                }
            }
        }

        foreach ($order_items as $item) {

            $item['price'] = ifempty($item['price'], 0.0);
            $item['price'] = shopHelper::workupValue($item['price'], 'price', $options['currency'], $options['order_currency']);

            $item['total_discount'] = ifempty($item['total_discount'], 0.0);
            $item['total_discount'] = shopHelper::workupValue($item['total_discount'], 'price', $options['currency'], $options['order_currency']);

            if ($options['weight']) {
                if (empty($item['weight'])) {
                    $item['weight'] = null;
                    if (ifset($item['type']) == 'product') {
                        if (!empty($item['sku_id']) && isset($values['skus'][$item['sku_id']])) {
                            $item['weight'] = $values['skus'][$item['sku_id']];
                        } elseif (!empty($item['product_id']) && isset($values[$item['product_id']])) {
                            $item['weight'] = $values[$item['product_id']];
                        }
                    }
                }

                $item['weight'] = shopHelper::workupValue($item['weight'], 'weight', $options['weight']);
            }

            $items[] = array(
                'id'             => ifset($item['id']),
                'name'           => ifset($item['name']),
                'sku'            => ifset($item['sku_code']),
                'tax_rate'       => ifset($item['tax_percent']),
                'tax_included'   => ifset($item['tax_included'], 1),
                'description'    => '',
                'price'          => (float)$item['price'],
                'quantity'       => ifset($item['quantity'], 0),
                'total'          => (float)$item['price'] * $item['quantity'],
                'type'           => ifset($item['type'], 'product'),
                'product_id'     => ifset($item['product_id']),
                'weight'         => (float)ifset($item['weight']),
                'weight_unit'    => (float)$options['weight'],
                'total_discount' => (float)$item['total_discount'],
                'discount'       => (float)($item['quantity'] ? ($item['total_discount'] / $item['quantity']) : 0.0),
            );


        }
        $strategy = (new shopZzzfractionalPluginFactory())->getStrategy();
        $strategy->workupOrderItems($items, $options);
        return array_values($items);
    }
}
