<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopCart extends shopZzzfractionalPluginCart
{
    use shopZzzfractionalPluginAppTrait;


    /**
     * Returns discount applicable to current customer's shopping cart contents, expressed in default currency.
     *
     * @param $order
     * @return float
     */
    public function discount(&$order = array())
    {
        $this->getStrategy()->setConvert(true);
        $total = parent::discount($order);
        $this->getStrategy()->setConvert(null);
        return $total;
    }

    /**
     * Returns number of items in current customer's shopping cart
     *
     * @return int
     */
    public function count()
    {
        return $this->model->count($this->code, 'product');
    }

    /**
     * Returns information about current shopping cart's items.
     *
     * @param bool $hierarchy Whether selected services must be included as 'services' sub-array for applicable items.
     *     If false, services are included as separate array items.
     * @return array
     */
    public function items($hierarchy = true)
    {
        $items =  parent::items($hierarchy);
        $items =  $this->getStrategy()->decodeItemsCount($items);
        return $items;
    }

    /**
     * Changes quantity for current shopping cart's item with specified id.
     *
     * @param int $item_id Item id
     * @param int $quantity New quantity
     */
    public function setQuantity($item_id, $quantity)
    {
        $item = $this->model->getById($item_id);
        $this->getStrategy()->encodeAndCastCountByProduct($quantity, $this->getProductModel()->getById($item['product_id']));
        parent::setQuantity($item_id,  $quantity);
    }

    public function addItem($item, $services = array())
    {
        $this->getStrategy()->encodeAndCastCountByProduct($item['quantity'], $this->getProductModel()->getById($item['product_id']));
        return parent::addItem($item, $services);
    }
    
    public function getItemTotal($item_id)
    {
        if (is_array($item_id)) {
            $item_id = $item_id['id'];
            $item = $this->getItem($item_id);
        } else {
            // this gives price already rounded for frontend
            $item = $this->getItem($item_id);
        }

        $cart_items_model = new shopCartItemsModel();
        $items = $cart_items_model->getByField('parent_id', $item['id'], true);
        $this->getStrategy()->decodeItemCount($item);
        $this->getStrategy()->decodeItemsCount($items);
        $price = shop_currency($item['price'], $item['currency'], null, false) * $item['quantity'];
        if (!$items) {
            return $price;
        }

        $variants = array();
        foreach ($items as $s) {
            $variants[] = $s['service_variant_id'];
        }

        $product_services_model = new shopProductServicesModel();
        $sql = "SELECT v.id, s.currency, ps.sku_id, ps.price, v.price base_price
                    FROM shop_service_variants v
                        LEFT JOIN shop_product_services ps
                            ON v.id = ps.service_variant_id
                                AND ps.product_id = i:0
                                AND (ps.sku_id = i:1 OR ps.sku_id IS NULL)
                        JOIN shop_service s
                            ON v.service_id = s.id
                WHERE v.id IN (i:2)
                ORDER BY ps.sku_id";
        $rows = $product_services_model->query($sql, $item['product_id'], $item['sku_id'], $variants)->fetchAll();
        $prices = array();
        foreach ($rows as $row) {
            if (!isset($prices[$row['id']]) || $row['price']) {
                if ($row['price'] === null) {
                    $row['price'] = $row['base_price'];
                }
                $prices[$row['id']] = $row;
            }
        }

        $rounding_enabled = shopRounding::isEnabled();
        $frontend_currency = wa('shop')->getConfig()->getCurrency(false);
        $round_services = wa()->getSetting('round_services');

        foreach ($items as $s) {
            if (!isset($prices[$s['service_variant_id']])) {
                continue;
            }
            $v = $prices[$s['service_variant_id']];
            if ($v['currency'] == '%') {
                $v['price'] = $v['price'] * $item['price'] / 100;
                $v['currency'] = $item['currency'];
            }

            $service_price = shop_currency($v['price'], $v['currency'], $frontend_currency, false);
            if ($rounding_enabled && (($v['currency'] != $frontend_currency) || ($round_services))) {
                $service_price = shopRounding::roundCurrency($service_price, $frontend_currency);
            }

            $price += $service_price * $item['quantity'];
        }
        return $price;
    }

}
