<?php


class shopZzzfractionalPluginStrategyConfig extends shopZzzfractionalPluginConfig
{
    protected static $_instance = null;

    protected function getConvertConfigArray()
    {
        return array(
            'frontendcartadd::post' =>   array(
                'encodeandcastcountbyproduct' =>  array(
                    'shopcart::setquantity' => false,
                ),
            ),
            'frontendcartsave::post' =>   array(
                'encodeandcastcountbyproduct' =>  array(
                    'shopcart::setquantity' => false,
                ),
               
            ),
            'frontendcartdelete::post' =>   array(
                'decodeitemscount' =>  array(
                    'shopcart::getitemtotal' => false,
                ),
            ),
            'frontendcart::post' =>   array(
                'encodeandcastcountbyproduct' =>  array(
                    'shopcart::setquantity' => false,
                ),
            ),
            'frontendcheckout::post' =>   array(
                'decodeitemscount' =>   array(
                    'shopworkflowcreateaction::execute' => false,
                ),
            ), 
            'ordersave::post' =>  array(
                'decodeitemscount' =>  array(
                    'shopordersavecontroller::execute' => false,
                ),
            ),
        );
    }
}






