<?php

class shopProductMassUpdate extends shopZzzfractionalPluginProductMassUpdate
{
    public static function update($raw_skus, $raw_products = array())
    {
        self::emulateSkus($raw_skus);
        parent::update($raw_skus, $raw_products);
        
    }
    
    protected static function emulateSkus(&$raw_skus) {
        if(is_array($raw_skus) && !empty($raw_skus)) {
            $strategy = wao(new shopZzzfractionalPluginFactory())->getStrategy();
            $sku_ids = array_keys($raw_skus);

            $product_model = new shopProductModel();
            $unit_key = $strategy->unitKey();
            if($product_model->fieldExists($unit_key)) {
                $sql = "SELECT  s.id,  p.".$unit_key." FROM shop_product_skus  s
                                    JOIN shop_product p ON s.product_id = p.id
                                    where s.id in (".implode(',',$sku_ids).")";
                $sku_products  = $product_model->query($sql)->fetchAll('id');
                foreach ($raw_skus as $id => &$sku)  {
                    if(array_key_exists($id, $sku_products)) {
                        $product_data = $sku_products[$id];
                        $f = shopZzzfractionalPluginDimension::isFractional($product_data[$unit_key]);
                        
                        if(array_key_exists('stock', $sku) && !empty($sku['stock'])) {
                            foreach ($sku['stock'] as $sid => &$v) {
                                if(is_array($v) && array_key_exists('count', $v)) {
                                    $sku['stock'][$sid]['count'] = (!$f)?(int)$v['count'] : $strategy->encodeCount($v['count'],  $product_data[$unit_key]);
                                } elseif(!is_array($v)) {
                                    $sku['stock'][$sid] = (!$f)?(int)$v : $strategy->encodeCount($v, $product_data[$unit_key]);
                                }
                            }
                            unset ($v);
                        }
                        if(array_key_exists('count', $sku)) {
                            $sku['count'] = (!$f)?(int)$sku['count'] : $strategy->encodeCount($sku['count'], $product_data[$unit_key], null);
                        }
                    }
                }
                unset($sku);
            }
        }
    }
    

}