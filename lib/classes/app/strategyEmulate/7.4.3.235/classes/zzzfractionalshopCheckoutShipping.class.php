<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopCheckoutShipping extends shopZzzfractionalPluginCheckoutShipping
{
    use shopZzzfractionalPluginAppTrait;

    public function getItems($weight_unit = null)
    {
        $items = array();
        $cart_items = $this->cart->items();

        #get actual order items weight
        $product_ids = $sku_ids = array();
        foreach ($cart_items as $item) {
            $product_ids[] = $item['product_id'];
            $sku_ids[] = $item['sku_id'];
        }
        $values = array();
        #get weight unit multiplier
        $m = null;

        $feature_model = new shopFeatureModel();
        $f = $feature_model->getByCode('weight');
        if ($f) {
            $values_model = $feature_model->getValuesModel($f['type']);
            if ($values_model) {
                $values = $values_model->getProductValues($product_ids, $f['id']);
                if ($values) {
                    if ($weight_unit) {
                        $dimension = shopDimension::getInstance()->getDimension('weight');
                        if ($weight_unit != $dimension['base_unit']) {
                            $m = $dimension['units'][$weight_unit]['multiplier'];
                        }
                    }
                }
            }
        }

        $strategy = $this->getStrategy();
        $weight_unit_key = $strategy->weightKey();
        $strategy->decodeItemsCount($cart_items);
        $strategy->addItemsData($cart_items);
        foreach ($cart_items as &$item) {
            if (isset($values['skus'][$item['sku_id']])) {
                $w = $values['skus'][$item['sku_id']];
            } else {
                $w = isset($values[$item['product_id']]) ? $values[$item['product_id']] : 0;
            }
            if ($m !== null) {
                $w = $w / $m;
            }
            if(array_key_exists($weight_unit_key,  $item)) {
                if ($m !== null) {
                    $w = $item[$weight_unit_key] / $m;
                } else {
                    $w = $item[$weight_unit_key];
                }
            }
            $item['_weight'] = $w;
            unset($item);
        }

        foreach ($cart_items as $item) {
            $items[] = array(
                'name'     => $item['name'],
                'price'    => $item['price'],
                'currency' => $item['currency'],
                'quantity' => $item['quantity'],
                'weight'   => $item['_weight'],
            );
        }
        return $items;
    }
}
