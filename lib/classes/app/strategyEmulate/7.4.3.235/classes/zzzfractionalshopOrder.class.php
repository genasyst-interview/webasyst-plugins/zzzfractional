<?php
include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopOrder extends shopZzzfractionalPluginOrder
{
    use shopZzzfractionalPluginAppTrait;
    protected function getSubtotal()
    {
        $subtotal = 0.0;
        $this->items; 

        $this->data['items'] =  $this->getStrategy()->decodeItemsCount($this->data['items']);
        foreach ($this->items as $i) {
            if(array_key_exists('type', $i)) {
                switch ($i['type']) {
                    case 'product':
                        $this->formatValues($i, self::$product_fields);
                        break;
                    case 'service':
                        $this->formatValues($i, self::$service_fields);
                        break;
                }
            } else {
                $i['price'] = $this->formatValue($i['price'], 'float');
                $i['quantity'] = $this->formatValue($i['quantity'], 'int');
            }
            $subtotal += $i['price']*$i['quantity'];
        }
         return $subtotal;
    }
   
    /**
     * @todo complete & test validation code
     */
    protected function validateStockExceed()
    {
        $this->product_skus_model = new shopProductSkusModel();

        $sku_ids = array();

        // calc current quantity usage
        $items_actual = $this->data['items'];
        $this->getStrategy()->encodeItemsCount($items_actual);
        $usage = $this->getStockUsage($items_actual, $sku_ids);

        // calc old quantity usage of this order (if order is new, than array will be empty)
        $old_usage = $this->getStockUsage($this->initOriginalItems(), $sku_ids);

        // calc stock counts
        $sku_stocks = $this->stocks();

        $skus = $this->product_skus_model->getByField('id', $sku_ids, 'id');
        $counts = array();

        foreach ($sku_stocks as $sku_id => &$stock) {
            if (!is_array($stock)) {
                    $counts[$sku_id][0] = $stock;
            } else {
                foreach ($stock as $stock_id => $st) {
                    $counts[$sku_id][$stock_id] = $st['count'];
                }
            }
        }

        // summarize stock counts with old usage as if temporary return items to stocks
        foreach ($old_usage as $sku_id => $ou) {
            if (!isset($counts[$sku_id])) {
                continue;
            }
            if (!is_array($counts[$sku_id])) {
                $cnt = array_sum((array)$ou);
                if ($counts[$sku_id] !== null) {
                    $counts[$sku_id] += $cnt;
                }
            } else {
                if (is_array($ou)) {
                    foreach ($ou as $stock_id => $cnt) {
                        if (isset($counts[$sku_id][$stock_id])) {
                            $counts[$sku_id][$stock_id] += $cnt;
                        }
                    }
                } else {
                    $stock_ids = array_keys($counts[$sku_id]);
                    $first_stock_id = reset($stock_ids);
                    $counts[$sku_id][$first_stock_id] += $ou;
                }
            }
        }

        // AND NOW check CURRENT USAGE does not exceed COUNT in stocks
        $error_sku_id = array();
        foreach ($usage as $sku_id => $u) {
            if (!isset($counts[$sku_id])) {
                continue;
            }
            if (is_array($u)) {
                foreach ($u as $stock_id => $cnt) {
                    if (isset($old_usage[$sku_id][$stock_id]) && $old_usage[$sku_id][$stock_id] == $cnt) {
                        continue;
                    }
                    if (isset($counts[$sku_id][$stock_id]) && $cnt > $counts[$sku_id][$stock_id]) {
                        $error_sku_id[] = $sku_id;
                        break 2;
                    }
                }
            } else {
                if ($counts[$sku_id] !== null && $u > $counts[$sku_id]) {
                    $error_sku_id[] = $sku_id;
                    break;
                }
            }
        }

        // Error for some sku
        if ($error_sku_id) {
            $message = _w('The number of items your can add to the order is limited by the stock level');
            foreach ($error_sku_id as $sku_id) {
                if (isset($skus[$sku_id])) {
                    $sku = $skus[$sku_id];
                } else {
                    $sku = $this->product_skus_model->getById($sku_id);
                }
                $product_id = $sku['product_id'];
                $this->errors['order']['product'][$product_id]['sku_id'] = $sku_id;
                $this->errors['order']['product'][$product_id]['quantity'] = $message;
            }
        }
    }

    /**
     * @return bool
     */
    protected function validateStockSelection()
    {
        $this->validateMultiplicity();

        $sku_stocks = $this->stocks();

        $errors = array();
        $items = $this->data['items'];
        /* Для правильного сравнения остатков приводдим все в базовоые единицы */
        $this->getStrategy()->encodeItemsCount($items);
        foreach ($items as $index => $item) {
            switch ($item['type']) {
                case 'product':
                    $sku_id = ifset($item['sku_id']);
                    $stock_id = ifset($item['stock_id']);

                    if (empty($stock_id) && is_array($sku_stocks[$sku_id]) && !empty($sku_stocks[$sku_id])) {
                        # Stock not selected
                        $errors[$index]['stock_id'] = _w('Select stock');// *not selected
                    } elseif ($stock_id !== null) {
                        if ($stock_id) {
                            if (empty($sku_stocks[$sku_id]) || empty($sku_stocks[$sku_id][$stock_id])) {
                                # Stock was deleted
                                $errors[$index]['stock_id'] = _w('Select stock');// *deleted
                            }
                        } else {
                            if (!empty($sku_stocks[$sku_id])) {
                                $errors[$index]['stock_id'] = _w('Select stock');// *not selected common stock'
                            }
                        }
                    }
                    break;
            }
        }
        if (!empty($errors)) {
            $this->errors['order']['items'] = $errors;
        }

        return empty($this->errors);
    }

    protected function extendItems(&$items = null)
    {
        # collect data to extend order items
        $this->initItems($items);
        $product_item = array();

        if ($items === null) {
            $items = &$this->data['items'];
        }

        foreach ($items as $index => &$item) {
            if (!isset($item['_index'])) {
                $item['_index'] = $index;
            }
            /* $product_item - неявное поведение, при нарушении порядка items все сломается, бег SS?! */
            $product = $this->itemProduct($item);
            switch ($item['type']) {
                case 'product':
                    $item = $product_item = $this->extendProductItem($item, $product);
                    if (empty($item['id'])) {
                        $item['purchase_price'] = shop_currency($item['purchase_price'], $product['currency'], $this->currency, false);
                    }
                    break;
                case 'service':
                    $item = $this->extendServiceItem($item, $product_item, $product);
                    $item['_parent_index'] = $product_item['_index'];
                    $item['purchase_price']=0;
                    break;
            }

            # round to currency precision
            $item['price'] = shop_currency($item['price'], $this->currency, $this->currency, false);
            $item['total_discount'] = 0;

            unset($item);
        }
        $this->getStrategy()->encodeItemsCount($items);
        unset($items);
        //XXX extend new items with name, tax_id, purchase_price & sku_code
        # tax_id & tax_percent^tax_included
    }

   
    
    protected function castItemsFlat($data)
    {
        $type = 'edit';

        $items_map = self::extractValue($data, 'item', $type);
        $products = self::extractValue($data, 'product', $type);
        $skus = self::extractValue($data, 'sku', $type);
        $services = self::extractValue($data, 'service', $type);
        $variants = self::extractValue($data, 'variant', $type);
        $names = self::extractValue($data, 'name', $type);
        $prices = self::extractValue($data, 'price', $type);
        $quantities = self::extractValue($data, 'quantity', $type);
        $stocks = self::extractValue($data, 'stock', $type);

        $items = array();

        foreach ($items_map as $index => $item_id) {

            $quantity = $this->castPrice($quantities[$item_id]);
            $items[] = array(
                'id'                 => (int)$item_id,
                'name'               => ifset($names, $item_id, null),
                'product_id'         => (int)ifset($products, $item_id, null),
                'sku_id'             => (int)$skus[$item_id],
                'type'               => 'product',
                'service_id'         => null,
                'service_variant_id' => null,
                'price'              => (float)$prices[$item_id],
                'quantity'           => $quantity,
                'stock_id'           => !empty($stocks[$item_id]) ? intval($stocks[$item_id]) : null,
                'parent_id'          => null,
                '_index'             => sprintf('%d', $index),
            );

            if (!empty($services[$index])) {
                $service_index = 0;
                foreach ($services[$index] as $group => $services_grouped) {
                    /**@var string $group one of 'new','edit', 'item', 'add' */
                    foreach ($services_grouped as $k => $service_id) {
                        /** @var int $k */
                        $item = array(
                            'name'               => null,
                            'product_id'         => (int)$products[$item_id],
                            'sku_id'             => (int)$skus[$item_id],
                            'type'               => 'service',
                            'service_id'         => (int)$service_id,
                            'price'              => (float)$prices[$group][$index][$k],
                            'quantity'           =>  $this->castPrice($quantity),
                            'service_variant_id' => null,
                            'stock_id'           => null,
                            'parent_id'          => null,
                            '_parent_index' => sprintf('%d', $index),
                        );

                        $item['parent_id'] = (int)$item_id;

                        if ($group == 'item') {        // it's item for update: $k is ID of item
                            $item['id'] = (int)$k;
                            if (isset($names[$k])) {
                                $item['name'] = $names[$k];
                            }
                            $item['_index'] = sprintf('%d_%d', $index, $service_index++);
                        } else {
                            $item['_index'] = sprintf('%d_%d', $index, $k);
                        }
                        $item['parent_id'] = (int)$item_id;


                        if (!empty($variants[$index][$service_id])) {
                            $item['service_variant_id'] = (int)$variants[$index][$service_id];
                        }
                        $items[] = $item;
                    }
                }
            }
        }

        $type = 'add';
        $products = self::extractValue($data, 'product', $type);
        if ($products) {

            $skus = self::extractValue($data, 'sku', $type);
            $prices = self::extractValue($data, 'price', $type);
            $quantities = self::extractValue($data, 'quantity', $type);
            $services = self::extractValue($data, 'service', $type);
            $variants = self::extractValue($data, 'variant', $type);
            $stocks = self::extractValue($data, 'stock', $type);

            foreach ($products as $index => $product_id) {
                $product_id = intval($product_id);
                $sku_id = intval($skus[$index]);
                $quantity = $quantities[$index]['product'];
                $items[] = array(
                    'name'               => null,
                    'product_id'         => $product_id,
                    'sku_id'             => $sku_id,
                    'type'               => 'product',
                    'service_id'         => null,
                    'price'              => $prices[$index]['product'],
                    'currency'           => '',
                    'quantity'           => $quantity,
                    'service_variant_id' => null,
                    'stock_id'           => !empty($stocks[$index]['product']) ? $stocks[$index]['product'] : null,
                    'parent_id'           => null,
                );
                if (!empty($services[$index])) {
                    foreach ($services[$index] as $service_id) {
                        $service_id = intval($service_id);
                        $item = array(
                            'name'               => null,
                            'product_id'         => $product_id,
                            'sku_id'             => $skus[$index],
                            'type'               => 'service',
                            'service_id'         => $service_id,
                            'price'              => $prices[$index]['service'][$service_id],
                            'currency'           => '',
                            'quantity'           => $quantity,
                            'service_variant_id' => null,
                            'stock_id'           => null,
                            'parent_id'           => null,
                        );
                        if (!empty($variants[$index][$service_id])) {
                            $item['service_variant_id'] = intval($variants[$index][$service_id]);
                        }
                        $items[] = $item;
                    }
                }
            }
        }

        return $items;
    }

    /**
     * @param string $weight_unit `kg`,`g`, `oz` and etc
     * @return array
     */
    protected function shippingItems($weight_unit = null)
    {
        $currency = $this->currency;

        $shipping_items = array();
        $this->items;

        $this->extendItemsWeight($this->data['items']);

        $m = null;
        if ($weight_unit) {
            $dimension = shopDimension::getInstance()->getDimension('weight');
            if ($weight_unit != $dimension['base_unit']) {
                $m = $dimension['units'][$weight_unit]['multiplier'];
            }
        }
        $weight_key = $this->weightKey();
        $this->getStrategy()->addItemsData($this->data['items']);
     

        foreach ($this->items as $i) {
            $i['price'] = shop_currency($i['price'], $currency, $currency, false);
            if ($m !== null) {
                $i['weight'] = $i['weight'] / $m;
            }
            if(array_key_exists($weight_key,  $i)) {
                if ($m !== null) {
                    $i['weight'] = $i[$weight_key] / $m;
                } else {
                    $i['weight'] = $i[$weight_key];
                }
            }
            $shipping_items[] = array(
                'name'     => '',
                'price'    => $i['price'],
                'quantity' => $i['quantity'],
                'weight'   => $i['weight'],
            );
        }
        return $shipping_items;
    }

    protected function validateMultiplicity()
    {
        $items = $this->data['items'];
        foreach ($items as $index => $item) { 
            if ($item['type'] == 'product') {
                $product = $this->getAppHelper()->getProductByOrderItem($item);
               /* $sku = $this->getAppHelper()->getSkuByOrderItem($item); //  имя артикула */
                if($product) {
                    $error = false;
                    $error_product_name = substr($product['name'], 0, 15);
                    $this->getStrategy()->castCountByProduct($item['quantity'], $product, $error);
                    if($error) {
                        /* $error =  'Количество должно быть кратно '.$product[$unit_multiplicity].'!'; */
                        $error = $error.' Самое близкое значение: '.$item['quantity'].'';
                        $this->errors['order']['product'][$item['product_id']]['quantity'] = $error;
                        $this->errors['order']['product'][$item['product_id']]['sku_id'] = $item['sku_id'];
                         
                    }
                }
            }
        }
        return empty($this->errors);
    }
}
