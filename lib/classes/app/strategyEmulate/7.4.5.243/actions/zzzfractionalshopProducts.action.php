<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/app/shopZzzfractionalPluginApp.trait.php', 'shop'));

class shopProductsAction extends shopZzzfractionalPluginProductsAction
{
    use shopZzzfractionalPluginAppTrait;

    public function workupProducts(&$products) {
        parent::workupProducts($products);
        $this->getStrategy()->returnValueType(shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE_STRING);
        $this->getStrategy()->decodeProducts($products);
        $this->getStrategy()->returnValueType();
        /* Дополнительно переопределяем иконки количества */
        $this->replaceProductsIcons($products);
    }
    
    protected function replaceProductsIcons(&$products)
    {
        if ($this->getProductView() === 'skus') {
            $stock_model = new shopStockModel();
            $stocks = $stock_model->getAll('id');
            foreach ($products as &$product) {
                foreach ($product['skus'] as &$sku) {
                    $this->replaceSkuIcons($sku,$stocks);
                }
                unset($sku);
                if (isset($product['sku'])) {
                    $this->replaceSkuIcons($product['sku'], $stocks);
                }
            } 
            unset($product);
        }
    }
    protected function replaceSkuIcons(&$sku, $stocks) {
        if ($sku['stock'] === null || $sku['stock'] === '') {
            $sku_stock_icon = shopHelper::getStockCountIcon(null);
            $sku['count_icon_html'] = $sku_stock_icon;
        } else {
            foreach ($stocks as $stock_id => $stock) {
                if (is_array($sku['stock'])) {
                    $sku_stock_count = $sku['stock'][$stock['id']]['count'];
                    if ($sku_stock_count === null || $sku_stock_count === '') {
                        $sku_stock_icon = shopHelper::getStockCountIcon(null);
                        $sku['stock'][$stock['id']]['count'] = null;
                    } else {
                        $sku_stock_icon = shopHelper::getStockCountIcon($sku_stock_count);
                    }
                    $sku['stock'][$stock['id']]['icon_html'] = $sku_stock_icon;
                }
            }
        }
    }

    public function getPluginRoot()  {
        return wa()->getAppPath('/');
    }
} 