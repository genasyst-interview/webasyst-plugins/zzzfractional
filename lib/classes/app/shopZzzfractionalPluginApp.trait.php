<?php

/**
 * Используется в подменах классов приложения
 * Class shopZzzfractionalPluginAppTrait
 */
trait shopZzzfractionalPluginAppTrait
{

    /**
     * @return shopZzzfractionalPluginAppHelper
     */
    protected function getAppHelper()
    {
        static $helper = null;
        if ($helper === null) {
            $helper = new shopZzzfractionalPluginAppHelper();
        }

        return $helper;
    }

    /**
     * @see shopZzzfractionalPluginStrategyInterface::getController()
     * @return string
     */
    protected function getStringController()
    {
        return $this->getStrategy()->getController();
    }

    /**
     * Возвращает стратегию преобразования количества
     * @return shopZzzfractionalPluginStrategyInterface
     */
    protected function getStrategy()
    {
        return wao(new shopZzzfractionalPluginFactory())->getStrategy();
    }

    /**
     * @see shopZzzfractionalPluginStrategyInterface::getConfig()
     * @return shopZzzfractionalPluginConfig
     */
    protected function getStrategyConfig()
    {
        return $this->getStrategy()->getConfig();
    }

    /**
     * @see shopZzzfractionalPluginStrategyInterface::getProductModel()
     * @return shopProductModel
     */
    protected function getProductModel()
    {
        return $this->getStrategy()->getProductModel();
    }

    /**
     * @see shopZzzfractionalPluginStrategyInterface::unitKey()
     * @return string
     */
    public function unitKey()
    {
        return $this->getStrategy()->unitKey();
    }

    /**
     * @see shopZzzfractionalPluginStrategyInterface::multiplicityKey()
     * @return string
     */
    public function multiplicityKey()
    {
        return $this->getStrategy()->multiplicityKey();
    }

    /**
     * @see shopZzzfractionalPluginStrategyInterface::weightKey()
     * @return string
     */
    public function weightKey()
    {
        return $this->getStrategy()->weightKey();
    }

    /**
     * Проверка версии приложения и установка стратегии при обновлении самого приложения
     */
    public function __destruct()
    {
        shopZzzfractionalPluginStrategyInstallerVersion::factory()->run();
    }
}

