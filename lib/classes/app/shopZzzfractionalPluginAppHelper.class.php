<?php

class shopZzzfractionalPluginAppHelper
{
    private $_app_id = 'shop';

    /**
     * @param string $name
     * @param null   $options
     *
     * @return waModel
     * @throws waException
     */
    protected function getModel($name = '', $options = null)
    {
        static $_models_pool = array();

        if (array_key_exists($name, $_models_pool)) {
            return $_models_pool[$name];
        }
        if (!empty($name)) {
            $class_name = $this->_app_id . ucfirst($name) . 'Model';
            if (class_exists($class_name)) {
                if ($options) {
                    $_models_pool[$name] = new $class_name($options);
                } else {
                    $_models_pool[$name] = new $class_name();
                }

                return $_models_pool[$name];
            }
        }
        throw new waException('Вызвана несуществующая модель приложения (' . (string)$name . ')!');
    }

    protected function getEntityId($data = array(), $default = null)
    {
        if (is_array($data) && array_key_exists('id', $data)) {
            return $data['id'];
        } else if (is_numeric($data) && intval($data) > 0) {
            return $data;
        }

        return $default;
    }

    public function getEntityName($data = array(), $default = '')
    {
        return $this->getEntityColumn($data, 'name', $default);
    }

    public function getEntityColumn($data = array(), $key, $default = null)
    {
        if (is_array($data) && array_key_exists($key, $data)) {
            return $data[$key];
        }

        return $default;
    }

    public function getProduct($id = 0)
    {
        return $this->getProductById($id);
    }

    public function getProductById($id = 0)
    {
        return $this->getModel('product')->getById($id);
    }

    public function getProductColumn($product, $key, $default = null)
    {
        if ((is_array($product) || $product instanceof ArrayAccess) && array_key_exists($key, $product)) {
            return $product[$key];
        }
        $id = $this->getEntityId($product);
        if (intval($id) > 0) {
            return $this->getProductColumnById($id, $key, $default);
        }

        return $default;
    }

    public function getProductColumnById($product_id, $key, $default = null)
    {
        $product = $this->getModel('product')->getById($product_id);

        return $this->getEntityColumn($product, $key, $default);
    }

    /* SKU */
    public function getSku($id = 0)
    {
        return $this->getSkuById($id);
    }

    public function getSkuById($id = 0)
    {
        return $this->getModel('productSkus')->getById($id);
    }

    public function getProductBySku($sku)
    {
        $product_id = $this->getProductIdBySku($sku);
        if ($product_id) {
            return $this->getModel('product')->getById($product_id);
        }

        return null;
    }

    public function getProductIdBySku($sku)
    {
        $sku_id = false;
        if (is_array($sku)) {
            if (array_key_exists('product_id', $sku)) {
                return $sku['product_id'];
            } elseif (array_key_exists('id', $sku)) {
                $sku_id = $sku['id'];
            }
        } elseif (is_numeric($sku) && intval($sku) > 0) {
            $sku_id = $sku;
        }
        if ($sku_id) {
            $sku_data = $this->getSkuById($sku_id);
            if (is_array($sku_data) && array_key_exists('product_id', $sku_data)) {
                return $sku_data['product_id'];
            }
        }

        return null;
    }

    /* SKUS */
    public function getProductsBySkus($skus)
    {
        $product_ids = $this->getProductIdsBySkus($skus);
        if (!empty($product_ids)) {
            return $this->getModel('product')->getById($product_ids);
        }

        return array();
    }

    public function getProductIdsBySkus($skus)
    {
        $product_ids = $sku_ids = array();
        foreach ($skus as $v) {
            if (array_key_exists('product_id', $v)) {
                $product_ids[$v['product_id']] = $v['product_id'];
            } elseif (array_key_exists('id', $v)) {
                $sku_ids[$v['id']] = $v['id'];
            }
        }
        if (!empty($product_ids)) {
            return $product_ids;
        } elseif (!empty($sku_ids)) {
            $sql = "SELECT DISTINCT product_id FROM shop_product_skus
                WHERE id IN (?)";

            return $this->getModel('productSkus')->query($sql, array($sku_ids))->fetchAll(null, true);
        }

        return array();
    }

    /* CART ITEM */
    public function getCartItem($id = 0)
    {
        return $this->getCartItemById($id);
    }

    public function getCartItemById($id = 0)
    {
        return $this->getModel('cartItems')->getById($id);
    }

    public function getProductByCartItem($item)
    {
        return $this->getProductByItem($item, 'cart');
    }

    public function getSkuByCartItem($item)
    {
        return $this->getSkuByItem($item, 'cart');
    }

    /* ORDER ITEM */
    public function getOrderItem($id = 0)
    {
        return $this->getCartItemById($id);
    }

    public function getOrderItemById($id = 0)
    {
        return $this->getModel('orderItems')->getById($id);
    }

    public function getProductByOrderItem($item)
    {
        return $this->getProductByItem($item, 'order');
    }

    public function getSkuByOrderItem($item)
    {
        return $this->getSkuByItem($item, 'order');
    }

    /* TYPE ITEM */
    public function getProductByItem($item, $item_type = null)
    {
        if (is_array($item)) {
            if (array_key_exists('product', $item)) {
                return $item['product'];
            }
        }
        $product_id = $this->getProductIdByItem($item, $item_type);
        if ($product_id) {
            return $this->getModel('product')->getById($product_id);
        }

        return null;
    }

    public function getProductIdByItem($item, $item_type = null)
    {
        $item_id = false;
        if (is_array($item)) {
            if (array_key_exists('product_id', $item)) {
                return $item['product_id'];
            } elseif (array_key_exists('id', $item)) {
                $item_id = $item['id'];
            }
        } elseif (is_numeric($item) && intval($item) > 0) {
            $item_id = $item;
        }
        if ($item_id) {
            $item_data = false;
            if ($item_type == 'cart') {
                $item_data = $this->getCartItemById($item_id);
            }
            if ($item_type == 'order') {
                $item_data = $this->getOrderItemById($item_id);
            }

            if (is_array($item_data) && array_key_exists('product_id', $item_data)) {
                return $item_data['product_id'];
            }
        }

        return null;
    }


    /* ITEMS */
    public function getProductsByCartItems($items)
    {
        return $this->getProductsByItems($items, 'cart');
    }

    public function getProductsByOrderItems($items)
    {
        return $this->getProductsByItems($items, 'order');
    }

    /* TYPE ITEMS */
    public function getProductsByItems($items, $items_type = null)
    {
        $product_ids = $this->getProductsIdsByItems($items, $items_type);
        if (!empty($product_ids)) {
            return $this->getModel('product')->getById($product_ids);
        }

        return array();
    }

    public function getProductsIdsByItems($items, $items_type = null)
    {
        $product_ids = $items_ids = array();

        foreach ($items as $v) {
            $product_id = false;
            if (array_key_exists('item', $v)) {
                $product_id = $v['item']['product_id'];
            } elseif (array_key_exists('product_id', $v)) {
                $product_id = $v['product_id'];
            } elseif (array_key_exists('id', $v)) {
                $items_ids[$v['id']] = $v['id'];
            }
            if ($product_id) {
                $product_ids[$product_id] = $product_id;
            }
        }
        if (!empty($product_ids)) {
            return $product_ids;
        } elseif (!empty($items_ids) && ($items_type === 'cart' || $items_type === 'order')) {
            $table = 'shop_' . strtolower($items_type) . '_items';
            $sql = "SELECT DISTINCT product_id FROM  {$table}  WHERE id IN (?)";

            return $this->getModel('cartItems')->query($sql, array($items_ids))->fetchAll(null, true);
        }

        return array();
    }

    public function getSkuByItem($item, $item_type = null)
    {
        if (is_array($item)) {
            if (array_key_exists('sku', $item)) {
                return $item['sku'];
            }
        }
        $sku_id = $this->getSkuIdByItem($item, $item_type);
        if ($sku_id) {
            return $this->getModel('productSkus')->getById($sku_id);
        }

        return null;
    }

    public function getSkuIdByItem($item, $item_type = null)
    {
        $item_id = false;
        if (is_array($item)) {
            if (array_key_exists('sku_id', $item)) {
                return $item['sku_id'];
            } elseif (array_key_exists('id', $item)) {
                $item_id = $item['id'];
            }
        } elseif (is_numeric($item) && intval($item) > 0) {
            $item_id = $item;
        }
        if ($item_id) {
            $item_data = false;
            if ($item_type == 'cart') {
                $item_data = $this->getCartItemById($item_id);
            }
            if ($item_type == 'order') {
                $item_data = $this->getOrderItemById($item_id);
            }

            if (is_array($item_data) && array_key_exists('sku_id', $item_data)) {
                return $item_data['sku_id'];
            }
        }

        return null;
    }

}