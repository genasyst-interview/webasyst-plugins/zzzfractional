<?php

trait shopZzzfractionalPluginHelperUnitsTrait /*implements shopZzzfractionalPluginHelperUnitsInterface*/
{

    /* UNIT */
    protected function helper()
    {
        static $helper = null;
        if ($helper === null) {
            $helper = new shopZzzfractionalPluginAppHelper();
        }

        return $helper;
    }

    public function getUnit($unit_id = 'item')
    {
        return shopZzzfractionalPluginDimension::getUnit($unit_id);
    }

    public function getUnitType($unit_id = 'item')
    {
        $dimension = shopZzzfractionalPluginDimension::getInstance();

        return $dimension->getUnitType($unit_id);
    }

    public function getUnitName($unit_id = 'item')
    {
        return $this->helper()->getEntityName($this->getUnit($unit_id));
    }


    /* PRODUCT UNIT */
    public function getProductUnit($product)
    {
        $unit_id = $this->getProductUnitId($product);
        if ($unit_id) {
            return $this->getUnit($unit_id);
        }

        return null;
    }

    public function getProductUnitId($product)
    {
        $key = shopZzzfractionalPluginStrategyConfig::init()->unit();

        return $this->helper()->getProductColumn($product, $key);
    }

    public function getProductUnitName($product)
    {
        return $this->helper()->getEntityName($this->getProductUnit($product));
    }

    public function getProductUnitType($product)
    {
        $key = shopZzzfractionalPluginStrategyConfig::init()->unit();
        $unit_id = $this->helper()->getProductColumn($product, $key);
        if ($unit_id) {
            return $this->getUnitType($unit_id);
        }

        return null;
    }

    /* SKU UNIT */
    public function getSkuUnit($sku)
    {
        $unit_id = $this->getSkuUnitId($sku);
        if ($unit_id) {
            return $this->getUnit($unit_id);
        }

        return null;
    }

    public function getSkuUnitId($sku)
    {
        $unit_key = shopZzzfractionalPluginStrategyConfig::init()->unit();
        if (is_array($sku) && array_key_exists($unit_key, $sku)) {
            return $sku[$unit_key];
        }
        $product = $this->helper()->getProductBySku($sku);
        if ($product) {
            return $this->helper()->getProductColumn($product, $unit_key);
        }

        return null;
    }

    public function getSkuUnitName($sku)
    {
        return $this->helper()->getEntityName($this->getSkuUnit($sku));
    }

    public function getSkuUnitType($sku)
    {
        $unit_id = $this->getSkuUnitId($sku);
        if ($unit_id) {
            return $this->getUnitType($unit_id);
        }

        return null;
    }

    /* ITEM UNIT */
    public function getItemUnit($item, $item_type = null)
    {
        $unit_id = $this->getItemUnitId($item, $item_type);

        return $this->getUnit($unit_id);

    }

    public function getItemUnitId($item, $item_type = null)
    {
        $unit_key = shopZzzfractionalPluginStrategyConfig::init()->unit();
        if ($unit_id = $this->helper()->getEntityColumn($item, $unit_key, false)) {
            return $unit_id;
        }
        $item_type = $this->detectItemType($item, $item_type);
        $product = $this->helper()->getProductByItem($item, $item_type);

        return $this->helper()->getEntityColumn($product, $unit_key);
    }

    public function getItemUnitName($item, $item_type = null)
    {
        return $this->helper()->getEntityName($this->getItemUnit($item, $item_type));
    }

    public function getItemUnitType($item, $item_type = null)
    {
        $unit_id = $this->getItemUnitId($item, $item_type);
        if ($unit_id) {
            return $this->getUnitType($unit_id);
        }

        return null;
    }

    protected function detectItemType($item, $item_type = null)
    {
        if ($item_type !== 'cart' && $item_type !== 'order') {
            $item_type = 'cart';
            if (array_key_exists('order_id', $item) || array_key_exists('name', $item)) {
                $item_type = 'order';
            }
        }

        return $item_type;
    }

    /* Multiplicity */
    public function getProductMultiplicity($product)
    {
        $key = shopZzzfractionalPluginStrategyConfig::init()->multiplicity();

        return $this->helper()->getProductColumn($product, $key, 1);
    }

    public function getSkuMultiplicity($sku)
    {
        $key = shopZzzfractionalPluginStrategyConfig::init()->multiplicity();
        if ($multiplicity = $this->helper()->getEntityColumn($sku, $key)) {
            return $multiplicity;
        }

        return $this->helper()->getProductColumn($this->helper()->getProductBySku($sku), $key, 1);
    }

    public function getItemMultiplicity($item, $item_type = null)
    {
        $key = shopZzzfractionalPluginStrategyConfig::init()->multiplicity();
        if ($multiplicity = $this->helper()->getEntityColumn($item, $key)) {
            return $multiplicity;
        }
        $item_type = $this->detectItemType($item, $item_type);
        $item_unit = $this->getItemUnitId($item, $item_type);
        $default = shopZzzfractionalPluginDimension::getUnitMultiplicity($item_unit);

        return $this->helper()->getProductColumn($this->helper()->getProductByItem($item, $item_type), $key, $default);
    }
}