<?php

interface shopZzzfractionalPluginHelperUnitsInterface
{

    /* UNIT */
    public function getUnit($unit_id = 'item');

    public function getUnitType($unit_id = 'item');

    public function getUnitName($unit_id = 'item');

    /* ENTITIES */
    /* Product */
    public function getProductUnit($product);

    public function getProductUnitName($product);

    public function getProductUnitType($product);

    /* Sku */
    public function getSkuUnit($sku);

    public function getSkuUnitId($sku);

    public function getSkuUnitName($sku);

    public function getSkuUnitType($sku);

    /* Item */
    public function getItemUnit($item, $item_type = null);

    public function getItemUnitId($item, $item_type = null);

    public function getItemUnitName($item, $item_type = null);

    public function getItemUnitType($item, $item_type = null);

    /* Multiplicity */
    public function getProductMultiplicity($product);

    public function getSkuMultiplicity($sku);

    public function getItemMultiplicity($item, $item_type = null);

}