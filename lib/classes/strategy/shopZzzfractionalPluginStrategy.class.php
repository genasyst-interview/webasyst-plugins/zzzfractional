<?php

include_once(wa()->getAppPath('/plugins/zzzfractional/lib/classes/helpers/shopZzzfractionalPluginHelperUnits.trait.php', 'shop'));

abstract class shopZzzfractionalPluginStrategy implements shopZzzfractionalPluginStrategyInterface
{
    use shopZzzfractionalPluginHelperUnitsTrait;
    protected static $product_model = null;

    protected static $controller = null;

    /**
     * @var string
     * @see getActionString
     */
    protected static $action_string = null;

    protected static $strategy_type = null;


    public static function castProductUnit(&$data)
    {
        $unit_key = shopZzzfractionalPluginStrategyConfig::init()->unit();
        if (!isset($data[$unit_key]) || !shopZzzfractionalPluginDimension::isUnit($data[$unit_key])) {
            $data[$unit_key] = shopZzzfractionalPlugin::DEFAULT_UNIT;
        }
    }

    /**
     * TODO: Надо сделать пул данных продуктов, чтобы запросы не плодить
     * @see self::emulateSkus()
     *
     * @param $skus
     *
     * @return array|null
     */
    protected function getSkusProducts($skus)
    {
        return $this->helper()->getProductsBySkus($skus);
    }

    public function getController()
    {
        if (self::$controller == null) {
            $module = waRequest::param('zzzractional_module');
            $action = waRequest::param('zzzractional_action');
            if ($module === null) {
                $module = waRequest::param('module');
            }
            if ($action === null) {
                $action = waRequest::param('action');
            }
            // Если не были записаны параметры запроса сами достаем их
            if ($module === null) {
                $module = waRequest::get('module');
            }
            if ($action === null) {
                $action = waRequest::get('action');
            }
            self::$controller = strtolower($module . $action);
        }

        return self::$controller;
    }

    public function getDimension()
    {
        return shopZzzfractionalPluginDimension::getInstance();
    }

    protected function getEmulateString($type)
    {
        $emulate_type = false;
        if ($type == 'decode') {
            $emulate_type = 'decoded';
        } elseif ($type == 'encode') {
            $emulate_type = 'encoded';
        }

        return $emulate_type;
    }

    protected function getEmulateType($data)
    {
        $emulate_attr_key = $this->getConfig()->emulateAttr();
        if (array_key_exists($emulate_attr_key, $data)) {
            return $data[$emulate_attr_key];
        }

        return false;
    }

    public function addEmulateType(&$data, $type = '')
    {
        $data[$this->getConfig()->emulateAttr()] = $this->getEmulateString($type);
    }

    /**
     * Возвращает вес в базовой единице веса магазина (kg)
     *
     * @param $quantity - переданный вес
     * @param $unit     - единица измерения веса
     *
     * @return bool|float
     */
    public function getBaseWeight($quantity, $unit)
    {
        $weight = false;
        if ($this->isWeightUnit($unit)) {

            $dimension_class = $this->getDimension();
            $shop_weight = $this->getShopBaseWeightUnit();
            if ($unit != $shop_weight) {
                $weight = $dimension_class->convert($quantity, 'weight', $shop_weight, $unit);
            } else {
                $weight = $quantity;
            }
        }

        return $weight;
    }

    /**
     * Возвращает базоввую единицу веса магазина (kg)
     * @see getBaseWeight
     */
    public function getShopBaseWeightUnit()
    {
        $shop_weight = shopDimension::getInstance()->getDimension('weight');

        return $shop_weight['base_unit'];
    }

    /**
     * Проверяет является ли единица измерения весовой
     *
     * @param $unit
     *
     * @return bool
     */
    public function isWeightUnit($unit)
    {
        return ($this->getDimension()->getUnitType($unit) == 'weight');
    }


    /**
     * @param $items
     *
     * @return mixed
     */
    public function addItemsData(&$items = array())
    {
        $products = $this->getItemsProducts($items);
        $unit_key = $this->unitKey();
        $weight_key = $this->weightKey();
        foreach ($items as &$v) {
            $unit = $is_emulate = false;
            if (array_key_exists('item', $v)) {
                $item = &$v['item'];
            } else {
                $item = &$v;
            }
            $product_id = false;
            if (array_key_exists('product_id', $item)) {
                $product_id = $item['product_id'];
            }

            /* Определяем единицу итема или добавляем */
            if (array_key_exists($unit_key, $item)) {
                $unit = $item[$unit_key];
            } elseif ($product_id && array_key_exists($product_id, $products) && array_key_exists($unit_key, $products[$product_id])) {
                $unit = $products[$product_id][$unit_key];
                $item[$unit_key] = $unit;
            }
            if ($unit) {
                /* Добавляем вес для доставок */
                $weight = $this->getBaseWeight($item['quantity'], $unit);
                if ($weight) {
                    /* Костыль для доствки считается ввес на количество, надо разделить примерно */
                    $item[$weight_key] = $weight / $item['quantity'];
                } 
            }
            unset($item);
        }
        unset($v);

        return $items;
    }

    public function workupOrderItems(&$items, $options)
    {

        $weight_key = $this->weightKey();
        $unit_key = $this->unitKey();
        $this->addItemsData($items);
        if (!array_key_exists('collapse_quantity', $options)) {
            $options['collapse_quantity'] = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID)->getSettings('collapse_quantity');
        }
        if (!array_key_exists('weight_key', $options)) {
            $options['weight_key'] = 'weight';
        }
        foreach ($items as &$v) {
            if (array_key_exists('item', $v)) {
                $item = &$v['item'];
            } else {
                $item = &$v;
            }
            /* Добавляем информацию о весе */
            if (isset($item[$unit_key])) {
                if ($options['weight']) {
                    if (isset($item[$weight_key])) {
                        if ($this->getShopBaseWeightUnit() != $options['weight']) {
                            $item[$options['weight_key']] = shopHelper::workupValue($item[$weight_key], 'weight', $this->getShopBaseWeightUnit(), $options['weight']);
                        } else {
                            $item[$options['weight_key']] = $item[$weight_key];
                        }
                    }
                }
                $u = shopZzzfractionalPluginDimension::getUnit($item[$unit_key]);
                $item['unit_name'] = $u['name'];
                /* Для плагинов оплаты и доставки делаем одну позицию */
                if (shopZzzfractionalPluginDimension::isFractional($item[$unit_key]) && $options['collapse_quantity']) {
                    if (array_key_exists($options['weight_key'], $item)) {
                        $item[$options['weight_key']] = $item[$options['weight_key']] * $item['quantity'];
                    }
                    $item['name'] .= ' (' . $item['quantity'] . ' ' . $item['unit_name'] . ')';
                    $item['price'] = $item['price'] * $item['quantity'];
                    $item['discount'] = $item['total_discount'];
                    $item['quantity'] = 1;
                }
            }
        }
        unset($v);

        return $items;
    }

    /**
     * @param $items
     *
     * @return array|null
     */
    public function getItemsProducts($items)
    {
        return $this->helper()->getProductsByItems($items);
    }

    /**
     * Проверяет и добавляет в продукт данные единицы измерения и кратности
     *
     * @param array|shopProduct $_product - массив данных продукта,
     *                                    или пустой массив содержащий только id $product = array( 'id' => 34534 );
     *
     * @return array
     */
    protected function addProductData(&$_product)
    {

        /* Что только не кидают в параметры )) */
        if (!is_array($_product) && !($_product instanceof shopProduct)) {
            return $_product;
        }
        $product = $_product;
        $unit_key = $this->unitKey();
        $multiplicity_key = $this->multiplicityKey();
        if (!array_key_exists($unit_key, $_product) || !array_key_exists($multiplicity_key, $_product)) {
            $product = $this->getProductModel()->getById($_product['id']);
        }
        /* Для js передаем доп данные */
        $_product['zzzfractional'] = 0;
        if (array_key_exists($unit_key, $product)) {
            $_product[$unit_key] = $product[$unit_key];
            $_product['zzzfractional'] = shopZzzfractionalPluginDimension::isFractional($product[$unit_key]) ? '1' : '0';
        } else {
            shopZzzfractionalPluginStrategy::castProductUnit($_product);
        }

        if (array_key_exists($multiplicity_key, $product)) {
            $_product[$multiplicity_key] = $product[$multiplicity_key];
        }

        return $_product;
    }

    /**
     * @param        $count
     * @param string $default
     *
     * @return float|mixed|string
     */
    public function castCount($count, $default = '')
    {
        if ($count === '' || $count === null || !preg_match('@^\-?\d*([\.,](\d+)?)?$@', $count)) {
            $count = $default;
        } else {
            $count = str_replace(',', '.', $count);
            $count = shopZzzfractionalPluginNumber::roundTo(floatval($count));
        }

        return $count;
    }

    public function castReturnValue(&$value)
    {
        if (waRequest::param(shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE) == shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE_STRING) {
            $value = str_replace(',', '.', (string)$value);
        }

        return $value;
    }


    public static function getStrategyType()
    {
        if (self::$strategy_type == null) {
            /*
             $plugin = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID);
             if($plugin) {
                $strategy_type = $plugin->getSettings('strategy');
                if(!empty($strategy_type) && ($strategy_type == shopZzzfractionalPluginConfig::STRATEGY_EMULATE || $strategy_type == shopZzzfractionalPluginConfig::STRATEGY_REAL)) {
                    self::$strategy_type = $strategy_type;
                }
            } 
            */
            self::$strategy_type = shopZzzfractionalPluginConfig::STRATEGY_EMULATE;
        }

        return self::$strategy_type;
    }

    public function getProductModel()
    {
        if (self::$product_model == null) {
            self::$product_model = new shopProductModel();
        }

        return self::$product_model;
    }

    public function getConfig()
    {
        return shopZzzfractionalPluginStrategyConfig::init();
    }

    /**
     * @return string
     */
    protected function getActionString()
    {
        if (self::$action_string === null) {
            self::$action_string = $this->getController() . '::' . waRequest::method();
        }

        return self::$action_string;
    }

    /**
     * @param int $level
     *
     * @return array
     */
    protected function getCallerData($level = 1)
    {
        $data = array(
            'method' => '',
            'caller' => '',
        );
        $level = intval($level);

        $method_key = $level + 1;
        $called_key = $method_key + 1;
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, ($called_key + 1));
        if (array_key_exists($method_key, $backtrace)) {
            $data['method'] = strtolower($backtrace[$method_key]['function']);
        }
        if (array_key_exists($called_key, $backtrace)) {
            if (array_key_exists('class', $backtrace[$called_key])) {
                $data['caller'] .= strtolower($backtrace[$called_key]['class']) . '::';
            }
            if (array_key_exists('function', $backtrace[$called_key])) {
                $data['caller'] .= strtolower($backtrace[$called_key]['function']);
            }
        }

        return $data;
    }

    /**
     * @return bool
     */
    protected function isConvert()
    {

        $actions = $this->getConfig()->getConvertConfig();
        $caller_data = $this->getCallerData();

        /*  waLog::dump(array($this->getController() . '::' . waRequest::method() =>
             array(
                 $caller_data['method'] => $caller_data['caller']
             )
         ), 'shopzzzfractional_no.log');*/
        /* Если конвертация вызвана из самомго ллассаразрешаем */
        if (strpos($caller_data['caller'], strtolower(get_class($this))) !== false) {
            return true;
        }
        /* Проверяем на запрещающие правила */
        if (!empty($caller_data['method']) && array_key_exists($this->getActionString(), $actions)) {
            if (array_key_exists($caller_data['method'], $actions[$this->getActionString()])
                && array_key_exists($caller_data['caller'], $actions[$this->getActionString()][$caller_data['method']])
            ) {
                if (!$actions[$this->getActionString()][$caller_data['method']][$caller_data['caller']]) {
                    return false;
                }
            }
        }
        /* Если был установлен парамет запрещающий конвертацию */
        if (waRequest::param(shopZzzfractionalPluginConfig::PARAM_CONVERT) === false) {
            return false;
        }

        return true;
    }

    public function setConvert($flag = null)
    {
        waRequest::setParam(shopZzzfractionalPluginConfig::PARAM_CONVERT, $flag);
    }

    public function returnValueType($type = shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE_NUMERIC)
    {
        if ($type == shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE_NUMERIC
            || $type == shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE_STRING
        )
            waRequest::setParam(shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE, $type);
    }

    public function unitKey()
    {
        return $this->getConfig()->unit();
    }

    public function multiplicityKey()
    {
        return $this->getConfig()->multiplicity();
    }

    public function weightKey()
    {
        return $this->getConfig()->weight();
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        if (waSystemConfig::isDebug()) {
            waLog::log('Вызван несуществующий метод Стратегии: ' . $name, 'zzzfractional_strategy.log');
        }
    }
}



