<?php


class shopZzzfractionalPluginStrategyEmulate extends shopZzzfractionalPluginStrategy
{

    /**
     * Метод преобразуют количество для показа пользователю (эмуляция дробного)
     * @param null|int|string $count - количество для конвертации
     * @param  string $unit_id - единица измерения для конвертации от базовой
     * @param string|null $cast_default - для бесконечного количества вернуть по умолчанию строку или null
     * @return float|mixed|null|string
     */
    public function decodeCount(&$count = null, $unit_id, $cast_default = '')
    {
        $count = $this->castCount($count, $cast_default);
        $unit = $this->getDimension()->getUnit($unit_id);
        if ((!is_null($count) && $count !== '') && !empty($unit) && $unit['fractional'] == 1) {
            $count = $this->getDimension()->convert($count, $unit['type'], $unit_id);
            /* Если количество без дробной части, переводим в int */
            if(fmod($count, 1) == 0) {
                $count = (int)$count;
            }
        }
        $this->castReturnValue($count);
        return $count;
    }

    /**
     * Метод преобразуют количество из эмулированного для пользователя в реальное от базовой единицы
     * @param null|int|string $count - количество для конвертации
     * @param  string $unit_id - единица измерения для конвертации от базовой
     * @param string|null $cast_default - для бесконечного количества вернуть по умолчанию строку или null
     * @return float|mixed|null|string
     */
    public function encodeCount(&$count = null, $unit_id, $cast_default = '')
    {
        $count = $this->castCount($count, $cast_default);
        $unit = $this->getDimension()->getUnit($unit_id);
        if (($count != null || $count != '') && !empty($unit) && $unit['fractional'] == 1) {
            $count = (int)$this->getDimension()->convert($count, $unit['type'], null, $unit_id);
        }
        return $count;
    }

    /**
     * Метод преобразует количество либо в базовую единицу, либо в дробное
     * @param null|int|string $count - количество для конвертации
     * @param  string $unit_id - единица измерения для конвертации
     * @param string|null $cast_default - для бесконечного количества вернуть по умолчанию строку или null
     * @param string $type - кодирование или декодирование количества (decode,encode)
     * @return float|mixed|null|string
     */
    protected function convertCount($type = '', &$count = null, $unit_id, $cast_default = '')
    {
        $method = false;
        if ($type == 'decode') {
            $method = 'decodeCount';
        } elseif ($type == 'encode') {
            $method = 'encodeCount';
        }
        if ($method) {
            $count = $this->{$method}($count, $unit_id, $cast_default);
        }
        return $count;
    }


    /**********************************************************
     * Методы преобразуют количество $items в корзине и заказе
     **********************************************************/

    /**
     * Перевод количества для показа пользователю
     * @see emulateItemsCount()
     */
    public function decodeItemsCount(&$items)
    {
        /*if ($this->getActionString() == 'frontendcheckout::post' && waRequest::param('step', waRequest::request('step')) != 'confirmation') {
            return $items;
        } else*/if ($this->isConvert()) {
            $items = $this->emulateItemsCount($items, 'decode');
        }
        return $items;
    }

    /**
     * Кодирование количества для запись в бд
     * @see emulateItemsCount()
     */
    public function encodeItemsCount(&$items)
    {
        if ($this->isConvert()) {
            $items = $this->emulateItemsCount($items, 'encode');
        }
        return $items;
    }

    /**
     * Метод эмуляции
     * @param array $items - Массив позиций корзины или заказа
     * @param string $type - кодирование или декодирование количества (decode,encode)
     * @return array $items
     */
    protected function emulateItemsCount(&$items, $type = null)
    {
        if ($type) {
            $products = $this->getItemsProducts($items);
            $unit_key = $this->unitKey();
            $weight_key = $this->weightKey();

            foreach ($items as &$v) {
                $unit_id = $is_emulate = false;
                if (array_key_exists('item', $v)) {
                    $item = &$v['item'];
                } else {
                    $item = &$v;
                }
                $quantity = $item['quantity'];
                $product_id = $item['product_id'];

                /* Определяем единицу итема или добавляем */
                if (array_key_exists($unit_key, $item)) {
                    $unit_id = $item[$unit_key];
                } elseif (array_key_exists($product_id, $products) && array_key_exists($unit_key, $products[$product_id])) {
                    $unit_id = $products[$product_id][$unit_key];
                    $item[$unit_key] = $unit_id;
                }

                if ($unit_id) {
                    $item[$unit_key] = $unit_id;
                    $item['fractional'] = shopZzzfractionalPluginDimension::isFractional($unit_id)? '1' : '0';
                    /*  Костыль для эмуляции складов артикулов товара итема! */
                    /*if (array_key_exists('item', $v)) {
                        $pr = array($product_id => &$v);
                        $this->emulateProducts($pr, $type);
                    }*/
                    /* Конвертируем если тип конвертации не совпадает */
                    if ($this->getEmulateType($item) != $this->getEmulateString($type)) {
                        $quantity = $this->convertCount($type, $quantity, $unit_id);
                        $item['quantity'] = $quantity;
                        $this->addEmulateType($item, $type);
                    }

                    /* Добавляем вес для доставок */
                    $weight = $this->getBaseWeight($quantity, $unit_id);
                    if ($weight) {
                        $item[$weight_key] = $weight / $item['quantity'];
                    }
                }
            }
            unset($v);

        }

        return $items;
    }

    /**********************************************************
     * Методы преобразуют количество одного $item в корзине и заказе
     **********************************************************/

    /**
     * Конвертация количества в видимое
     * @see emulateItemCount()
     * @param array $item
     * @return array $item
     */
    public function decodeItemCount(&$item, $product = null)
    {
        if ($this->isConvert()) {
            return $this->emulateItemCount($item, $product, 'decode');
        }
        return $item;
    }

    /**
     * Приведение количества для запись в бд
     * @see emulateItemCount()
     * @param array $item
     * @return array $item
     */
    public function encodeItemCount(&$item, $product = null)
    {
        if ($this->isConvert()) {
            return $this->emulateItemCount($item, $product, 'encode');
        }
        return $item;
    }

    /**
     * @param array $item - Массив данных позиции заказа или корзины
     * @param null|array $product - Массив или объект продукта, чтобы не делать лишнего запроса
     * @param string $type - кодирование или декодирование количества (decode,encode)
     * @return array $item
     */
    protected function emulateItemCount(&$item, $product = null, $type = null)
    {
        if ($type) {
            $unit_key = $this->unitKey();
            $weight_key = $this->weightKey();
            if (array_key_exists('product_id', $item)) {
                if (!$product) {
                    $product = $this->getProductModel()->getById($item['product_id']);
                }
                if (is_array($product) && array_key_exists($unit_key, $product)) {
                    if ($this->getEmulateType($item) != $this->getEmulateString($type)) {
                        $item['quantity'] = $this->convertCount($type, $item['quantity'], $product[$unit_key]);
                    }
                    $weight = $this->isWeightUnit($product[$unit_key]);
                    if ($weight) {
                        $item[$weight_key] = $item['quantity'];
                    }
                }
                $item[$unit_key] = $product[$unit_key];
                $this->addEmulateType($item, $type);
            }
        }
        return $item;
    }


    /**********************************************************
     * Методы преобразуют количество в массиве $products, а также их skus
     **********************************************************/

    /**
     * Конвертация количества в видимое
     * @see emulateProducts()
     * @param array $products
     * @return array $products
     */
    public function decodeProducts(&$products)
    {
        if ($this->isConvert()) {
            return $this->emulateProducts($products, 'decode');
        }
        return $products;
    }

    /**
     * Приведение количества для запись в бд
     * @see emulateProducts()
     * @param array $products
     * @return array $products
     */
    public function encodeProducts(&$products)
    {
        if ($this->isConvert()) {
            return $this->emulateProducts($products, 'encode');
        }
        return $products;
    }

    /**
     * @param array $products - Массив данных продуктов, например полученных в коллекции shopProductsCollection::getProducts
     * @param string $type - кодирование или декодирование количества (decode,encode)
     * @return mixed
     */
    protected function emulateProducts(&$products, $type = null)
    {
        if ($type) {
            $unit_key = $this->unitKey();
            $pr_ids = array();
            foreach ($products as $product) {
                $pr_ids[$product['id']] = $product['id'];
            }
            $_products = $this->getProductModel()->getById($pr_ids);
            foreach ($products as &$product) {
                $unit = $is_emulate = false;
                $product_id = $product['id'];
                /* Определяем единицу итема или добавляем */
                if (array_key_exists($unit_key, $product)) {
                    $unit = $product[$unit_key];
                } elseif (array_key_exists($product_id, $_products) && array_key_exists($unit_key, $_products[$product_id])) {
                    $unit = $_products[$product_id][$unit_key];
                    $product[$unit_key] = $unit;
                }

                if ($unit && $this->getEmulateType($product) != $this->getEmulateString($type)) {
                    if (array_key_exists('count', $product)) {
                        $product['count'] = $this->convertCount($type, $product['count'], $unit, null);
                    }
                    if (array_key_exists('skus', $product)) {
                        foreach ($product['skus'] as &$sku) {
                            if ($this->getEmulateType($sku) != $this->getEmulateString($type)) {
                                if (!empty($sku['stock'])) {
                                    foreach ($sku['stock'] as $sid => $v) {
                                        if (is_array($v) && array_key_exists('count', $v)) {
                                            $sku['stock'][$sid]['count'] = $this->convertCount($type, $v['count'], $unit, null);
                                        } elseif (!is_array($v)) {
                                            $sku['stock'][$sid] = $this->convertCount($type, $v, $unit, null);
                                        }
                                    }
                                }
                                if (array_key_exists('count', $sku)) {
                                    $sku['count'] = $this->convertCount($type, $sku['count'], $unit, null);
                                }
                            }
                            $this->addEmulateType($sku, $type);

                        }
                        unset($sku);
                    }
                    if (array_key_exists('sku', $product)) {
                        $sku = &$product['sku'];
                        if ($this->getEmulateType($sku) != $this->getEmulateString($type)) {
                            if (!empty($sku['stock'])) {
                                foreach ($sku['stock'] as $sid => $v) {
                                    if (is_array($v) && array_key_exists('count', $v)) {
                                        $sku['stock'][$sid]['count'] = $this->convertCount($type, $v['count'], $unit, null);
                                    } elseif (!is_array($v)) {
                                        $sku['stock'][$sid] = $this->convertCount($type, $v, $unit, null);
                                    }
                                }
                            }
                            if (array_key_exists('count', $sku)) {
                                $sku['count'] = $this->convertCount($type, $sku['count'], $unit, null);
                            }
                        }
                        $this->addEmulateType($sku, $type);
                    }
                    $this->addEmulateType($product, $type);
                }
            }
            unset($product);
        }
        return $products;
    }

    /**
     * Конвертация количества в видимое
     * @see emulateProducts()
     * @param array $product
     * @return array $product
     */
    public function decodeProduct(&$product)
    {
       if(is_array($product)) {
           $products = array( 0 => &$product );
           $this->decodeProducts($products);
       }
       return $product;
    }

    /**
     * Приведение количества для запись в бд
     * @see emulateProducts()
     * @param array $product
     * @return array $product
     */
    public function encodeProduct(&$product)
    {
        if(is_array($product)) {
            $products = array( 0 => &$product );
            $this->encodeProducts($products);
        }
        return $product;
    }


    /**********************************************************
     * Методы преобразуют количество в массиве $skus
     **********************************************************/

    /**
     *  Конвертация количества в видимое
     * @param $skus
     * @return array $skus
     */
    public function decodeSkus(&$skus)
    {
        return $this->emulateSkus($skus, 'decode');
    }

    /**
     * Приведение количества для запись в бд
     * @param $skus
     * @return array $skus
     */
    public function encodeSkus(&$skus)
    {
        return $this->emulateSkus($skus, 'encode');
    }

    /**
     * @uses getSkusProducts()
     * @param $skus - Массив артикулов одного товраа или разных
     * @param string $type - кодирование или декодирование количества (decode,encode)
     * @return array $skus
     */
    protected function emulateSkus(&$skus, $type = null)
    {
        if ($type) {
            $products = $this->getSkusProducts($skus);
            $unit_key = $this->unitKey();

            foreach ($skus as &$sku) {
                $unit = false;
                if(array_key_exists('product_id', $sku)) {
                    $product_id = $sku['product_id'];
                    /* Определяем единицу итема или добавляем */
                    if (array_key_exists($unit_key, $sku)) {
                        $unit = $sku[$unit_key];
                    } elseif (array_key_exists($product_id, $products) && array_key_exists($unit_key, $products[$product_id])) {
                        $unit = $products[$product_id][$unit_key];
                        $sku[$unit_key] = $unit;
                    }

                    if ($unit && $this->getEmulateType($sku) != $this->getEmulateString($type)) {
                        if (!empty($sku['stock'])) {
                            foreach ($sku['stock'] as $sid => $v) {
                                if (is_array($v) && array_key_exists('count', $v)) {
                                    $sku['stock'][$sid]['count'] = $this->convertCount($type, $v['count'], $unit, null);
                                } elseif (!is_array($v)) {
                                    $sku['stock'][$sid] = $this->convertCount($type, $v, $unit, null);
                                }
                            }
                        }
                        if (array_key_exists('count', $sku)) {
                            $sku['count'] = $this->convertCount($type, $sku['count'], $unit,  null);
                        }
                        $this->addEmulateType($sku, $type);
                    }
                }
                
            }
            unset($sku);
        }
        return $skus;
    }

    /**
     *  Конвертация количества в видимое
     * @param $sku
     * @return array $sku
     */
    public function decodeSku(&$sku)
    {
        if(is_array($sku)) {
            $skus = array( 0 => &$sku );
            $this->decodeSkus($skus);
        }
        return $sku;
    }

    /**
     * Приведение количества для запись в бд
     * @param $sku
     * @return array $sku
     */
    public function encodeSku(&$sku)
    {
        if(is_array($sku)) {
            $skus = array( 0 => &$sku );
            $this->encodeSkus($skus);
        }
        return $sku;
    }


    /* systesm use */
    public function decodeStocks(&$stocks, $unit)
    {
        return $this->emulateStocks($stocks, $unit, 'decode');
    }

    /* systesm use */
    public function encodeStocks(&$stocks, $unit)
    {
        return $this->emulateStocks($stocks, $unit, 'encode');
    }

    /**
     *
     * @param $stocks
     * @param $unit
     * @param string $type - кодирование или декодирование количества (decode,encode)
     * @return array $stocks
     */
    protected function emulateStocks(&$stocks, $unit, $type = null)
    {
        if ($type) {
            foreach ($stocks as &$stock) {
                if (shopZzzfractionalPluginDimension::isUnit($unit)) {
                    if(is_array($stock) && array_key_exists('count', $stock)) {
                        $stock['count'] = $this->convertCount($type, $stock['count'], $unit,  null);
                    } else {
                        $stock = $this->convertCount($type, $stock, $unit,  null);
                    }
                }
            }
            unset($stock);
        }
        return $stocks;
    }

    /************************
     *  CART ACTIONS EMULATE
     ************************/

    /**
     * @param $count
     * @param $product_data
     * @return float|mixed|string
     */
    public function encodeAndCastCountByProduct(&$count, $product_data, &$error = null)
    {
        $caller = $this->getCallerData(0);
        if ($this->isConvert() || ($this->getActionString() == 'frontendcart::post' && waRequest::issetPost('checkout') && $caller['caller'] == 'shopcart::setquantity')) {
            return $this->emulateAndCastCountByProduct($count, $product_data, $error, 'encode');
        }
        return $count;
    }

    /**
     * @param $count
     * @param $product_data
     * @return float|mixed|string
     */
    public function decodeAndCastCountByProduct(&$count, $product_data, &$error = null)
    {
        if ($this->isConvert()) {
            return $this->emulateAndCastCountByProduct($count, $product_data, $error, 'decode');
        }
        return $count;
    }

    /**
     * @param $count
     * @param $product_data
     * @param null $type
     * @return float|mixed|string
     */
    protected function emulateAndCastCountByProduct(&$count, $product_data, &$error = null, $type = null)
    {
        if ($type && $product_data) {
            shopZzzfractionalPluginStrategy::castProductUnit($product_data);
            $unit_key = $this->unitKey();
            if ($type == 'encode') {
                $count = $this->castCountByProduct($count, $product_data, $error);
            }
            if (shopZzzfractionalPluginDimension::isFractional($product_data[$unit_key])) {
                $count = $this->convertCount($type, $count, $product_data[$unit_key]);
            }
            if ($type == 'decode') {
                $count = $this->castCountByProduct($count, $product_data, $error);
            }
        }
        return $count;
    }

    /**
     * @param $$quantity
     * @param $product_data
     * @return float|mixed|string
     */
    public function encodeCartAddQuantity(&$quantity, $sku_id, $product_id = null, &$error = null)
    {
        /* Не доваеряем присланным данным */
        if(!empty($sku_id)) {
            $sku = $this->helper()->getSkuById($sku_id);
        }
        if(empty($sku)) {
            $error = 'Не определен артикул товара!';
            return $quantity;
        }

        if(empty($product_id)) {
            $product_id = $sku['product_id'];
        }
        $product = $this->helper()->getProduct($product_id);
        if(empty($product)) {
            $error = 'Такого товара нет!';
            return $quantity;
        }

        $code = waRequest::cookie('shop_cart');
        $c = wao(new shopCartItemsModel())->countSku($code, $sku['id']);
        return $this->encodeCartQuantity($quantity, $product, $sku, $c, $error);
        
    }

    public function encodeCartSaveQuantity(&$quantity, $sku_id, $product_id = null, &$error = null)
    {
        if(!empty($sku_id)) {
            $sku = $this->helper()->getSkuById($sku_id);
        }
        if(empty($sku)) {
            $error = 'Не определен артикул товара!';
            return $quantity;
        }

        if(empty($product_id)) {
            $product_id = $sku['product_id'];
        }
        $product = $this->helper()->getProduct($product_id);
        if(empty($product)) {
            $error = 'Такого товара нет!';
            return $quantity;
        }
        
        return $this->encodeCartQuantity($quantity, $product, $sku, null, $error);

    }
    
    protected function encodeCartQuantity(&$quantity, $product, $encoded_sku, $encoded_cart_quantity = null, &$error = null)
    {
        $this->addProductData($product);
        $quantity = $this->encodeAndCastCountByProduct($quantity, $product, $error);
        if (!wa()->getSetting('ignore_stock_count'))  {
            /* Для экшена cartAdd мы сумируем для правильного количества, для cartSave просто меняем количество */
            $c = (is_null($encoded_cart_quantity))? 0 : intval($encoded_cart_quantity);
            $sku_count = $encoded_sku['count'];
            if ($sku_count !== null && (($c + $quantity) > $sku_count)) {
                $encode_quantity = $quantity = $sku_count - $c;
                $name = $product['name'].($encoded_sku['name'] ? ' ('.$encoded_sku['name'].')' : '');
                /* Делаем декодирование наличия с проверкой, мало ли ранее уже обработан sku */
                $skus = array($encoded_sku['id'] => $encoded_sku);
                $skus = $this->decodeSkus($skus);
                $decoded_sku = $skus[$encoded_sku['id']];
                if (floatval($quantity) <= 0.0) {
                    if ($decoded_sku['count'] > 0) {
                        $mess = _w('Only %d pcs of %s are available, and you already have all of them in your shopping cart.');
                    } else {
                         $mess = _w('Oops! %s just went out of stock and is not available for purchase at the moment. We apologize for the inconvenience.');
                    }
                } else {
                    $mess = _w('Only %d pcs of %s are available, and you already have all of them in your shopping cart.');
                }

                /**
                 * Дополнительная проверка количества с приведением остатка к кратному,
                 * иначе может отрицательное количество получиться на складе
                 **/
                $mess_quantity = $decode_quantity =  $this->decodeAndCastCountByProduct($encode_quantity, $product);
                if($decode_quantity > $decoded_sku['count']) {
                    $mess_quantity = $decode_quantity = $decode_quantity - $product[$this->multiplicityKey()];
                    $quantity = $this->encodeCount($decode_quantity, $product[$this->unitKey()]);
                }
                $replaces = array(
                    'find' => array('%d','%s'),
                    'replace' => array($mess_quantity, $name),
                );
                $mess = str_replace($replaces['find'], $replaces['replace'], $mess);
                if(!$error) {
                    $error .= ' '.$mess;
                } else {
                    $error = $mess;
                }
            }
        }
        return $quantity;
    }

    public function castCountByProduct(&$count, $product_data, &$error = null)
    {
        $count = $this->castCount($count);
        if ($product_data) {
            $config = $this->getConfig();
            $unit = $this->getProductUnitId($product_data);
            $multiplicity =  $this->getProductMultiplicity($product_data);
            $multiplicity_conversion = $config->multiplicityConversion();
            $display_error = ($config->multiplicityError() && $multiplicity_conversion);
            shopZzzfractionalPluginStrategy::castProductUnit($product_data);
            
            /* Проверяем на дробность и кратность */
            if (!shopZzzfractionalPluginDimension::isFractional($unit)) {
                if (fmod($count, 1) != 0 && $display_error) {
                    $error = 'Товар продается только целыми единицами, количество округлено до целого (' . $count . ')!';
                }
                $count = round($count, 0);
            } elseif ($display_error) {
                if ($count < $multiplicity) {
                    $error = 'Минимальное кратное количество ' . $multiplicity . '!';
                } elseif (fmod(round($count / $multiplicity, 4), 1) != 0.0) {
                    $error = 'Количество должно быть кратно ' . $multiplicity . '!';
                } 
            }
            /* Приводим к кратному */
            if ($multiplicity_conversion) {
                if ($count < $multiplicity) {
                    $count = $multiplicity;
                } else {
                    $count = shopZzzfractionalPluginNumber::roundToMultiplicity($count, $multiplicity);
                }
            }
        }
        return $count;
    }

    /*********************
     *  HOOKS   METHODS
     *********************/

    /**
     * @inheritdoc
     */
    public function frontendHead()
    {
        $html = '<script src="'.shopZzzfractionalPluginView::getTemplateUrl('shopZzzfractionalPluginFrontend.js').'"></script>';
        $files = shopZzzfractionalPluginView::getThemeCorrection();
        if (array_key_exists('js', $files)) {
            $html .= '<script src="'.$files['js'].'"></script>';
        }
        $path = shopZzzfractionalPluginView::getTemplateUrl('frontend.js');
        if (!empty($path)) {
            $html .= '<script src="'.$path.'"></script>';
        }
        if (array_key_exists('css', $files)) {
            $html .= ' <link href="'.$files['css'].'" rel="stylesheet" type="text/css" />';
        }
        $multiplicity_conversion = $this->getConfig()->multiplicityConversion();
        $display_error = ($this->getConfig() && $multiplicity_conversion);
        $settings = array(
            'multiplicity_conversion' => (int) $multiplicity_conversion,
            'multiplicity_error' => (int) $display_error
        );
        $html .= '<script>$.shopZzzfractionalPluginFrontend.setSettings('.json_encode($settings).');</script>';
        return $html;
    }

    /**
     * @inheritdoc
     */
    public function backendProductEdit(&$data)
    {
        $settings = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID)->getSettings();
        $product_types_settings = $settings['product_type'];
        $this->prepareProduct($data);
        $_data = array(
            'product' => $data,
            'dimension' => $this->getDimension(),
            'product_types' => $product_types_settings
        );

        return array('basics' => shopZzzfractionalPluginView::fetch($_data, 'BackendProductEdit.edit_basics.html'));
    }
    
    public function prepareProduct(&$data)
    {
        if ($data instanceof shopProduct) {
            if ((!$data instanceof shopZzzfractionalPluginProductDecorator)) {
                $data = new shopZzzfractionalPluginProductDecorator($data, (wa()->getEnv() == 'frontend'));
            }
        } elseif ((!$data instanceof shopZzzfractionalPluginProductDecorator)) {
            self::castProductUnit($data);
        }
    }
    
    /**
     * @inheritdoc
     */
    public function backendProduct(&$product)
    {
        // TODO: экшен трансфера сделать через хук
    }

    /**
     * @inheritdoc
     */
    public function backendOrder(&$order)
    {
        if (array_key_exists('items', $order)) {
            $order['items'] = $this->decodeItemsCount($order['items']);
        }
    }

    /**
     * @inheritdoc
     */
    public function backendOrderEdit(&$order)
    {
        if (array_key_exists('items', $order) && !empty($order['items'])) {
            $unit_key = $this->unitKey();
            $this->decodeProducts($order['items']);
            $sku_ids = array();
            foreach ($order['items'] as $item) {
                foreach ($item['skus'] as $sku) {
                    if (empty($sku['fake'])) {
                        $sku_ids[] = $sku['id'];
                    }
                }
            }

            // How many of each SKU are left in stock
            $skus_stocks = array();
            if ($sku_ids) {
                $product_stocks_model = new shopProductStocksModel();
                $skus_stocks = $product_stocks_model->getBySkuId($sku_ids);
            }
            foreach ($order['items'] as &$item) {

                if (empty($item['fake'])) {
                    $item['icon'] = shopHelper::getStockCountIcon($item['count'], null, true);
                }
                $this->decodeSkus($item['skus']);
                $skus = &$item['skus'];
                /* Взято из shopOrderEditActions::workupItems */
                foreach ($skus as &$sku) {
                    if (empty($sku['fake'])) {
                        // detailed stocks count icon for sku
                        if (empty($sku['stock'])) {
                            $sku['icon'] = shopHelper::getStockCountIcon($sku['count'], null, true);
                        } elseif(array_key_exists($sku['id'], $skus_stocks)) {
                            $icons = array();
                            $counts_htmls = array();
                            $sku_stocks = $skus_stocks[$sku['id']];
                            if(isset($sku[$unit_key]))  {
                                $this->decodeStocks($sku_stocks, $sku[$unit_key]);
                            }
                            foreach ($sku_stocks as $stock_id => $stock) {
                                $count = $this->getStockCount($stock);
                                $icons[$stock_id] = shopHelper::getStockCountIcon($count, $stock_id, true);
                                if ($count === null) {
                                    $counts_htmls[$stock_id] = sprintf(str_replace('%d', '%s', _w('%d left')), '∞');
                                } else {
                                    $mess = str_replace('%d', '%.2f', _w('%d left'));
                                    $counts_htmls[$stock_id] = _w($mess, $mess, $count);
                                }
                            }
                            $sku['icon'] = shopHelper::getStockCountIcon($sku['count'], null, true);
                            $sku['icons'] = $icons;
                            $sku['count_htmls'] = $counts_htmls;
                        }
                    }
                }
                unset($sku);
            }
        }
    }

    public function getStockCount($stock, $return = null) {
        $count = null;
        if(is_array($stock) && array_key_exists('count',$stock)) {
            $count = $stock['count'];
        } else {
            $count = $stock;
        }
        if($count === '' && $return === null) {
            $count = null;
        } elseif($count === null  &&  $return === '') {
            $count = '';
        }
        return $count;
    }
    /**
     * @inheritdoc
     */
    public function frontendProducts(&$data)
    {
        if ($this->isConvert()) {
            $unit_key = $this->unitKey();
            if (array_key_exists('products', $data)) {
                $this->decodeProducts($data['products']);
            }
            if (array_key_exists('skus', $data)) {
                $products = array();
                if (array_key_exists('products',  $data)) {
                    $products = $data['products'];
                }
                if(empty($products)) {
                    $products = $this->getSkusProducts($data['skus']);
                }
                $this->decodeSkus($data['skus']);
                foreach ($data['skus'] as &$v) {
                    if (array_key_exists($v['product_id'], $products)) {
                        $product = $data['products'][$v['product_id']];
                        // Используется в методе cartItems::total();
                        if (array_key_exists('quantity', $v) && array_key_exists($unit_key, $product)) {
                            $v['quantity'] = $this->decodeCount($v['quantity'], $product[$unit_key]);
                        }
                        $this->addEmulateType($v, 'decode');
                    }
                }
                unset($v);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function frontendProduct(&$product)
    {
        $this->addProductData($product);
        $return = array(
            'unit' => $product[$this->unitKey()],
            'unit_multiplicity' => $product[$this->multiplicityKey()],
            'zzzfractional' =>  $product['zzzfractional']
        );
        /* Декорируем в shopZzzfractionalPluginProductDecorator */
        $this->prepareProduct($product);
        /* Пишем в оригинальный объект преобразованные данные складов */
        $original_object = $product->getProduct();
        $original_object['skus'] = $product['skus'];
        $original_object['count'] = $product['count'];
        /* Отдаем скрипт для инициаклизации кратности покупки на js */
        return array(
            'cart' =>
                '<span class="zzzfractional-product-cart"></span><script>
                $.shopZzzfractionalPluginFrontend.initProduct(' . json_encode($return) . ');
                </script>'
        );
    }

    /**
     * @inheritdoc
     */
    public function frontendCart()
    {
        $cart = new shopCart();
        $items = $cart->items();
        $products = $this->getItemsProducts($items);
        $data = array();
        foreach ($items as $v) {
            if (array_key_exists('item', $v)) {
                $product_id = $v['item']['product_id'];
            } else {
                $product_id = $v['product_id'];
            }
            if (array_key_exists($product_id, $products)) {
                $product = $products[$product_id];
                $data[$v['id']] = $this->addProductData($product);
            }
        }
        return '<script>$.shopZzzfractionalPluginFrontend.initCart(' . json_encode($data) . ');</script>';
    }

    /**
     * @inheritdoc
     */
    public function cartAdd(&$item)
    {
        if (array_key_exists('product_id', $item)) {
            $unit_key = $this->unitKey();
            $product = $this->getProductModel()->getById($item['product_id']);
            if (is_array($product) && array_key_exists($unit_key, $product) && isset($item['id'])) {
                $cart_items_model = new shopCartItemsModel();
                $cart_items_model->updateById($item['id'], array($unit_key => $product[$unit_key]));
                $cart_items_model->updateByField(array('parent_id' => $item['id']), array($unit_key => $product[$unit_key]));
            }
        }
    }

    /**
     * @inheritdoc
     * @uses shopProductSaveController::execute()
     * @uses shopCsvProductrunController::stepImportSku()
     * @uses prepareSaveSkus()
     * @param array $data
     */
    public function productPresave(&$data)
    {
        $product = new shopZzzfractionalPluginProductDecorator($data['instance'], (wa()->getEnv() == 'frontend'));
        $data['new_data']['skus'] = $this->prepareSaveSkus($data['new_data']['skus'], $product);
    }
    
    /**
     * @inheritdoc
     * @param array $data
     */
    public function productSave(&$data)
    {
        // Дополняем продукт правильными данными
        $model = new shopProductSkusModel();
        $skus = $model->getData($data['instance']);
        $this->decodeSkus($skus);
        $data['instance']->setData('skus', $skus);
    }

    /**
     * Объединение старых и новых данных складов артикулов и преборазование в базовую единицу измерения
     * @see productPresave()
     * @param array $skus - Массив артикулов продукта
     * @param array $product -  продукт
     * @return mixed
     */
    protected function prepareSaveSkus($skus, $product)
    {
        $unit = $product[$this->unitKey()];
        if(waRequest::issetPost('skus')) {
            $save_skus = array_combine(array_keys($skus), waRequest::post('skus'));
        } else {
            $save_skus = $skus;
        }
        foreach ($skus as $id => $sku) {
            if (array_key_exists($id, $save_skus) && !empty($save_skus[$id]['stock'])) {
                foreach ($save_skus[$id]['stock'] as $sid => $v) {
                    $skus[$id]['stock'][$sid] = $this->encodeCount($v, $unit, null);
                }
            } else {
                if ($unit && $this->getEmulateType($sku) != $this->getEmulateString('encode')) {
                    foreach ($sku['stock'] as $sid => $v) {
                        $skus[$id]['stock'][$sid] = $this->encodeCount($v, $unit, null);
                    }
                }
            }
        }
        return $skus;
    }


    /****************************
     *  SYSTEM SPECIFIC METHODS
     ****************************/

    /**
     * Добавление в данные Лога складов артикулов единицы измерения
     * @see shopProductStocksLogModel::insert()
     * @param $data
     * @param null $product
     */
    public function productStocksLogModelPrepareInsert(&$data, $product = null)
    {
        $unit_key = $this->unitKey();
        if (empty($product) && (array_key_exists('product_id', $data) && !empty($data['product_id']))) {
            $model = $this->getProductModel();
            $product = $model->getById($data['product_id']);
        }
        if (!empty($product) && array_key_exists($unit_key, $product)) {
            $data[$unit_key] = $product[$unit_key];
        }
    }

    /**
     * Добавление в данные Лога складов артикулов единицы измерения
     * @see  shopProductStocksLogModel::multipleInsert()
     * @uses getItemsProducts()
     * @param array $data
     */
    public function productStocksLogModelPrepareMultipleInsert(&$data)
    {
        $products = $this->getItemsProducts($data);
        $unit_key = $this->unitKey();
        foreach ($data as &$v) {
            if (array_key_exists($v['product_id'], $products)) {
                $product = $products[$v['product_id']];
                if (array_key_exists($unit_key, $product)) {
                    $v[$unit_key] = $product[$unit_key];
                }
            }
        }
        unset($v);
    }

    /**
     * Преобразование данных из модели Логов складов в дробное
     * @see shopProductStocksLogModel::getList()
     * @param array $data
     */
    public function productStocksLogModelGetList(&$data)
    {
        $unit_key = $this->unitKey();
        $fields = array('before_count', 'after_count', 'diff_count');
        foreach ($data as &$v) {
            if (array_key_exists($unit_key, $v)) {
                /* Возможны несоответствия при смене единицы продукта??? */
                foreach ($fields as $field) {
                    $v[$field] = $this->decodeCount($v[$field], $v[$unit_key]);
                }
            }
        }
        unset($v);
    }

}
 