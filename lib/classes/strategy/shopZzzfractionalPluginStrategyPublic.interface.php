<?php


interface shopZzzfractionalPluginStrategyPublicInterface
{

    /**
     * Метод преобразуют количество для показа пользователю (эмуляция дробного)
     *
     * @param null|int|string $count        - количество для конвертации
     * @param  string         $unit_id      - единица измерения для конвертации от базовой
     * @param string|null     $cast_default - для бесконечного количества вернуть по умолчанию строку или null
     *
     * @return float|mixed|null|string
     */
    public function decodeCount(&$count = null, $unit_id, $cast_default = '');

    /**
     * Метод преобразуют количество из эмулированного для пользователя в реальное от базовой единицы
     *
     * @param null|int|string $count        - количество для конвертации
     * @param  string         $unit_id      - единица измерения для конвертации от базовой
     * @param string|null     $cast_default - для бесконечного количества вернуть по умолчанию строку или null
     *
     * @return float|mixed|null|string
     */
    public function encodeCount(&$count = null, $unit_id, $cast_default = '');


    /**********************************************************
     * Методы преобразуют количество $items в корзине и заказе
     **********************************************************/

    /**
     * Перевод количества для показа пользователю
     * @see emulateItemsCount()
     */
    public function decodeItemsCount(&$items);

    /**
     * Кодирование количества для запись в бд
     * @see shopZzzfractionalPluginStrategyEmulate::emulateItemsCount()
     */
    public function encodeItemsCount(&$items);


    public function addItemsData(&$items = array());

    /**********************************************************
     * Методы преобразуют количество одного $item в корзине и заказе
     **********************************************************/

    /**
     * Конвертация количества в видимое
     * @see emulateItemCount()
     *
     * @param array $item
     *
     * @return array $item
     */
    public function decodeItemCount(&$item, $product = null);

    /**
     * Приведение количества для запись в бд
     * @see emulateItemCount()
     *
     * @param array $item
     *
     * @return array $item
     */
    public function encodeItemCount(&$item, $product = null);



    /**********************************************************
     * Методы преобразуют количество в массиве $products, а также их skus
     **********************************************************/

    /**
     * Конвертация количества в видимое
     * @see emulateProducts()
     *
     * @param array $products
     *
     * @return array $products
     */
    public function decodeProducts(&$products);

    /**
     * Приведение количества для запись в бд
     * @see emulateProducts()
     *
     * @param array $products
     *
     * @return array $products
     */
    public function encodeProducts(&$products);

    /**
     * Конвертация количества в видимое
     * @see decodeProducts()
     *
     * @param array $product
     *
     * @return array $product
     */
    public function decodeProduct(&$product);

    /**
     * Приведение количества для запись в бд
     * @see encodeProducts()
     *
     * @param array $product
     *
     * @return array $product
     */
    public function encodeProduct(&$product);


    /**********************************************************
     * Методы преобразуют количество в массиве $skus
     **********************************************************/

    /**
     *  Конвертация количества в видимое
     *
     * @param $skus
     *
     * @return array $skus
     */
    public function decodeSkus(&$skus);

    /**
     * Приведение количества для запись в бд
     *
     * @param $skus
     *
     * @return array $skus
     */
    public function encodeSkus(&$skus);

    /**
     *  Конвертация количества в видимое
     * @see decodeSkus()
     *
     * @param $sku
     *
     * @return array $sku
     */
    public function decodeSku(&$sku);

    /**
     * Приведение количества для запись в бд
     * @see encodeSkus()
     *
     * @param $sku
     *
     * @return array $sku
     */
    public function encodeSku(&$sku);




    /************************
     *  CART ACTIONS EMULATE
     ************************/

    /**
     * @param $count
     * @param $product_data
     * @param $error - переменнкая по ссылке возвращает текст ошибки
     *
     * @return float|mixed|string
     */
    public function encodeAndCastCountByProduct(&$count, $product_data, &$error = null);

    /**
     * @param $count
     * @param $product_data
     * @param $error - переменнкая по ссылке возвращает текст ошибки
     *
     * @return float|mixed|string
     */
    public function decodeAndCastCountByProduct(&$count, $product_data, &$error = null);


    /**
     * @param      $count
     * @param      $product_data
     * @param null $error
     * @param null $type
     *
     * @return mixed
     */
    public function castCountByProduct(&$count, $product_data, &$error = null);

    public function encodeCartAddQuantity(&$quantity, $sku_id, $product_id = null, &$error = null);

    public function encodeCartSaveQuantity(&$quantity, $sku_id, $product_id = null, &$error = null);

    /************************
     * HELPERS
     ************************/

    public function setConvert($flag = null);

    public function returnValueType($type = shopZzzfractionalPluginConfig::RETURN_VALUE_TYPE_NUMERIC);

    public function castReturnValue(&$value);

    /**
     * @return string
     */
    public function unitKey();

    /**
     * @return string
     */
    public function multiplicityKey();

    /**
     * @return string
     */
    public function weightKey();


}

