<?php


interface shopZzzfractionalPluginStrategyInterface extends
    shopZzzfractionalPluginStrategyPublicInterface, shopZzzfractionalPluginHooksInterface
{
    public function addEmulateType(&$data, $type = '');

    /****************************
     *  SYSTEM SPECIFIC METHODS
     ****************************/
    /**
     * Возвращает строковый идентификатор текущего экшена (frontendcartadd, backendproductsave...)
     * @return string
     */

    public function getController();

    /**
     * @return shopZzzfractionalPluginConfig
     */
    public function getConfig();

    /**
     * @return shopProductModel
     */
    public function getProductModel();

    /**
     * Добавление в данные Лога складов артикулов единицы измерения
     * @see shopProductStocksLogModel::insert()
     *
     * @param      $data
     * @param null $product
     */
    public function productStocksLogModelPrepareInsert(&$data, $product = null);

    /**
     * Добавление в данные Лога складов артикулов единицы измерения
     * @see  shopProductStocksLogModel::multipleInsert()
     * @uses getItemsProducts()
     *
     * @param array $data
     */
    public function productStocksLogModelPrepareMultipleInsert(&$data);

    /**
     * Преобразование данных из модели Логов складов в дробное
     * @see shopProductStocksLogModel::getList()
     *
     * @param array $data
     */
    public function productStocksLogModelGetList(&$data);
}

