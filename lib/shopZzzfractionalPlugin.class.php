<?php

class shopZzzfractionalPlugin extends shopPlugin implements shopZzzfractionalPluginHooksInterface
{
    const PLUGIN_ID = 'zzzfractional';
    const DEFAULT_UNIT = 'item';
    const DEFAULT_UNIT_MULTIPLICITY = '1';

    protected $strategy = null;

    protected static $status = null;
    protected static $register_templates = array(
        'plugin' => array(
            'BackendProductEdit.edit_basics.html' => array(
                'path' => '/templates/actions/backend/BackendProductEdit.edit_basics.html',
                'description' => 'Файл для экшена редактирования продукта'
            ),
            /* FRONTEND */
            'shopZzzfractionalPluginFrontend.js' => array(
                'path' => '/js/shopZzzfractionalPluginFrontend.js',
                'description' => 'Файл для контроля ввода количества в темах'
            ),
        ),
        'theme' => array(
            'frontend.js' => array(
                'path' => 'frontend.js',
                'description' => 'Дополнительный файл JS для переопределения поведения плагина',
                'content' => '',
            ),

        ),
    );
    
  
    /**
     * @return shopZzzfractionalPluginStrategyInterface
     * */
    protected function getStrategy() {
        if($this->strategy == null) {
          $this->strategy = wao(new shopZzzfractionalPluginFactory())->getStrategy();
        }
        return $this->strategy;
    } 
    public function getRegisterTemplates()
    {
        return self::$register_templates;
    }

    public function appInit(&$app_config) {
        $version_installer = shopZzzfractionalPluginStrategyInstallerVersion::factory($app_config);
        if($version_installer instanceof shopZzzfractionalPluginStrategyInstallerVersionAutoload) {
           $version_installer->autoload();
        }
    }
    public function getTemplatesType()
    {
        return 'theme';
    }

    public static function isOn()
    {
        if (self::$status === null) {
            self::$status = false;
            $settings = wa('shop')->getPlugin(shopZzzfractionalPlugin::PLUGIN_ID)->getSettings();
            if (array_key_exists('status', $settings) && $settings['status'] == 1) {
                self::$status = true;
            }
        }
        return self::$status;
    }

    public function frontendHead()
    {
        if (self::isOn()) {
            return $this->getStrategy()->frontendHead();
        }
        return '';
    }

    public function backendProduct(&$product)
    {
        if (self::isOn()) {
            $this->getStrategy()->backendProductEdit($product);
        }
    }

    public function backendProductEdit(&$product)
    {
        if (self::isOn()) {
            return $this->getStrategy()->backendProductEdit($product);
        }
        return '';
    }

    public function productSave(&$product)
    {
        if (self::isOn()) {
            $this->getStrategy()->productSave($product);
        }
    }
    public function productPresave(&$product)
    {
        if (self::isOn()) {
            $this->getStrategy()->productPresave($product);
        }
    }

    public function frontendProducts(&$data)
    {
        if (self::isOn()) {
            $this->getStrategy()->frontendProducts($data);
        }
    }
    public function frontendCart()
    {
        if (self::isOn()) {
           return $this->getStrategy()->frontendCart();
        }
        return '';
    }
    public function cartAdd(&$item)
    {
        if (self::isOn()) {
            return $this->getStrategy()->cartAdd($item);
        }
    }

    public function  frontendProduct(&$product) {
        if (self::isOn()) {
            return $this->getStrategy()->frontendProduct($product);
        }
        return '';
    }
    public function backendOrderEdit(&$order)
    {
       if (self::isOn()) {
            $this->getStrategy()->backendOrderEdit($order);
        }
    }

    public function backendOrder(&$order)
    {
        /* if (self::isOn()) {
              $this->getStrategy()->backendOrder($order);
          }*/
    }

    public static function log($message = '')
    {
        waLog::log($message, 'shop_zzzfractional_plugin.log');
    }
    public function routing($route = array())
    {

        $files['shopSdekintPluginSdekSendAction'] = $this->path.'/lib/classes/plugins/sdekint/classes/workflow/shopSdekintPluginSdekSendAction.class.php';
        if (!empty($files)) {
            $autoload = waAutoload::getInstance();
            foreach ($files as $class_name => $path) {
                $autoload->add($class_name, $path);
            }
        }
        $file = $this->path.'/lib/config/routing.php';
        if (file_exists($file)) {
            /**
             * @var array $route Variable available at routing file
             */
            return include($file);
        } else {
            return array();
        }
    }
   
}



   