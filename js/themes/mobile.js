$.shopZzzfractionalPluginFrontendConfig.cart_root = '.cart';
$.shopZzzfractionalPluginFrontendConfig.item_root_selector = '.row, .zzzfractional_item';
$.shopZzzfractionalPluginFrontendConfig.item_quantity = '.qty, .zzzfractional_quantity';

$.shopZzzfractionalPluginFrontendConfig.button_plus = '.plus-link';
$.shopZzzfractionalPluginFrontendConfig.button_minus = '.minus-link';