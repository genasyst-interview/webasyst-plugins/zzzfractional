if (typeof floatval != 'function') {
    function floatval(val) {
        if (val !== null && val !== undefined && val.toString().match(/,/)) {
            val = val.replace(/,/, '.');
            return parseFloat(val);
        }
        if (!isNaN(val)) {
            return parseFloat(val);
        } else {
            return 0.0;
        }
    }
}

$.shopZzzfractionalPluginFrontendConfig = {
    init_delay: 100,
    button_plus: '.zzzfractional_plus',
    button_minus: '.zzzfractional_minus',
    quantity: '[name="quantity"]',
    add2cart: '.add2cart',
    price: '.price',
    comparePrice: '.compare-at-price',
    product_root_selector: 'form',
    product_cart_selector: '.zzzfractional-product-cart',
    cart_root: '.cart',
    item_root_selector: '.item, .zzzfractional_item',
    item_quantity: '.qty, .zzzfractional_quantity',
    item_id: 'id',
    getRoot: function ($elem) {
        if ($elem.closest(this.item_root_selector).length > 0) {
            return this.getItemRoot($elem);
        } else {
            return this.getProductRoot($elem);
        }
    },
    getProductRoot: function ($elem) {
        return $elem.closest(this.product_root_selector);
    },
    isProductCart: function ($elem) {
        var root = this.getProductRoot($elem);
        return (root.find(this.product_cart_selector).length > 0)
    },
    getItemRoot: function ($elem) {
        return $elem.closest(this.item_root_selector);
    },
    getItemId: function ($item_root) {
        return $item_root.data(this.item_id);
    },
    getItems: function () {
        var self = this;
        var items = {};
        $(this.cart_root).find(this.item_root_selector).each(function () {
            var id = self.getItemId($(this));
            if (parseInt(id) > 0) {
                items[id] = $(this);
            }
        });
        return items;
    }
};

var shopZzzfractionalPluginFrontendProductEntity = function ($quantity, type) {
    var self = this;
    self.config = $.shopZzzfractionalPluginFrontendConfig;
    // DOM
    self.$quantity = $quantity;
    self.type = 'product';
    if ($quantity.closest(self.config.item_root_selector).length > 0) {
        self.type = 'item';
        self.$root = self.config.getItemRoot($quantity);
    } else if (type == 'item') {
        self.type = 'item';
        self.$root = self.config.getItemRoot($quantity);
    } else {
        self.$root = self.config.getProductRoot($quantity);
    }
    self.$quantity = $quantity;
    self.is_fractional = false;
    self.multiplicity = 1;
    self.init();
};
shopZzzfractionalPluginFrontendProductEntity.prototype.init = function () {
    this.$quantity.val(this.$quantity.val().toString().replace(/,/, '.'));
    if(this.$quantity.val().toString().match( /\./ig )) {
        this.$quantity.val(this.$quantity.val().toString().replace(/0$/, ''));
    }
    var multiplicity = this.$quantity.data("unit_multiplicity");
    if (this.$quantity.data('fractional') == '1') {
        this.is_fractional = true;
        if (floatval(multiplicity) > 0.0) {
            this.multiplicity = floatval(multiplicity);
        }
    } else {
        multiplicity = parseInt(multiplicity);
        this.multiplicity = ( isNaN(multiplicity) && multiplicity > 0 ) ? multiplicity : 1;
    }
    this.unbind();
    this.bind();
};
shopZzzfractionalPluginFrontendProductEntity.prototype.getElement = function (selector) {
    return this.$root.find(selector);
};
shopZzzfractionalPluginFrontendProductEntity.prototype.count = function (mixed_var, mode) {
    var key, cnt = 0;
    if (mode == 'COUNT_RECURSIVE')mode = 1;
    if (mode != 1)mode = 0;
    for (key in mixed_var) {
        cnt++;
        if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
            cnt += this.count(mixed_var[key], 1)
        }
    }
    return cnt;
};
shopZzzfractionalPluginFrontendProductEntity.prototype.unbinds = {};
shopZzzfractionalPluginFrontendProductEntity.prototype.binds = {
    'quantity': function (self) {
        var events = {};
        var old_events = self.$quantity.data('events');
        if (typeof (old_events) == 'object' && old_events.hasOwnProperty('change')) {
            events = old_events.change.slice();
            self.$quantity.off('change').unbind("change");
        }
        if (self.type == 'item') {
            //self.$quantity.off('change');
        } else {
            events = {};
            self.$quantity.off('change').off('click')
                .off('keyup').off('keydown');
            self.$quantity.unbind('change').unbind('click')
                .unbind('keyup').unbind('keydown');

        }
        self.$quantity.on('change', function () {
            self.changeQuantity($(this));
        });
        if (typeof (events) == 'object' && self.count(events) > 0) {
            $.each(events, function (i, event) {
                self.$quantity.on('change', event.handler);
            });
        }
    },
    'button_plus': function (self) {
        if (typeof(self.config.button_plus) == 'string' && self.config.button_plus != '') {
            var button_plus = self.getElement(self.config.button_plus);
            if (button_plus.length > 0) {
                button_plus.unbind("click").off('click').bind("click", function () {
                    self.quantityUp($(this));
                });
            }
        }
    },
    'button_minus': function (self) {
        if (typeof(self.config.button_minus) == 'string' && self.config.button_minus != '') {
            var button_minus = self.getElement(self.config.button_minus);
            if (button_minus.length > 0) {
                button_minus.unbind("click").off('click').bind("click", function () {
                    self.quantityDown($(this));
                });
            }
        }
    }
};
shopZzzfractionalPluginFrontendProductEntity.prototype.bind = function () {
    var self = this;
    for (var k in this.binds) {
        var func = this.binds[k];
        if (typeof func === 'function') {
            func(self);
        }
    }
};
shopZzzfractionalPluginFrontendProductEntity.prototype.unbind = function () {
    var self = this;
    for (var k in this.unbinds) {
        var func = this.unbinds[k];
        if (typeof func === 'function') {
            func(self);
        }
    }
};
shopZzzfractionalPluginFrontendProductEntity.prototype.getRoot = function ($elem) {
    return $.shopZzzfractionalPluginFrontendConfig.getRoot($elem);
};
shopZzzfractionalPluginFrontendProductEntity.prototype.quantityUp = function ($elem) {
    var $quantity = this.$quantity;
    var current_val = this.getQuantityValue(this.$quantity);
    current_val = this.round(current_val, this.multiplicity);
    var new_val = floatval(current_val) + floatval(this.multiplicity);
    new_val = this.round(new_val, this.multiplicity);
    $quantity.val(this.castQuantity(new_val));
    $quantity.trigger('change');
};
shopZzzfractionalPluginFrontendProductEntity.prototype.quantityDown = function ($elem) {
    var $quantity = this.$quantity;
    var current_val = this.getQuantityValue($quantity);
    current_val = this.round(current_val, this.multiplicity);
    var new_val = floatval(current_val) - floatval(this.multiplicity);
    new_val = this.round(new_val, this.multiplicity);
    $quantity.val(this.castQuantity(new_val));
    $quantity.trigger('change');
};
shopZzzfractionalPluginFrontendProductEntity.prototype.castQuantity = function (quantity) {
    if (this.is_fractional) {
        return floatval(quantity).toFixed(3).toString().replace(/,/, '.').replace(/0$/, '');
    } else {
        return parseInt(floatval(quantity));
    }
};
shopZzzfractionalPluginFrontendProductEntity.prototype.isNotMultiple = function (quantity) {
    if (this.is_fractional) {
        var num = quantity / this.multiplicity.toFixed(3);
        num = num.toFixed(3);
        return (num % 1 == 0);
    } else {
        return (quantity % this.multiplicity == 0);
    }
};
shopZzzfractionalPluginFrontendProductEntity.prototype.round = function (quantity, multiplicity) {
    var new_val = quantity;
    var zzzfractional = $.shopZzzfractionalPluginFrontend;
    if (!this.isNotMultiple(quantity) && zzzfractional.isMultiplicityConversion()) {
        if (zzzfractional.isMultiplicityError()) {
            alert('Введено не кратное количество товара, кратность товара составляет '
                + multiplicity.toString().replace(/,/, '.') + '! ' +
                "\nВ корзину будет добавлено округленное количество!");
        }
        new_val = Math.round(quantity / multiplicity) * multiplicity;
    }
    if (this.is_fractional) {
        return new_val.toFixed(3);
    } else {
        return parseInt(new_val);
    }
};
shopZzzfractionalPluginFrontendProductEntity.prototype.changeQuantity = function ($quantity) {
    var current_val = this.getQuantityValue($quantity);
    current_val = this.round(current_val, this.multiplicity);
    if (this.is_fractional) {
        $quantity.val(floatval(current_val).toFixed(3).toString().replace(/,/, '.').replace(/0$/, ''));
    } else {
        $quantity.val(current_val.toString().replace(/,/, '.').replace(/0$/, ''));
    }
};
shopZzzfractionalPluginFrontendProductEntity.prototype.getQuantityValue = function ($quantity) {
    var val = parseInt($quantity.val().replace(/,/, '.'));
    if (this.is_fractional) {
        val = floatval($quantity.val().replace(/,/, '.'));
    }
    if (val <= 0.0) {
        if (this.multiplicity > 0.0) {
            val = this.multiplicity;
        } else {
            val = 1;
        }
    }
    return val;
};


$.shopZzzfractionalPluginFrontend = {
    settings: {
        'multiplicity_conversion': 1,
        'multiplicity_error': 0
    },
    products: {},
    items: {},
    frontend_product: {},
    setSettings: function (data) {
        if (typeof (data) == 'object') {
            this.settings = data;
        } else {
            console.error('Zzzfractional: Settings is not defined!');
        }
    },
    getSetting: function (name) {
        if (typeof (this.settings) == 'object' && this.settings.hasOwnProperty(name)) {
            return this.settings[name];
        } else {
            console.error('Zzzfractional: Setting (' + name + ') is not defined!');
            return null;
        }
    },
    isMultiplicityConversion: function () {
        return (this.getSetting('multiplicity_conversion') == 1);
    },
    isMultiplicityError: function () {
        return (this.getSetting('multiplicity_error') == 1);
    },
    init: function () {
        setTimeout(function () {
            $.shopZzzfractionalPluginFrontend._init();
        }, parseInt(this.init_delay));

    },
    _init: function () {
        var config = $.shopZzzfractionalPluginFrontendConfig;
        $(config.quantity).each(function () {
            if (!config.isProductCart($(this))) {
                new shopZzzfractionalPluginFrontendProductEntity($(this));
            }
        });
    },
    initCart: function (items) {
        var self = this;
        self.items = items;
        var config = $.shopZzzfractionalPluginFrontendConfig;
        $(document).ready(function () {
            setTimeout(function () {
                var cart_items = config.getItems();
                for (var k in cart_items) {
                    if (self.items.hasOwnProperty(k)) {
                        var item = cart_items[k];
                        var item_data = self.items[k];
                        var quantity = item.find(config.item_quantity);
                        if (quantity.length > 0) {
                            quantity.data('unit_multiplicity', item_data['unit_multiplicity']);
                            quantity.data('fractional', item_data['zzzfractional']);
                            new shopZzzfractionalPluginFrontendProductEntity(quantity, 'item');
                        } else {
                            console.log('Не найдено количество: ' + config.item_quantity);
                        }
                    } else {
                        console.log('Не найден Item: ' + k);
                    }
                }
            }, parseInt(self.init_delay));
        });
    },
    initProduct: function (data) {
        var self = this;
        self.frontend_product = data;
        var config = $.shopZzzfractionalPluginFrontendConfig;
        $(document).ready(function () {
            setTimeout(function () {
                if ($(config.product_cart_selector).length > 0) {
                    var quantity = $(config.product_cart_selector).parent().find(config.quantity);
                    if (quantity.length > 0) {
                        quantity.data('unit_multiplicity', self.frontend_product['unit_multiplicity']);
                        quantity.data('fractional', self.frontend_product['zzzfractional']);
                        new shopZzzfractionalPluginFrontendProductEntity(quantity, 'product');
                    }
                }
            }, parseInt(self.init_delay));

        });
    }
};
$(document).ready(function () {
    $.shopZzzfractionalPluginFrontend.init();
});
